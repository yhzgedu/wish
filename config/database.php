<?php

return [
    'fetch' => PDO::FETCH_ASSOC,

    'default' => env('DB_CONNECTION', 'mysql'),

    'connections' => [
        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST'),
            'port' => env('DB_PORT'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => 'w_',
            'strict' => false,
            'engine' => null,
        ],
    ],

    'migrations' => 'migrations',
    //学样性质
    'school_nature' => [
        [
            'id'=>1,
            'name'=>'综合',

        ],
        [
            'id'=>2,
            'name'=>'理工',

        ],
        [
            'id'=>3,
            'name'=>'医药',

        ],
        [
            'id'=>4,
            'name'=>'军事',

        ],
        [
            'id'=>5,
            'name'=>'艺术',

        ],
    ],
    //学校层次
    'school_arrangement' => [
        [
            'id'=>1,
            'name'=>'本科',

        ],
        [
            'id'=>1,
            'name'=>'专科',

        ],
    ],
    //录取概率
    'admissions_probability'=>[
        [
            'id'=>1,
            'name'=>'冲',

        ],
        [
            'id'=>2,
            'name'=>'保',

        ],
        [
            'id'=>3,
            'name'=>'稳',

        ],
        [
            'id'=>4,
            'name'=>'垫',

        ]

    ]
    


];
