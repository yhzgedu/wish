<?php

return [
    // PHP
    App\Repository\Contracts\AdminInterface::class                  => App\Repository\Db\AdminRepository::class,
    App\Repository\Contracts\CodeInterface::class                   => App\Repository\Db\CodeRepository::class,
    App\Repository\Contracts\EvaluateInterface::class               => App\Repository\Db\EvaluateRepository::class,
    App\Repository\Contracts\FavoriteInterface::class               => App\Repository\Db\FavoriteRepository::class,
    App\Repository\Contracts\InsuranceInterface::class              => App\Repository\Db\InsuranceRepository::class,
    App\Repository\Contracts\MessageInterface::class                => App\Repository\Db\MessageRepository::class,
    App\Repository\Contracts\MessageTemplateInterface::class        => App\Repository\Db\MessageTemplateRepository::class,
    App\Repository\Contracts\NewsInterface::class                   => App\Repository\Db\NewsRepository::class,
    App\Repository\Contracts\NoticeInterface::class                 => App\Repository\Db\NoticeRepository::class,
    App\Repository\Contracts\OperationInterface::class              => App\Repository\Db\OperationRepository::class,
    App\Repository\Contracts\ProvinceScoreInterface::class          => App\Repository\Db\ProvinceScoreRepository::class,
    App\Repository\Contracts\SchoolInterface::class                 => App\Repository\Db\SchoolRepository::class,
    App\Repository\Contracts\SchoolScoreInterface::class            => App\Repository\Db\SchoolScoreRepository::class,
    App\Repository\Contracts\CollegeApplicationInterface::class     => App\Repository\Db\CollegeApplicationRepository::class,
    App\Repository\Contracts\UserInterface::class                   => App\Repository\Db\UserRepository::class,
    App\Repository\Contracts\MajorInterface::class                  => App\Repository\Db\MajorRepository::class,
    App\Repository\Contracts\SchoolMajorInterface::class            => App\Repository\Db\SchoolMajorRepository::class,
    App\Repository\Contracts\FeedbackInterface::class               => App\Repository\Db\FeedbackRepository::class,
    App\Repository\Contracts\BannerInterface::class                 => App\Repository\Db\BannerRepository::class,
    App\Repository\Contracts\ShareInterface::class                  => App\Repository\Db\ShareRepository::class,
    App\Repository\Contracts\FilterInterface::class                 => App\Repository\Db\FilterRepository::class,
    App\Repository\Contracts\ProbabilityInterface::class            => App\Repository\Db\ProbabilityRepository::class,
     App\Repository\Contracts\AnalogueFillingInterface::class            => App\Repository\Db\AnalogueFillingRepository::class,
    // Redis
];
