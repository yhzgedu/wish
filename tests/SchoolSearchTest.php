<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SchoolSearchTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSearchPlan()
    {
        $this->get('/schools/search/plan?name=学院&province_id=3')->assertResponseOk();
    }
}
