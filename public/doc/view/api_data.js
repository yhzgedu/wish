define({ "api": [
  {
    "type": "post",
    "url": "analogueFilling/addmajor",
    "title": "模拟报考添加专业",
    "version": "1.0.0",
    "name": "analogueFilling_addmajor",
    "group": "AnalogueFilling",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "school_id",
            "description": "<p>学校id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>[作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"code\": 200,\n      \"message\": \"Success\",\n      \"data\": {\n         \"id\": 学校id\n\t        \"name\": 学校名称\n\t        \"logo\": 学校logo\n\t        \"filingProbability\": 投档概率\n\t        \"major_list\": [//专业列表\n\t            {\n\t                \"school_major_id\": 专业id\n\t                \"major_name\": 专业名称\n\t            },\n           ....\n          ]\n       }\n      \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/AnalogueFilling.php",
    "groupTitle": "AnalogueFilling",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn/analogueFilling/addmajor"
      }
    ]
  },
  {
    "type": "post",
    "url": "analogueFilling/addschool",
    "title": "模拟报考添加学校",
    "version": "1.0.0",
    "name": "analogueFilling_addschool",
    "group": "AnalogueFilling",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称关键字</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>[作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"code\": 200,\n      \"message\": \"Success\",\n      \"data\": [\n          {\n\t            \"id\": 学校id\n\t            \"name\": 学校名称\n\t        },\n           ....\n       ]\n      \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/AnalogueFilling.php",
    "groupTitle": "AnalogueFilling",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn/analogueFilling/addschool"
      }
    ]
  },
  {
    "type": "post",
    "url": "/collegeApplications",
    "title": "模拟填报保存",
    "version": "1.0.0",
    "name": "collegeApplicationFill",
    "group": "AnalogueFilling",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        },
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "school_id",
            "description": "<p>学校ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "first_major_id",
            "description": "<p>第一专业ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_major_name",
            "description": "<p>第一专业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "second_major_id",
            "description": "<p>第二专业ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "second_major_name",
            "description": "<p>第二专业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "third_major_id",
            "description": "<p>第三专业ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "third_major_name",
            "description": "<p>第三专业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "fourth_major_id",
            "description": "<p>第四专业ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fourth_major_name",
            "description": "<p>第四专业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "fifth_major_id",
            "description": "<p>第五专业ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fifth_major_name",
            "description": "<p>第五专业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sixth_major_id",
            "description": "<p>第六专业ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sixth_major_name",
            "description": "<p>第六专业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "degree_type",
            "description": "<p>学历类型</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "type",
            "description": "<p>填报类型(1:冲;2:保;3:稳;4:垫)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sort",
            "description": "<p>排序（1-6分别代表第一到第六志愿）</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "is_obey",
            "description": "<p>是否服从调剂(1:是;2:否)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"school_id\": 1,\n  \"school_name\": \"北京大学\",\n  \"first_major_id\": 1\n  \"first_major_name\": \"first_name\"\n  \"second_major_id\": 2\n  \"second_major_name\": \"second_name\"\n  \"third_major_id\": 3\n  \"third_major_name\": \"third_name\"\n  \"fourth_major_id\": 4\n  \"fourth_major_name\": \"fourth_name\"\n  \"fifth_major_id\": 5\n  \"fifth_major_name\": \"fifth_name\"\n  \"sixth_major_id\": 6\n  \"sixth_major_name\": \"sixth_name\"\n  \"degree_type\": \"本科一批\",\n  \"type\": 1,\n  \"sort\": 1,\n  \"is_obey\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/CollegeApplication.php",
    "groupTitle": "AnalogueFilling",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//collegeApplications"
      }
    ]
  },
  {
    "type": "get",
    "url": "/collegeApplications?limit={limit}&page={page}",
    "title": "模拟报考首页（列表）",
    "version": "1.0.0",
    "name": "collegeApplicationList",
    "group": "AnalogueFilling",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>志愿ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "uid",
            "description": "<p>填报人ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "school_id",
            "description": "<p>学校ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "first_major_id",
            "description": "<p>第一专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "first_major_name",
            "description": "<p>第一专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "second_major_id",
            "description": "<p>第二专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "second_major_name",
            "description": "<p>第二专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "third_major_id",
            "description": "<p>第三专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "third_major_name",
            "description": "<p>第三专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "fourth_major_id",
            "description": "<p>第四专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "fourth_major_name",
            "description": "<p>第四专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "fifth_major_id",
            "description": "<p>第五专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "fifth_major_name",
            "description": "<p>第五专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "sixth_major_id",
            "description": "<p>第六专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "sixth_major_name",
            "description": "<p>第六专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "degree_type",
            "description": "<p>学历类型</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "type",
            "description": "<p>填报类型(1:冲;2:保;3:稳;4:垫)</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "sort",
            "description": "<p>排序（1-6分别代表第一到第六志愿）</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "is_obey",
            "description": "<p>是否服从调剂(1:是;2:否)</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:显示2:删除]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": {\n      \"本科第一批\": [\n        {\n          \"id\": 2,\n          \"uid\": 1,\n          \"school_id\": 2012,\n          \"school_name\": \"兰州大学\",\n          \"first_major_id\": 2019,\n          \"first_major_name\": \"经济学\",\n          \"second_major_id\": null,\n          \"second_major_name\": null,\n          \"third_major_id\": null,\n          \"third_major_name\": null,\n          \"fourth_major_id\": null,\n          \"fourth_major_name\": null,\n          \"fifth_major_id\": null,\n          \"fifth_major_name\": null,\n          \"sixth_major_id\": null,\n          \"sixth_major_name\": null,\n          \"degree_type\": \"本科第一批\",\n          \"type\": 1,\n          \"sort\": 1,\n          \"is_obey\": 1,\n          \"status\": 1,\n          \"created_at\": \"2018-01-16 11:37:57\"\n          \"updated_at\": \"2018-01-16 11:37:57\"\n          \"logo\": \"http://school-icon.b0.upaiyun.com/52ac2e9a747aec013fcf5251.jpg\"\n        },\n        {\n          \"id\": 3,\n          \"uid\": 1,\n          \"school_id\": 2017,\n          \"school_name\": \"成都学院\",\n          \"first_major_id\": 2019,\n          \"first_major_name\": \"经济学\",\n          \"second_major_id\": null,\n          \"second_major_name\": null,\n          \"third_major_id\": null,\n          \"third_major_name\": null,\n          \"fourth_major_id\": null,\n          \"fourth_major_name\": null,\n          \"fifth_major_id\": null,\n          \"fifth_major_name\": null,\n          \"sixth_major_id\": null,\n          \"sixth_major_name\": null,\n          \"degree_type\": \"本科第一批\",\n          \"type\": 2,\n          \"sort\": 2,\n          \"is_obey\": 1,\n          \"status\": 1,\n          \"created_at\": \"2018-01-16 12:15:21\"\n          \"updated_at\": \"2018-01-16 12:15:21\"\n          \"logo\": \"http://school-icon.b0.upaiyun.com/52ac2e9b747aec013fcf5367.jpg\"\n        }\n      ],\n      \"本科第二批\": [\n        {\n          \"id\": 4,\n          \"uid\": 1,\n          \"school_id\": 2020,\n          \"school_name\": \"长春东方职业学院\",\n          \"first_major_id\": 2020,\n          \"first_major_name\": \"经济学\",\n          \"second_major_id\": null,\n          \"second_major_name\": null,\n          \"third_major_id\": null,\n          \"third_major_name\": null,\n          \"fourth_major_id\": null,\n          \"fourth_major_name\": null,\n          \"fifth_major_id\": null,\n          \"fifth_major_name\": null,\n          \"sixth_major_id\": null,\n          \"sixth_major_name\": null,\n          \"degree_type\": \"本科第二批\",\n          \"type\": 2,\n          \"sort\": 2,\n          \"is_obey\": 1,\n          \"status\": 1,\n          \"created_at\": \"2018-01-16 12:19:09\"\n          \"updated_at\": \"2018-01-16 12:19:09\"\n          \"logo\": \"http://school-icon.b0.upaiyun.com/52ac2e99747aec013fcf4f5e.jpg\"\n        }\n      ]\n    },\n    \"total\": 1\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/CollegeApplication.php",
    "groupTitle": "AnalogueFilling",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//collegeApplications?limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/banners",
    "title": "轮播图获取",
    "version": "1.0.0",
    "name": "bannerList",
    "group": "Banner",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": [\n    {\n      \"title\": \"Banner3\",\n      \"sort\": 3,\n      \"url\": \"http://www.yhzgedu.com/banner/banner3.png\"\n    },\n    {\n      \"title\": \"Banner2\",\n      \"sort\": 2,\n      \"url\": \"http://www.yhzgedu.com/banner/banner2.png\"\n    },\n    {\n      \"title\": \"Banner1\",\n      \"sort\": 1,\n      \"url\": \"http://www.yhzgedu.com/banner/banner1.png\"\n    }\n  ],\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Banner.php",
    "groupTitle": "Banner",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//banners"
      }
    ]
  },
  {
    "type": "post",
    "url": "/codes",
    "title": "公共 - 获取验证码",
    "version": "1.0.0",
    "name": "commonCode",
    "group": "Common",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"phone\": \"15111223344\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<p>[ &gt; 0 获取成功]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 3,\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Common.php",
    "groupTitle": "Common",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//codes"
      }
    ]
  },
  {
    "type": "get",
    "url": "/provinces",
    "title": "公共 - 获取省区名称和id",
    "version": "1.0.0",
    "name": "commonProvince",
    "group": "Common",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>省区id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>省区名称</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": [\n    {\n      \"id\": 1,\n      \"name\": \"上海\"\n    }\n  ],\n  \"token\": \"null\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Common.php",
    "groupTitle": "Common",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//provinces"
      }
    ]
  },
  {
    "type": "post",
    "url": "/evaluates",
    "title": "用户评测 - 创建",
    "version": "1.0.0",
    "name": "evaluateCreated",
    "group": "Evaluate",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>评测类型：character-&gt;性格评测；profession-&gt;专业评测</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "result",
            "description": "<p>评测用户所选答案</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "key",
            "description": "<p>评测结果</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"type\": \"character\",\n  \"result\": \"E,I,E,I,E,E,E,S,S,N,N,S,N,N,F,F,T,F,F,F,F,P,J,J,J,J,J,J,J\",\n  \"key\": \"ISFJ\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>评测类型：character-&gt;性格评测；profession-&gt;专业评测</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result",
            "description": "<p>评测用户所选答案</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "key",
            "description": "<p>评测结果</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": [\n    {\n      \"type\": \"character\",\n      \"result\": \"E,I,E,I,E,E,E,S,S,N,N,S,N,N,F,F,T,F,F,F,F,P,J,J,J,J,J,J,J\",\n      \"key\": \"ISFJ\",\n    }\n  ],\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Evaluate.php",
    "groupTitle": "Evaluate",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//evaluates"
      }
    ]
  },
  {
    "type": "get",
    "url": "/evaluates",
    "title": "用户评测 - 获取",
    "version": "1.0.0",
    "name": "evaluateList",
    "group": "Evaluate",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>评测类型：character-&gt;性格评测；profession-&gt;专业评测</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result",
            "description": "<p>评测用户所选答案</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "key",
            "description": "<p>评测结果</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "total",
            "description": "<p>用户已评测数量。0代表还没有评测</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"list\": [],\n    \"total\": 0\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}\n\nHTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"list\": [\n      {\n        \"type\": \"character\",\n        \"result\": \"E,I,E,I,E,E,E,S,S,N,N,S,N,N,F,F,T,F,F,F,F,P,J,J,J,J,J,J,J\",\n        \"key\": \"INFJ\"\n      }\n    ],\n    \"total\": 1\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Evaluate.php",
    "groupTitle": "Evaluate",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//evaluates"
      }
    ]
  },
  {
    "type": "get",
    "url": "/favorites/cancel/{target}/{target_id}",
    "title": "收藏 - 取消",
    "version": "1.0.0",
    "name": "favoritesCancel",
    "group": "Favorites",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "target",
            "description": "<p>目标[ 1 =&gt; 学校, 2 =&gt; 专业, 3 =&gt; 招生计划 ]</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "target_id",
            "description": "<p>目标ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Favorite.php",
    "groupTitle": "Favorites",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//favorites/cancel/{target}/{target_id}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/favorites",
    "title": "收藏 - 创建",
    "version": "1.0.0",
    "name": "favoritesCreated",
    "group": "Favorites",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "target",
            "description": "<p>目标[ 1 =&gt; 学校, 2 =&gt; 专业, 3 =&gt; 招生计划 ]</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "target_id",
            "description": "<p>目标ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"target\": \"major\",\n  \"target_id\": \"2\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Favorite.php",
    "groupTitle": "Favorites",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//favorites"
      }
    ]
  },
  {
    "type": "get",
    "url": "/favorites/{target}?limit={limit}&page={page}",
    "title": "收藏 - 列表",
    "version": "1.0.0",
    "name": "favoritesList",
    "group": "Favorites",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "target",
            "description": "<p>目标[ 1 =&gt; 学校, 2 =&gt; 专业, 3 =&gt; 招生计划 ]</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "//学校收藏\nHTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 2020,\n        \"name\": \"长春东方职业学院\",\n        \"logo\": null,\n        \"is_985\": 0,\n        \"is_211\": 0,\n        \"type\": 公办, //学校类型 [ 公立, 民办 ]\n        \"property\": \"一本\", //学校性质 [ 一本, 二本, 三本, 专科 ... ]\n        \"subject_type\": \"医药院校\", //学科类型 [ 综合院校, 工科院校, 艺术院校 ... ]\n        \"education\": \"专科\", //学历类型 [ 本科/专科, 本科, 专科 ... ]\n        \"archive_year\": 2017,\n        \"archive_province\": \"长春\",\n        \"archive_score\": 555,\n        \"archive_probability\": \"40%\",\n        \"province_name\": \"吉林\", //学校地址\n        \"address\": \"长春市净月旅游经济开发区新大学城路\", //详细地址\n        \"male_ratio\": 42,\n        \"female_ratio\": 58,\n        \"comprehensive_rank\": 0 //综合排名\n      }\n    ],\n    \"total\": 2\n  },\n  \"token\": \"eyJpdiI6InJhTklZWlArUzFIS25LdnQycFJHUFE9PSIsInZhbHVlIjoiTHVXTUo2bkR5d29Nd3BkQ2ZHN0JBampLalpDRDM0emNqaUdUOG9aNlZRT0hOTDVsbDVBTEluXC9hWVZVSStoSlEiLCJtYWMiOiI2Y2MxZDA4MjFkMWIxNWI0MzM4NGQwMDM3ODcxNTU5ZGVlOWI2OTQwMDM1NmYwZWFmZDlmMThkMjc0ZGQ1NmExIn0=\"\n}\n\n//专业收藏\nHTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 2,\n        \"name\": \"航空地面设备维修\", //专业名称\n        \"degree_type\": \"专科\", //学历类型\n        \"major_type\": \"交通运输大类\", //专业类别\n        \"major_category\": \"航空运输类\"\n      }\n    ],\n    \"total\": 2\n  },\n  \"token\": \"eyJpdiI6InJhTklZWlArUzFIS25LdnQycFJHUFE9PSIsInZhbHVlIjoiTHVXTUo2bkR5d29Nd3BkQ2ZHN0JBampLalpDRDM0emNqaUdUOG9aNlZRT0hOTDVsbDVBTEluXC9hWVZVSStoSlEiLCJtYWMiOiI2Y2MxZDA4MjFkMWIxNWI0MzM4NGQwMDM3ODcxNTU5ZGVlOWI2OTQwMDM1NmYwZWFmZDlmMThkMjc0ZGQ1NmExIn0=\"\n}\n\n//招生计划收藏\nHTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 2,\n        \"school_id\": 2,\n        \"school_name\": \"河套学院\", //学校名称\n        \"school_code\": null, //学校代码\n        \"year\": null, //年份\n        \"count\": 500 //招生人数\n      }\n    ],\n    \"total\": 2\n  },\n  \"token\": \"eyJpdiI6InJhTklZWlArUzFIS25LdnQycFJHUFE9PSIsInZhbHVlIjoiTHVXTUo2bkR5d29Nd3BkQ2ZHN0JBampLalpDRDM0emNqaUdUOG9aNlZRT0hOTDVsbDVBTEluXC9hWVZVSStoSlEiLCJtYWMiOiI2Y2MxZDA4MjFkMWIxNWI0MzM4NGQwMDM3ODcxNTU5ZGVlOWI2OTQwMDM1NmYwZWFmZDlmMThkMjc0ZGQ1NmExIn0=\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Favorite.php",
    "groupTitle": "Favorites",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//favorites/{target}?limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/feedback",
    "title": "用户反馈 - 创建",
    "version": "1.0.0",
    "name": "feedbackCreated",
    "group": "Feedback",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account",
            "description": "<p>用户手机/微信/QQ</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>反馈内容</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"account\": \"18223505994\",\n  \"content\": \"萤火之光，可以燎原！\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Feedback.php",
    "groupTitle": "Feedback",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//feedback"
      }
    ]
  },
  {
    "type": "get",
    "url": "/filters?province_id={province_id}&school_nature={school_nature}&school_arrangement={school_arrangement}&major_name={major_name}&admissions_probability={admissions_probability}&limit={limit}&page={page}",
    "title": "智能筛选列表",
    "version": "1.0.0",
    "name": "filters",
    "group": "Filter",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "province_id",
            "description": "<p>学校地区</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_nature",
            "description": "<p>学校性质</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_arrangement",
            "description": "<p>学校层次</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "major_name",
            "description": "<p>专业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "admissions_probability",
            "description": "<p>录取概率</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>[作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"code\": 200,\n      \"message\": \"Success\",\n      \"data\": {\n        \"list\": [\n           {\n              \"school_id\":学校id\n*              \"school_name\": 学校名称\n              \"major_id\": 专业id\n              \"major_name\": 专业名称\n              \"school_logo\": 学校logo\n              \"is_985\": 1, 是否是985（1=是,0=否）\n              \"is_211\": 1, 是否是211（1=是,0=否）\n              \"subject_type\": 学校性质,\n              \"education\": 学校层次,\n              \"property\":  本次,\n              \"province_name\" : 省名称\n              \"year\": 年份\n                 \"min_score\": 最低分数\n                 \"recruit_count\": 计划招生人数\n                 \"probability\": 录取概率\n          },\n          ........\n      }\n      \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Filter.php",
    "groupTitle": "Filter",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//filters?province_id={province_id}&school_nature={school_nature}&school_arrangement={school_arrangement}&major_name={major_name}&admissions_probability={admissions_probability}&limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/filters_option",
    "title": "智能筛选项",
    "version": "1.0.0",
    "name": "filters_option",
    "group": "Filter",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>[作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n      \"school_filter\": {//学校选项\n          \"area\": [//地区\n               {\n                   \"id\": 1,\n                   \"name\": \"上海\"\n               },\n           ]\n           \"school_nature\": [//学校性质\n               {\n                   \"id\": 1,\n                   \"name\": \"综合\"\n               },\n           ],\n           \"school_arrangement\": [//学校层次    \n               {\n                   \"id\": 1,\n                   \"name\": \"本科\"\n               },\n           ]\n\n       }\n      \"school_filter\": {//专业\n         \"major_list\": {\n           \"专科\": { //一级\n               \"资源环境与安全大类\": {/// 二级\n                   \"煤炭类\": [//三级\n                       {\n                           \"id\": 1,\n                           \"name\": \"煤层气采输技术\" //四级\n                       },\n                   ]\n               }\n           }\n      }\n      \"probability_filter\": {//录取概率\n         \"admissions_probability\": [\n               {\n                   \"id\": 1,\n                   \"name\": \"冲\"\n               },\n               {\n                   \"id\": 2,\n                  \"name\": \"保\"\n               },\n               {\n                   \"id\": 3,\n                   \"name\": \"稳\"\n               },\n               {\n                   \"id\": 4,\n                   \"name\": \"垫\"\n               }\n           ]\n      }\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Filter.php",
    "groupTitle": "Filter",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//filters_option"
      }
    ]
  },
  {
    "type": "get",
    "url": "/insurances/{id}",
    "title": "保险 - 详情",
    "version": "1.0.0",
    "name": "getInsurancesDetail",
    "group": "Insurances",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>保险ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>保险ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "uid",
            "description": "<p>保险人</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "service_id",
            "description": "<p>保险服务ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "wish_id",
            "description": "<p>志愿ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:正常2:关闭3:理赔]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 1,\n    \"uid\": 1,\n    \"service_id\": 1,\n    \"wish_id\": 1,\n    \"status\": \"1\",\n    \"created_at\": \"2016-10-18 11:43:17\"\n  },\n  \"token\": \"token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Insurance.php",
    "groupTitle": "Insurances",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//insurances/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/insurances/{id}/indemnity",
    "title": "保险 - 理赔",
    "version": "1.0.0",
    "name": "getInsurancesIndemnity",
    "group": "Insurances",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>保险ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 1,\n    \"uid\": 1,\n    \"service_id\": 1,\n    \"wish_id\": 1,\n    \"status\": \"1\",\n    \"created_at\": \"2016-10-18 11:43:17\"\n  },\n  \"token\": \"token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Insurance.php",
    "groupTitle": "Insurances",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//insurances/{id}/indemnity"
      }
    ]
  },
  {
    "type": "get",
    "url": "/insurances?limit={limit}&page={page}",
    "title": "保险 - 列表",
    "version": "1.0.0",
    "name": "insurancesList",
    "group": "Insurances",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>保险ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "uid",
            "description": "<p>保险人</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "service_id",
            "description": "<p>保险服务ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "wish_id",
            "description": "<p>志愿ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:正常2:关闭3:理赔]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 1,\n        \"uid\": 1,\n        \"service_id\": 1,\n        \"wish_id\": 1,\n        \"status\": \"1\",\n        \"created_at\": \"2016-10-18 11:43:17\"\n      }\n    ],\n    \"total\": 2\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Insurance.php",
    "groupTitle": "Insurances",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//insurances?limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/insurances",
    "title": "保险 - 创建",
    "version": "1.0.0",
    "name": "insurancesLogin",
    "group": "Insurances",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "wish_id",
            "description": "<p>志愿ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "service_id",
            "description": "<p>服务ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"wish_id\": \"1\",\n  \"service_id\": \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Insurance.php",
    "groupTitle": "Insurances",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//insurances"
      }
    ]
  },
  {
    "type": "get",
    "url": "/majors/{id}",
    "title": "专业 - 详情",
    "version": "1.0.0",
    "name": "getMajorDetail",
    "group": "Major",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>专业ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "degree_type",
            "description": "<p>学历类型</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_type",
            "description": "<p>专业所属系别</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_category",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "introduce",
            "description": "<p>专业介绍</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "graduation_salary",
            "description": "<p>毕业平均薪酬</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "two_year_salary",
            "description": "<p>毕业两年平均薪酬</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "five_year_salary",
            "description": "<p>毕业五年平均薪酬</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "ten_year_salary",
            "description": "<p>毕业十年平均薪酬</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "male_ratio",
            "description": "<p>男比例</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "female_ratio",
            "description": "<p>女比例</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "industry",
            "description": "<p>行业去向</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "job",
            "description": "<p>岗位去向</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "json",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>是否已收藏</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 1,\n        \"name\": \"煤层气采输技术\",\n        \"degree_type\": \"专科\",\n        \"major_type\": \"资源环境与安全大类\",\n        \"major_category\": \"煤炭类\",\n        \"introduce\": \"无\",\n        \"graduation_salary\": null,\n        \"two_year_salary\": null,\n        \"five_year_salary\": 0,\n        \"ten_year_salary\": null,\n        \"male_ratio\": 0,\n        \"female_ratio\": 0,\n        \"industry\": [\n          {\n            \"name\": \"航天航空\",\n            \"ratio\": \"14.04\"\n          }\n        ],\n        \"job\": [\n          {\n            \"name\": \"航天航空\",\n            \"ratio\": \"14.04\"\n          }\n        ],\n        \"json\": \"{\\\"params\\\":{\\\"major_id\\\":\\\"576ce15ac62e9c35de92c8a3\\\",\\\"_major_id\\\":\\\"576ce15ac62e9c35de92c8a3\\\",\\\"diploma\\\":5,\\\"diploma_id\\\":5},\\\"intro\\\":\\\"\\\",\\\"excellent\\\":[],\\\"employment\\\":{\\\"salary\\\":[],\\\"countrySal\\\":[],\\\"ind\\\":[],\\\"zhineng_dis\\\":[],\\\"loc\\\":[]}}\",\n        \"is_favorite\": true\n      }\n    ]\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Major.php",
    "groupTitle": "Major",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//majors/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/majors",
    "title": "专业 - 列表(所有)",
    "version": "1.0.0",
    "name": "majorList",
    "group": "Major",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>专业名称</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"专科\": {\n      \"资源环境与安全大类\":{\n        \"煤炭类\": [\n          {\n            \"id\": 1\n            \"name\": \"煤层气采输技术\",\n          },\n          {\n            \"id\": 231\n            \"name\": \"矿井建设\",\n          }\n        ],\n        \"测绘地理信息类\": [\n          {\n            \"id\": 31\n            \"name\": \"摄影测量与遥感技术\",\n          },\n          {\n            \"id\": 45\n            \"name\": \"测绘工程技术\",\n          }\n        ]\n      }\n    },\n    \"本科\": {\n      \"工学\": {\n        \"公安技术类\": [\n          {\n            \"id\": 5\n            \"name\": \"核生化消防\",\n          },\n          {\n            \"id\": 74\n            \"name\": \"抢险救援指挥与技术\",\n          }\n        ],\n        \"建筑类\": [\n          {\n            \"id\": 24\n            \"name\": \"风景园林\",\n          },\n          {\n            \"id\": 338\n            \"name\": \"历史建筑保护工程\",\n          }\n        ]\n      }\n    }\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Major.php",
    "groupTitle": "Major",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//majors"
      }
    ]
  },
  {
    "type": "get",
    "url": "/news/{id}",
    "title": "文章 - 详情",
    "version": "1.0.0",
    "name": "getNewsDetail",
    "group": "News",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>文章ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "category_id",
            "description": "<p>分类id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>标题</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "is_top",
            "description": "<p>是否置顶</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "category",
            "description": "<p>分类名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"id\": \"1\",\n    \"category_id\": 1,\n    \"title\": \"111\",\n    \"content\": \"434234241432141\",\n    \"is_top\": \"1\",\n    \"created_at\": \"2016-10-18 11:43:17\"\n    \"category\": \"Hello\"\n  },\n  \"token\": \"token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/News.php",
    "groupTitle": "News",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//news/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/news?limit={limit}&page={page}",
    "title": "文章 - 列表",
    "version": "1.0.0",
    "name": "newsList",
    "group": "News",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "category_id",
            "description": "<p>分类id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>标题</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "is_top",
            "description": "<p>是否置顶</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": \"1\",\n        \"category_id\": 1,\n        \"title\": \"111\",\n        \"content\": \"434234241432141\",\n        \"is_top\": \"1\",\n        \"created_at\": \"2016-10-18 11:43:17\"\n      },\n      {\n        \"id\": \"1\",\n        \"category_id\": 1,\n        \"title\": \"111\",\n        \"content\": \"434234241432141\",\n        \"is_top\": \"1\",\n        \"created_at\": \"2016-10-18 11:43:17\"\n      }\n    ],\n    \"total\": 2\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/News.php",
    "groupTitle": "News",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//news?limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/notices",
    "title": "通知 - 创建",
    "version": "1.0.0",
    "name": "noticeCreated",
    "group": "Notice",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "uid",
            "description": "<p>通知人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>通知内容</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"uid\": 1,\n  \"content\": \".........\",\n  \"version\": \"1.0.0\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Notice.php",
    "groupTitle": "Notice",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//notices"
      }
    ]
  },
  {
    "type": "get",
    "url": "/notices/{id}/delete",
    "title": "通知 - 删除",
    "version": "1.0.0",
    "name": "noticeDelete",
    "group": "Notice",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>通知ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Notice.php",
    "groupTitle": "Notice",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//notices/{id}/delete"
      }
    ]
  },
  {
    "type": "get",
    "url": "/notices/{id}",
    "title": "通知 - 详情",
    "version": "1.0.0",
    "name": "noticeDetail",
    "group": "Notice",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>通知 ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>通知 ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "uid",
            "description": "<p>通知对象ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>通知内容</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:未读, 2:已读, 3:删除]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"id\": 1,\n    \"uid\": 1,\n    \"content\": \"........\",\n    \"status\": 1,\n    \"created_at\": \"2017-01-13 04:10:10\"\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Notice.php",
    "groupTitle": "Notice",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//notices/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/notices?limit={limit}&page={page}",
    "title": "通知 - 列表",
    "version": "1.0.0",
    "name": "noticeList",
    "group": "Notice",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>通知ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "uid",
            "description": "<p>通知对象ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>通知内容</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:未读；2:已读；3:删除]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "updated_at",
            "description": "<p>更新时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 1,\n        \"uid\": 1,\n        \"content\": \"........\",\n        \"status\": 1,\n        \"created_at\": \"2017-08-20 09:20:04\"\n        \"updated_at\": \"2017-08-20 09:20:04\"\n      }\n    ],\n    \"total\": 1\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Notice.php",
    "groupTitle": "Notice",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//notices?limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/plans/{id}",
    "title": "招生计划 - 详情",
    "version": "1.0.0",
    "name": "planDetail",
    "group": "Plan",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>招生计划 ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "school_id",
            "description": "<p>学校ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "year",
            "description": "<p>年份</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "school_code",
            "description": "<p>学校代码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "school_logo",
            "description": "<p>学校LOGO</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "score",
            "description": "<p>分数线</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "recruit_count",
            "description": "<p>招生人数</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>是否已收藏</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"school_id\": 1,\n      \"school_name\": \"晋中职业技术学院\",\n      \"school_code\": \"\",\n      \"school_logo\": null,\n      \"year\": 2014,\n      \"score\": 452,\n      \"recruit_count\": 100,\n      \"is_favorite\": false,\n      \"recruit\": [\n        {\n          \"major_id\": 1,\n          \"major_name\": \"煤层气采输技术\",\n          \"major_code\": \"4\",\n          \"recruit_count\": 50,\n          \"school_year\": 4,\n          \"tuition\": 6000\n        }\n      ]\n    }\n  ],\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Plan.php",
    "groupTitle": "Plan",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//plans/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/plans/filters",
    "title": "招生计划 - 获取筛选条件",
    "version": "1.0.0",
    "name": "planFilers",
    "group": "Plan",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"year\": [\n      {\n        \"year\": 2017\n      }\n    ],\n    \"subject\": [\n      {\n        \"subject\": \"文科\"\n      },\n      {\n        \"subject\": \"理科\"\n      }\n    ],\n    \"recruit_province\": [\n      {\n        \"province_name\": \"上海\"\n      },\n      {\n        \"province_name\": \"云南\"\n      }\n    ],\n    \"school_province\": [\n      {\n        \"province_name\": \"上海\"\n      },\n      {\n        \"province_name\": \"云南\"\n      }\n    ],\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Plan.php",
    "groupTitle": "Plan",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//plans/filters"
      }
    ]
  },
  {
    "type": "get",
    "url": "/plans?year={year}&subject={subject}&recruit_province={recruit_province}&school_province={school_province}&limit={limit}&page={page}",
    "title": "招生计划 - 列表",
    "version": "1.0.0",
    "name": "planList",
    "group": "Plan",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "year",
            "description": "<p>年份</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "subject",
            "description": "<p>科目类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "recruit_province",
            "description": "<p>招生地区</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_province",
            "description": "<p>学校位置</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>招生计划ID</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "year",
            "description": "<p>年份</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "school_code",
            "description": "<p>学校代码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "school_logo",
            "description": "<p>学校LOGO</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "score",
            "description": "<p>分数线</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "recruit_count",
            "description": "<p>招生人数</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>是否已收藏</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 1,\n        \"school_code\": 10001,\n        \"school_name\": '北京大学',\n        \"school_logo\": null,\n        \"year\": 2017,\n        \"score\": 453,\n        \"recruit_count\": 200,\n        \"is_favorite\": true\n      }\n    ],\n    \"total\": 1\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Plan.php",
    "groupTitle": "Plan",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//plans?year={year}&subject={subject}&recruit_province={recruit_province}&school_province={school_province}&limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/probability",
    "title": "录取概率",
    "version": "1.0.0",
    "name": "probability",
    "group": "Probability",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "school_id",
            "description": "<p>学校id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "user_score",
            "description": "<p>分数</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_major_name",
            "description": "<p>专业名称</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>[作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n   \"data\": {\n            \"admissionsProbability\": 录取概率,\n            \"filingProbability\": 投档概率\n        },\n }\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Probability.php",
    "groupTitle": "Probability",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//probability"
      }
    ]
  },
  {
    "type": "post",
    "url": "probability/filters",
    "title": "录取录取概率筛选项",
    "version": "1.0.0",
    "name": "probability_filters",
    "group": "Probability",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称关键字</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>[作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n     \"code\": 200,\n     \"message\": \"Success\",\n     \"data\": [\n         {\n             \"school_id\": 学校id,\n             \"school_name\": 学校名称,\n             \"major_list\": [\n                 {\n                    \"major_id\": 专业id,\n                     \"major_name\": 专业名称,\n                     \"ranking\": 专业排名\n                 },\n                ....\n             ]\n          }\n          ....\n      ]\n     \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Probability.php",
    "groupTitle": "Probability",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn/probability/filters"
      }
    ]
  },
  {
    "type": "get",
    "url": "/province_scores/filters",
    "title": "省控线 - 获取筛选条件",
    "version": "1.0.0",
    "name": "provinceScoreFilers",
    "group": "ProvinceScore",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"province\": [\n      {\n        \"province_name\": \"上海\"\n      },\n      {\n        \"province_name\": \"云南\"\n      }\n    ],\n    \"year\": [\n      {\n        \"year\": 2017\n      }\n    ],\n    \"batch\": [\n      {\n        \"batch\": \"本科一批\"\n      }\n    ],\n    \"subject\": [\n      {\n        \"subject\": \"文科\"\n      },\n      {\n        \"subject\": \"理科\"\n      }\n    ]\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/ProvinceScore.php",
    "groupTitle": "ProvinceScore",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//province_scores/filters"
      }
    ]
  },
  {
    "type": "get",
    "url": "/province_scores?province_name={province_name}&year={year}&batch={batch}&subject={subject}&limit={limit}&page={page}",
    "title": "省控线 - 列表",
    "version": "1.0.0",
    "name": "provinceScoreList",
    "group": "ProvinceScore",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "province_name",
            "description": "<p>省份</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "year",
            "description": "<p>年份</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "batch",
            "description": "<p>批次(一本/二本/专科..)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "subject",
            "description": "<p>科目类型</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "province_name",
            "description": "<p>省份</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "year",
            "description": "<p>年份</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "subject",
            "description": "<p>科目类型</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "score",
            "description": "<p>分数线</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "batch",
            "description": "<p>批次(一本/二本/专科..)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"province_name\": '北京',\n        \"year\": 2017,\n        \"subject\": \"理科\",\n        \"score\": 499,\n        \"batch\": \"本科一批\"\n      }\n    ],\n    \"total\": 2\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/ProvinceScore.php",
    "groupTitle": "ProvinceScore",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//province_scores?province_name={province_name}&year={year}&batch={batch}&subject={subject}&limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/schools/search/plan?name={name}&province_id={province_id}&limit={limit}&page={page}",
    "title": "学校 - 招生计划搜索",
    "version": "1.0.0",
    "name": "SearchPlanList",
    "group": "School",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "province_id",
            "description": "<p>所在省份id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "limit",
            "description": "<p>每页个数默认15</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "page",
            "description": "<p>当前页默认1</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>招生计划id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "school_code",
            "description": "<p>学校标识码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "subject",
            "description": "<p>文理科</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "batch",
            "description": "<p>批次</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "recruit_count",
            "description": "<p>招生计划数量</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "score",
            "description": "<p>投档线</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n      \"id\": 5,\n      \"school_code\": \"231\",\n      \"school_name\": \"河套学院\",\n      \"subject\": \"理科\",\n      \"batch\": \"本科第一批\",\n      \"recruit_count\": 758,\n      \"min_score\": 466\n      }\n    ],\n    \"total\": 1\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/School.php",
    "groupTitle": "School",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//schools/search/plan?name={name}&province_id={province_id}&limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/school_majors/{id}/detail",
    "title": "学校专业 - 详情",
    "version": "1.0.0",
    "name": "getSchoolMajorsDetail",
    "group": "School_Majors",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>专业ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "degree_type",
            "description": "<p>学历类型</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_type",
            "description": "<p>专业所属系别</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_category",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "introduce",
            "description": "<p>专业介绍</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "graduation_salary",
            "description": "<p>毕业平均薪酬</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "two_year_salary",
            "description": "<p>毕业两年平均薪酬</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "five_year_salary",
            "description": "<p>毕业五年平均薪酬</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "ten_year_salary",
            "description": "<p>毕业十年平均薪酬</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "male_ratio",
            "description": "<p>男生占比</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "female_ratio",
            "description": "<p>女生占比</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "json",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 1,\n        \"name\": \"煤层气采输技术\",\n        \"degree_type\": \"专科\",\n        \"major_type\": \"资源环境与安全大类\",\n        \"major_category\": \"煤炭类\",\n        \"introduce\": \"无\",\n        \"graduation_salary\": null,\n        \"two_year_salary\": null,\n        \"five_year_salary\": 0,\n        \"ten_year_salary\": null,\n        \"male_ratio\": 0,\n        \"female_ratio\": 0,\n        \"json\": \"{\\\"params\\\":{\\\"major_id\\\":\\\"576ce15ac62e9c35de92c8a3\\\",\\\"_major_id\\\":\\\"576ce15ac62e9c35de92c8a3\\\",\\\"diploma\\\":5,\\\"diploma_id\\\":5},\\\"intro\\\":\\\"\\\",\\\"excellent\\\":[],\\\"employment\\\":{\\\"salary\\\":[],\\\"countrySal\\\":[],\\\"ind\\\":[],\\\"zhineng_dis\\\":[],\\\"loc\\\":[]}}\"\n      }\n    ]\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/SchoolMajors.php",
    "groupTitle": "School_Majors",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//school_majors/{id}/detail"
      }
    ]
  },
  {
    "type": "get",
    "url": "/school_majors/{id}/delete",
    "title": "学校专业 - 删除",
    "version": "1.0.0",
    "name": "schoolMajorDelete",
    "group": "School_Majors",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校专业ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/SchoolMajors.php",
    "groupTitle": "School_Majors",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//school_majors/{id}/delete"
      }
    ]
  },
  {
    "type": "get",
    "url": "/school_majors/{id}?limit={limit}&page={page}",
    "title": "学校专业 - 列表",
    "version": "1.0.0",
    "name": "schoolMajorList",
    "group": "School_Majors",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校ID</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_name",
            "description": "<p>专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "school_id",
            "description": "<p>学校ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "major_id",
            "description": "<p>专业ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "degree_type",
            "description": "<p>学历类型</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "major_type",
            "description": "<p>专业所属系别</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "url",
            "description": "<p>链接</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:显示2:删除]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 1,\n        \"school_id\": 1,\n        \"school_name\": \"北京大学\",\n        \"major_id\": 20,\n        \"major_name\": \"会计专业\",\n        \"degree_type\": \"本科\",\n        \"major_type\": \"经济管理系\",\n        \"url\": \"www.xxx.com\",\n        \"status\": 1,\n        \"created_at\": \"2017-08-20 09:20:04\"\n      }\n    ],\n    \"total\": 1\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/SchoolMajors.php",
    "groupTitle": "School_Majors",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//school_majors/{id}?limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/school_majors",
    "title": "学校专业 - 创建",
    "version": "1.0.0",
    "name": "schoolMajorsFill",
    "group": "School_Majors",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "school_id",
            "description": "<p>学校ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school_name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "major_id",
            "description": "<p>专业ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "major_name",
            "description": "<p>专业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "degree_type",
            "description": "<p>学历类型</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "major_type",
            "description": "<p>专业所属系别</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "url",
            "description": "<p>链接</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"school_id\": 1,\n  \"school_name\": \"北京大学\",\n  \"major_id\": 20,\n  \"major_name\": \"会计专业\",\n  \"degree_type\": \"本科\",\n  \"major_type\": \"经济管理系\",\n  \"url\": \"www.xxx.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/SchoolMajors.php",
    "groupTitle": "School_Majors",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//school_majors"
      }
    ]
  },
  {
    "type": "get",
    "url": "/schools/admissions/{id}",
    "title": "学校 - 院校前年投档线和录取概率",
    "version": "1.0.0",
    "name": "admissions",
    "group": "School",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校 ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "throwSchoolLine",
            "description": "<p>上一年投档线</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "filingProbability",
            "description": "<p>投档概率</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "filingProbabilityText",
            "description": "<p>概率信息</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    {\n       \"throwSchoolLine\": 668,\n       \"filingProbability\": \"33%\",\n       \"filingProbabilityText\": \"几乎不可能\"\n    },\n\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/School.php",
    "groupTitle": "School",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//schools/admissions/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/schools/filingLine/{id}",
    "title": "学校 - 院校投档线",
    "version": "1.0.0",
    "name": "filingLine",
    "group": "School",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校 ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "year",
            "description": "<p>年份</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "province_filing_line",
            "description": "<p>投档线</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "min_score",
            "description": "<p>最低分</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": [\n    {\n     {\n         \"min_score\": 644,\n         \"province_filing_line\": 644,\n         \"year\": 2013\n    },\n    .....\n  ],\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/School.php",
    "groupTitle": "School",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//schools/filingLine/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/schools/majorScore/{id}/{year:\\d+}",
    "title": "学校 - 专业分数线",
    "version": "1.0.0",
    "name": "majorScore",
    "group": "School",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校 ID</p>"
          },
          {
            "group": "Parameter",
            "type": "year",
            "optional": false,
            "field": "year",
            "description": "<p>年份</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "school_major_id",
            "description": "<p>学校专业 ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_code",
            "description": "<p>学校专业代码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_name",
            "description": "<p>学校专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "min_score",
            "description": "<p>最低分</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "max_score",
            "description": "<p>最高分</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "average_score",
            "description": "<p>平均分</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "enrollment_count",
            "description": "<p>实际招生人数</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": [\n    {\n      \"major_code\": null,\n      \"school_major_id\": 58,\n      \"subject\": \"文科\",\n      \"school_major_name\": \"工业工程\",\n      \"min_score\": 682,\n      \"max_score\": 686,\n      \"average_score\": 0,\n      \"enrollment_count\": 0\n    },\n     ....\n  ],\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/School.php",
    "groupTitle": "School",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//schools/majorScore/{id}/{year:\\d+}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/schools/majors/{id}",
    "title": "学校 - 在招专业",
    "version": "1.0.0",
    "name": "majors",
    "group": "School",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校 ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "school_id",
            "description": "<p>学校专业 ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_id",
            "description": "<p>学校专业代码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "major_name",
            "description": "<p>学校专业名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "degree_type",
            "description": "<p>学位类型</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "introduce",
            "description": "<p>简介</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": [\n    {\n      \"id\": 1,\n      \"school_id\": 1,\n      \"major_id\": 0,\n      \"major_name\": \"航空航天工程(定向航空)\",\n      \"degree_type\": null,\n      \"introduce\": null\n    },\n     ....\n  ],\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/School.php",
    "groupTitle": "School",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//schools/majors/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/schools/{id}",
    "title": "学校 - 详情",
    "version": "1.0.0",
    "name": "schoolDetail",
    "group": "School",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校 ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "old_name",
            "description": "<p>原校名</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "logo",
            "description": "<p>LOGO</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>学校代码</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_985",
            "description": "<p>是否985高校</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_211",
            "description": "<p>是否211高校</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>学校类型 [ 公立, 民办 ]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "property",
            "description": "<p>学校性质 [ 一本, 二本, 三本, 专科 ... ]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "subject_type",
            "description": "<p>学科类型 [ 综合院校, 工科院校, 艺术院校 ... ]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "education",
            "description": "<p>学历类型 [ 本科/专科, 本科, 专科 ... ]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "male_ratio",
            "description": "<p>男生占比</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "female_ratio",
            "description": "<p>女生占比</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "website",
            "description": "<p>学校官网</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>招生电话</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "comprehensive_rank",
            "description": "<p>综合排名</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "founding_year",
            "description": "<p>建校时间</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>详细地址</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "introduce",
            "description": "<p>简介</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>是否已收藏</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"name\": \"清华大学\",\n    \"old_name\": \"\",\n    \"logo\": \"http://school-icon.b0.upaiyun.com/52ac2e99747aec013fcf4e6f.jpg\",\n    \"code\": \"\",\n    \"is_985\": 1,\n    \"is_211\": 1,\n    \"type\": \"公立\",\n    \"property\": \"一本\",\n    \"subject_type\": \"工科院校\",\n    \"education\": \"本科\",\n    \"male_ratio\": 75,\n    \"female_ratio\": 25,\n    \"website\": \"http://www.tsinghua.edu.cn\",\n    \"phone\": \"招生电话：010-62770334；010-62782051\",\n    \"comprehensive_rank\": 1,\n    \"founding_year\": 1911,\n    \"address\": \"北京市海淀区清华大学\",\n    \"introduce\": \"清华大学概览...\",\n    \"is_favorite\": false\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/School.php",
    "groupTitle": "School",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//schools/{id}"
      }
    ]
  },
  {
    "type": "get",
    "url": "/schools/filters",
    "title": "学校 - 获取筛选条件",
    "version": "1.0.0",
    "name": "schoolFilers",
    "group": "School",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"type\": [\n      {\n        \"type\": \"公办\"\n      },\n      {\n        \"type\": \"私立\"\n      }\n    ],\n    \"subject_type\": [\n      {\n        \"subject_type\": \"体育院校\"\n      },\n      {\n        \"subject_type\": \"农业院校\"\n      },\n     {\n        \"subject_type\": \"医药院校\"\n      }\n    ],\n    \"property\": [\n      {\n        \"property\": \"211\"\n      },\n      {\n        \"property\": \"双一流 985 211\"\n      },\n     {\n        \"property\": \"医药\"\n      }\n    ],\n    \"province\": [\n      {\n        \"province_name\": \"上海\"\n      },\n      {\n        \"province_name\": \"云南\"\n      }\n    ]\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/School.php",
    "groupTitle": "School",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//schools/filters"
      }
    ]
  },
  {
    "type": "get",
    "url": "/schools?type={type}&subject_type={subject_type}&property={property}&province_name={province_name}&limit={limit}&page={page}",
    "title": "学校 - 列表",
    "version": "1.0.0",
    "name": "schoolList",
    "group": "School",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>学校类型 [ 公立, 民办 ]</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "subject_type",
            "description": "<p>学科类型 [ 综合院校, 工科院校, 艺术院校 ... ]</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "property",
            "description": "<p>学校性质 [ 一本, 二本, 三本, 专科 ... ]</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "province_name",
            "description": "<p>所在省市</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>每页个数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>学校ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>学校名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "logo",
            "description": "<p>LOGO</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_985",
            "description": "<p>是否985高校</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_211",
            "description": "<p>是否211高校</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "comprehensive_rate",
            "description": "<p>综合评级</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "archive_year",
            "description": "<p>投档年份</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "archive_province",
            "description": "<p>投档省市</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "archive_score",
            "description": "<p>投档线</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "archive_probability",
            "description": "<p>投档概率</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>学校类型 [ 公立, 民办 ]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "subject_type",
            "description": "<p>学科类型 [ 综合院校, 工科院校, 艺术院校 ... ]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "property",
            "description": "<p>学校性质 [ 一本, 二本, 三本, 专科 ... ]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "education",
            "description": "<p>学历类型 [ 本科/专科, 本科, 专科 ... ]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "province_name",
            "description": "<p>所在省市</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>详细地址</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "male_ratio",
            "description": "<p>男生占比</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "female_ratio",
            "description": "<p>女生占比</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "master_point",
            "description": "<p>硕士点</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "doctoral_point",
            "description": "<p>博士点</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>是否已收藏</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"\",\n  \"data\": {\n    \"list\": [\n      {\n        \"id\": 2020,\n        \"name\": \"长春东方职业学院\",\n        \"logo\": null,\n        \"is_985\": 0,\n        \"is_211\": 0,\n        \"comprehensive_rate\": \"A\",\n        \"archive_year\": 2017,\n        \"archive_province\": \"长春\",\n        \"archive_score\": 555,\n        \"archive_probability\": \"40%\",\n        \"type\": \"私立\",\n        \"subject_type\": \"医药院校\",\n        \"property\": \"一本\",\n        \"education\": \"专科\",\n        \"province_name\": \"吉林\",\n        \"address\": \"长春市净月旅游经济开发区新大学城路\",\n        \"male_ratio\": 42,\n        \"female_ratio\": 58,\n        \"master_point\": null,\n        \"doctoral_point\": null,\n        \"is_favorite\": true\n      }\n    ],\n    \"total\": 1\n  },\n  \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/School.php",
    "groupTitle": "School",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//schools?type={type}&subject_type={subject_type}&property={property}&province_name={province_name}&limit={limit}&page={page}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/shares",
    "title": "分享",
    "version": "1.0.0",
    "name": "createShare",
    "group": "Share",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>类型：wechat-&gt;微信；qq-&gt;QQ</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>标题</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "image",
            "description": "<p>缩略图</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "link",
            "description": "<p>链接</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"type\": \"qq\",\n  \"title\": \"QQ Share\",\n  \"content\": \"QQ Share Content\",\n  \"image\": \"QQ Share Image\",\n  \"link\": \"QQ Share Link\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<blockquote> <p>0</p> </blockquote>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1\n  \"token\": \"eyJpdiI6InJhTklZWlArUzFIS25LdnQycFJHUFE9PSIsInZhbHVlIjoiTHVXTUo2bkR5d29Nd3BkQ2ZHN0JBampLalpDRDM0emNqaUdUOG9aNlZRT0hOTDVsbDVBTEluXC9hWVZVSStoSlEiLCJtYWMiOiI2Y2MxZDA4MjFkMWIxNWI0MzM4NGQwMDM3ODcxNTU5ZGVlOWI2OTQwMDM1NmYwZWFmZDlmMThkMjc0ZGQ1NmExIn0=\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/Share.php",
    "groupTitle": "Share",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//shares"
      }
    ]
  },
  {
    "type": "post",
    "url": "/users/avatar",
    "title": "用户 - 修改头像",
    "version": "1.0.0",
    "name": "userAvatar",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "avatar",
            "description": "<p>头像文件</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<p>[ &gt; 0 验证成功]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"avatar\":\"http://www.yhzgedu.com/user/10/avatar/1533624280.jpg\"\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/User.php",
    "groupTitle": "Users",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//users/avatar"
      }
    ]
  },
  {
    "type": "get",
    "url": "/users/{id}",
    "title": "用户 - 详情",
    "version": "1.0.0",
    "name": "userDetail",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>用户 ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>用户 ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>电话号码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "real_name",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "avatar",
            "description": "<p>图像</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "sex",
            "description": "<p>性别[1:男, 2:女]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "province_id",
            "description": "<p>省份</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "subject",
            "description": "<p>科目[1:文科, 2:理科]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "score",
            "description": "<p>分数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:正常, 2:锁定, 3:禁用]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "student_id",
            "description": "<p>学生证号</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "memo",
            "description": "<p>备注[简介]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"id\": 1,\n    \"phone\": \"15157123839\",\n    \"real_name\": \"黄朝森\",\n    \"nickname\": \"黄易\",\n    \"avatar\": \"http://www.yhzgedu.com/user/10/avatar/1533624280.jpg\",\n    \"sex\": 1,\n    \"province_id\": 1,\n    \"subject\": 1,\n    \"score\": null,\n    \"status\": 1,\n    \"student_id\": \"201400020125\",\n    \"address\": \"十堰市郧西县\",\n    \"memo\": null,\n    \"created_at\": \"2017-01-13 04:10:10\"\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/User.php",
    "groupTitle": "Users",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//users/{id}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/users/info",
    "title": "用户 - 修改资料",
    "version": "1.0.0",
    "name": "userInfo",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sex",
            "description": "<p>性别[1:男, 2:女]</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "province_id",
            "description": "<p>省份</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "subject",
            "description": "<p>科目[1:文科, 2:理科]</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "score",
            "description": "<p>分数</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "real_name",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "student_id",
            "description": "<p>学生证号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "file",
            "optional": false,
            "field": "avatar",
            "description": "<p>头像文件</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"nickname\": \"黄易\",\n  \"sex\": \"1\",\n  \"province_id\": \"1\",\n  \"subject\": \"1\",\n  \"score\": \"666\",\n  \"real_name\": \"黄朝森\",\n  \"student_id\": \"201421356985\",\n  \"address\": \"十堰市郧西县\",\n  \"avatar\": avatar file\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>用户 ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>电话号码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "real_name",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "avatar",
            "description": "<p>图像</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "sex",
            "description": "<p>性别[1:男, 2:女]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "province_id",
            "description": "<p>省份</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "subject",
            "description": "<p>科目[1:文科, 2:理科]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "score",
            "description": "<p>分数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:正常, 2:锁定, 3:禁用]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "student_id",
            "description": "<p>学生证号</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "memo",
            "description": "<p>备注[简介]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"id\": 1,\n    \"phone\": \"15157123839\",\n    \"real_name\": \"黄朝森\",\n    \"nickname\": \"黄易\",\n    \"avatar\": \"http://www.yhzgedu.com/user/10/avatar/1533624280.jpg\",\n    \"sex\": 1,\n    \"province_id\": 1,\n    \"subject\": 1,\n    \"score\": null,\n    \"status\": 1,\n    \"student_id\": \"201400020125\",\n    \"address\": \"十堰市郧西县\",\n    \"memo\": null,\n    \"created_at\": \"2017-01-13 04:10:10\"\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/User.php",
    "groupTitle": "Users",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//users/info"
      }
    ]
  },
  {
    "type": "post",
    "url": "/login",
    "title": "用户 - 登录",
    "version": "1.0.0",
    "name": "userLogin",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>验证码</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"phone\": \"15111223344\",\n  \"code\": \"785623\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>用户 ID</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>电话号码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "real_name",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "avatar",
            "description": "<p>图像</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "sex",
            "description": "<p>性别[1:男, 2:女]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "province_id",
            "description": "<p>省份</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "subject",
            "description": "<p>科目[1:文科, 2:理科]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "score",
            "description": "<p>分数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态[1:正常, 2:锁定, 3:禁用]</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "student_id",
            "description": "<p>学生证号</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "memo",
            "description": "<p>备注[简介]</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "created_at",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>授权验证 token [作为接口授权验证，通过 Header 方式传递]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": {\n    \"id\": 1,\n    \"phone\": \"15157123839\",\n    \"real_name\": \"黄朝森\",\n    \"nickname\": \"黄易\",\n    \"avatar\": \"http://www.yhzgedu.com/user/10/avatar/1533624280.jpg\",\n    \"sex\": 1,\n    \"province_id\": 1,\n    \"subject\": 1,\n    \"score\": null,\n    \"status\": 1,\n    \"student_id\": \"201400020125\",\n    \"address\": \"十堰市郧西县\",\n    \"memo\": null,\n    \"created_at\": \"2017-01-13 04:10:10\"\n  },\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/User.php",
    "groupTitle": "Users",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//login"
      }
    ]
  },
  {
    "type": "get",
    "url": "/logout",
    "title": "用户 - 退出",
    "version": "1.0.0",
    "name": "userLogout",
    "group": "Users",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "data",
            "description": "<p>[ &gt; 0 验证成功]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 200,\n  \"message\": \"Success\",\n  \"data\": 1,\n  \"token\": \"eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ==\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/pc_project/wish/public/doc/api/User.php",
    "groupTitle": "Users",
    "sampleRequest": [
      {
        "url": "http://api.yhzgly.cn//logout"
      }
    ]
  }
] });
