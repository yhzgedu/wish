<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {post} /shares  分享
 * @apiVersion 1.0.0
 * @apiName createShare
 * @apiGroup Share
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {string}       type               类型：wechat->微信；qq->QQ
 * @apiParam {string}       title              标题
 * @apiParam {string}       content            内容
 * @apiParam {string}       image              缩略图
 * @apiParam {string}       link               链接
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "type": "qq",
 *       "title": "QQ Share",
 *       "content": "QQ Share Content",
 *       "image": "QQ Share Image",
 *       "link": "QQ Share Link",
 *     }
 *
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1
 *       "token": "eyJpdiI6InJhTklZWlArUzFIS25LdnQycFJHUFE9PSIsInZhbHVlIjoiTHVXTUo2bkR5d29Nd3BkQ2ZHN0JBampLalpDRDM0emNqaUdUOG9aNlZRT0hOTDVsbDVBTEluXC9hWVZVSStoSlEiLCJtYWMiOiI2Y2MxZDA4MjFkMWIxNWI0MzM4NGQwMDM3ODcxNTU5ZGVlOWI2OTQwMDM1NmYwZWFmZDlmMThkMjc0ZGQ1NmExIn0="
 *     }
 */