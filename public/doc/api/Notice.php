<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /notices?limit={limit}&page={page}             通知 - 列表
 * @apiVersion 1.0.0
 * @apiName noticeList
 * @apiGroup Notice
 *
 * @apiParam {int}          limit           每页个数
 * @apiParam {int}          page            当前页
 *
 * @apiSuccess {int}        id              通知ID
 * @apiSuccess {int}        uid             通知对象ID
 * @apiSuccess {string}     content         通知内容
 * @apiSuccess {int}        status          状态[1:未读；2:已读；3:删除]
 * @apiSuccess {string}     created_at      创建时间
 * @apiSuccess {string}     updated_at      更新时间
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 1,
 *             "uid": 1,
 *             "content": "........",
 *             "status": 1,
 *             "created_at": "2017-08-20 09:20:04"
 *             "updated_at": "2017-08-20 09:20:04"
 *           }
 *         ],
 *         "total": 1
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {post} /notices              通知 - 创建
 * @apiVersion 1.0.0
 * @apiName noticeCreated
 * @apiGroup Notice
 *
 * @apiParam {int}          uid                 通知人ID
 * @apiParam {string}       content             通知内容
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "uid": 1,
 *       "content": ".........",
 *       "version": "1.0.0"
 *     }
 *
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /notices/{id}/delete            通知 - 删除
 * @apiVersion 1.0.0
 * @apiName noticeDelete
 * @apiGroup Notice
 *
 * @apiParam {string}       id          通知ID
 *
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /notices/{id}            通知 - 详情
 * @apiVersion 1.0.0
 * @apiName noticeDetail
 * @apiGroup Notice
 *
 * @apiParam {int}          id          通知 ID
 *
 * @apiSuccess {int}        id          通知 ID
 * @apiSuccess {int}        uid         通知对象ID
 * @apiSuccess {string}     content     通知内容
 * @apiSuccess {int}        status      状态[1:未读, 2:已读, 3:删除]
 * @apiSuccess {string}     created_at  创建时间
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "id": 1,
 *         "uid": 1,
 *         "content": "........",
 *         "status": 1,
 *         "created_at": "2017-01-13 04:10:10"
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */
