<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /news?limit={limit}&page={page}            文章 - 列表
 * @apiVersion 1.0.0
 * @apiName newsList
 * @apiGroup News
 *
 * @apiParam {int}          limit           每页个数
 * @apiParam {int}          page            当前页
 *
 * @apiSuccess {int}        id              文章ID
 * @apiSuccess {int}        category_id     分类id
 * @apiSuccess {string}     title           标题
 * @apiSuccess {int}        content         内容
 * @apiSuccess {string}     is_top          是否置顶
 * @apiSuccess {string}     created_at      创建时间
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": "1",
 *             "category_id": 1,
 *             "title": "111",
 *             "content": "434234241432141",
 *             "is_top": "1",
 *             "created_at": "2016-10-18 11:43:17"
 *           },
 *           {
 *             "id": "1",
 *             "category_id": 1,
 *             "title": "111",
 *             "content": "434234241432141",
 *             "is_top": "1",
 *             "created_at": "2016-10-18 11:43:17"
 *           }
 *         ],
 *         "total": 2
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {get} /news/{id}             文章 - 详情
 * @apiVersion 1.0.0
 * @apiName getNewsDetail
 * @apiGroup News
 *
 * @apiParam {int}          id              文章ID
 *
 * @apiSuccess {int}        id              文章ID
 * @apiSuccess {int}        category_id     分类id
 * @apiSuccess {string}     title           标题
 * @apiSuccess {int}        content         内容
 * @apiSuccess {string}     is_top          是否置顶
 * @apiSuccess {string}     category        分类名称
 * @apiSuccess {string}     created_at      创建时间
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "id": "1",
 *         "category_id": 1,
 *         "title": "111",
 *         "content": "434234241432141",
 *         "is_top": "1",
 *         "created_at": "2016-10-18 11:43:17"
 *         "category": "Hello"
 *       },
 *       "token": "token"
 *     }
 */
