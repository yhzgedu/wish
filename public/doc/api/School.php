<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /schools?type={type}&subject_type={subject_type}&property={property}&province_name={province_name}&limit={limit}&page={page}     学校 - 列表
 * @apiVersion 1.0.0
 * @apiName schoolList
 * @apiGroup School
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {string}       type                                 学校类型 [ 公立, 民办 ]
 * @apiParam {string}       subject_type                         学科类型 [ 综合院校, 工科院校, 艺术院校 ... ]
 * @apiParam {string}       property                             学校性质 [ 一本, 二本, 三本, 专科 ... ]
 * @apiParam {string}       province_name                        所在省市
 * @apiParam {int}          limit                                每页个数
 * @apiParam {int}          page                                 当前页
 *
 * @apiSuccess {int}        id                                   学校ID
 * @apiSuccess {string}     name                                 学校名称
 * @apiSuccess {string}     logo                                 LOGO
 * @apiSuccess {bool}       is_985                               是否985高校
 * @apiSuccess {bool}       is_211                               是否211高校
 * @apiSuccess {string}     comprehensive_rate                   综合评级
 * @apiSuccess {int}        archive_year                         投档年份
 * @apiSuccess {string}     archive_province                     投档省市
 * @apiSuccess {int}        archive_score                        投档线
 * @apiSuccess {string}     archive_probability                  投档概率
 * @apiSuccess {string}     type                                 学校类型 [ 公立, 民办 ]
 * @apiSuccess {string}     subject_type                         学科类型 [ 综合院校, 工科院校, 艺术院校 ... ]
 * @apiSuccess {string}     property                             学校性质 [ 一本, 二本, 三本, 专科 ... ]
 * @apiSuccess {string}     education                            学历类型 [ 本科/专科, 本科, 专科 ... ]
 * @apiSuccess {string}     province_name                        所在省市
 * @apiSuccess {string}     address                              详细地址
 * @apiSuccess {int}        male_ratio                           男生占比
 * @apiSuccess {int}        female_ratio                         女生占比
 * @apiSuccess {int}        master_point                         硕士点
 * @apiSuccess {int}        doctoral_point                       博士点
 * @apiSuccess {bool}       is_favorite                          是否已收藏
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 2020,
 *             "name": "长春东方职业学院",
 *             "logo": null,
 *             "is_985": 0,
 *             "is_211": 0,
 *             "comprehensive_rate": "A",
 *             "archive_year": 2017,
 *             "archive_province": "长春",
 *             "archive_score": 555,
 *             "archive_probability": "40%",
 *             "type": "私立",
 *             "subject_type": "医药院校",
 *             "property": "一本",
 *             "education": "专科",
 *             "province_name": "吉林",
 *             "address": "长春市净月旅游经济开发区新大学城路",
 *             "male_ratio": 42,
 *             "female_ratio": 58,
 *             "master_point": null,
 *             "doctoral_point": null,
 *             "is_favorite": true
 *           }
 *         ],
 *         "total": 1
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {get} /schools/{id}              学校 - 详情
 * @apiVersion 1.0.0
 * @apiName schoolDetail
 * @apiGroup School
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          id                                   学校 ID
 *
 * @apiSuccess {string}     name                                 学校名称
 * @apiSuccess {string}     old_name                             原校名
 * @apiSuccess {string}     logo                                 LOGO
 * @apiSuccess {string}     code                                 学校代码
 * @apiSuccess {bool}       is_985                               是否985高校
 * @apiSuccess {bool}       is_211                               是否211高校
 * @apiSuccess {string}     type                                 学校类型 [ 公立, 民办 ]
 * @apiSuccess {string}     property                             学校性质 [ 一本, 二本, 三本, 专科 ... ]
 * @apiSuccess {string}     subject_type                         学科类型 [ 综合院校, 工科院校, 艺术院校 ... ]
 * @apiSuccess {string}     education                            学历类型 [ 本科/专科, 本科, 专科 ... ]
 * @apiSuccess {int}        male_ratio                           男生占比
 * @apiSuccess {int}        female_ratio                         女生占比
 * @apiSuccess {string}     website                              学校官网
 * @apiSuccess {string}     phone                                招生电话
 * @apiSuccess {int}        comprehensive_rank                   综合排名
 * @apiSuccess {int}        founding_year                        建校时间
 * @apiSuccess {string}     address                              详细地址
 * @apiSuccess {string}     introduce                            简介
 * @apiSuccess {bool}       is_favorite                          是否已收藏
 * @apiSuccess {string}     token                                授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "name": "清华大学",
 *         "old_name": "",
 *         "logo": "http://school-icon.b0.upaiyun.com/52ac2e99747aec013fcf4e6f.jpg",
 *         "code": "",
 *         "is_985": 1,
 *         "is_211": 1,
 *         "type": "公立",
 *         "property": "一本",
 *         "subject_type": "工科院校",
 *         "education": "本科",
 *         "male_ratio": 75,
 *         "female_ratio": 25,
 *         "website": "http://www.tsinghua.edu.cn",
 *         "phone": "招生电话：010-62770334；010-62782051",
 *         "comprehensive_rank": 1,
 *         "founding_year": 1911,
 *         "address": "北京市海淀区清华大学",
 *         "introduce": "清华大学概览...",
 *         "is_favorite": false
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /schools/admissions/{id}             学校 - 院校前年投档线和录取概率
 * @apiVersion 1.0.0
 * @apiName admissions
 * @apiGroup School
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          id                                   学校 ID
 *
 * @apiSuccess {int}        throwSchoolLine                          上一年投档线
 * @apiSuccess {string}     filingProbability                        投档概率
 * @apiSuccess {string}     filingProbabilityText                    概率信息
 * @apiSuccess {string}     token                                授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         {
 *            "throwSchoolLine": 668,
 *            "filingProbability": "33%",
 *            "filingProbabilityText": "几乎不可能"
 *         },
 *
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */


/**
 * @api {get} /schools/filingLine/{id}             学校 - 院校投档线
 * @apiVersion 1.0.0
 * @apiName filingLine
 * @apiGroup School
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          id                                   学校 ID
 *
 * @apiSuccess {int}        year                                 年份
 * @apiSuccess {int}        province_filing_line                 投档线
 * @apiSuccess {int}        min_score                             最低分
 * @apiSuccess {string}     token                                授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": [
 *         {
 *          {
 *              "min_score": 644,
 *              "province_filing_line": 644,
 *              "year": 2013
 *         },
 *         .....
 *       ],
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /schools/majorScore/{id}/{year:\d+}             学校 - 专业分数线
 * @apiVersion 1.0.0
 * @apiName majorScore
 * @apiGroup School
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          id                                   学校 ID
 * @apiParam {year}         year                                 年份
 *
 * @apiSuccess {int}        school_major_id                      学校专业 ID
 * @apiSuccess {string}     major_code                           学校专业代码
 * @apiSuccess {string}     major_name                           学校专业名称
 * @apiSuccess {int}        min_score                            最低分
 * @apiSuccess {int}        max_score                            最高分
 * @apiSuccess {int}        average_score                        平均分
 * @apiSuccess {int}        enrollment_count                     实际招生人数
 * @apiSuccess {string}     token                                授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": [
 *         {
 *           "major_code": null,
 *           "school_major_id": 58,
 *           "subject": "文科",
 *           "school_major_name": "工业工程",
 *           "min_score": 682,
 *           "max_score": 686,
 *           "average_score": 0,
 *           "enrollment_count": 0
 *         },
 *          ....
 *       ],
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /schools/majors/{id}            学校 - 在招专业
 * @apiVersion 1.0.0
 * @apiName majors
 * @apiGroup School
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          id                                   学校 ID
 *
 * @apiSuccess {int}        school_id                           学校专业 ID
 * @apiSuccess {string}     major_id                           学校专业代码
 * @apiSuccess {string}     major_name                           学校专业名称
 * @apiSuccess {int}        degree_type                            学位类型
 * @apiSuccess {int}        introduce                            简介
 * @apiSuccess {string}     token                                授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": [
 *         {
 *           "id": 1,
 *           "school_id": 1,
 *           "major_id": 0,
 *           "major_name": "航空航天工程(定向航空)",
 *           "degree_type": null,
 *           "introduce": null
 *         },
 *          ....
 *       ],
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /schools/filters               学校 - 获取筛选条件
 * @apiVersion 1.0.0
 * @apiName schoolFilers
 * @apiGroup School
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "type": [
 *           {
 *             "type": "公办"
 *           },
 *           {
 *             "type": "私立"
 *           }
 *         ],
 *         "subject_type": [
 *           {
 *             "subject_type": "体育院校"
 *           },
 *           {
 *             "subject_type": "农业院校"
 *           },
 *          {
 *             "subject_type": "医药院校"
 *           }
 *         ],
 *         "property": [
 *           {
 *             "property": "211"
 *           },
 *           {
 *             "property": "双一流 985 211"
 *           },
 *          {
 *             "property": "医药"
 *           }
 *         ],
 *         "province": [
 *           {
 *             "province_name": "上海"
 *           },
 *           {
 *             "province_name": "云南"
 *           }
 *         ]
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {get} /schools/search/plan?name={name}&province_id={province_id}&limit={limit}&page={page}      学校 - 招生计划搜索
 * @apiVersion 1.0.0
 * @apiName SearchPlanList
 * @apiGroup School
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {string}       name                                 学校名称
 * @apiParam {int}          province_id                          所在省份id
 * @apiParam {int}          [limit]                              每页个数默认15
 * @apiParam {int}          [page]                               当前页默认1
 *
 * @apiSuccess {int}        id                                   招生计划id
 * @apiSuccess {string}     school_name                          学校名称
 * @apiSuccess {string}     school_code                          学校标识码
 * @apiSuccess {string}     subject                              文理科
 * @apiSuccess {string}     batch                                批次
 * @apiSuccess {int}        recruit_count                        招生计划数量
 * @apiSuccess {int}        score                                投档线
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *           "id": 5,
 *           "school_code": "231",
 *           "school_name": "河套学院",
 *           "subject": "理科",
 *           "batch": "本科第一批",
 *           "recruit_count": 758,
 *           "min_score": 466
 *           }
 *         ],
 *         "total": 1
 *       },
 *       "token": null
 *     }
 */