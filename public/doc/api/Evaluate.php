<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {post} /evaluates            用户评测 - 创建
 * @apiVersion 1.0.0
 * @apiName evaluateCreated
 * @apiGroup Evaluate
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {string}       type               评测类型：character->性格评测；profession->专业评测
 * @apiParam {string}       result             评测用户所选答案
 * @apiParam {string}       key                评测结果
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "type": "character",
 *       "result": "E,I,E,I,E,E,E,S,S,N,N,S,N,N,F,F,T,F,F,F,F,P,J,J,J,J,J,J,J",
 *       "key": "ISFJ",
 *     }
 *
 * @apiSuccess {string}     type            评测类型：character->性格评测；profession->专业评测
 * @apiSuccess {string}     result          评测用户所选答案
 * @apiSuccess {string}     key             评测结果
 * @apiSuccess {string}     token           授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": [
 *         {
 *           "type": "character",
 *           "result": "E,I,E,I,E,E,E,S,S,N,N,S,N,N,F,F,T,F,F,F,F,P,J,J,J,J,J,J,J",
 *           "key": "ISFJ",
 *         }
 *       ],
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /evaluates             用户评测 - 获取
 * @apiVersion 1.0.0
 * @apiName evaluateList
 * @apiGroup Evaluate
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiSuccess {string}     type            评测类型：character->性格评测；profession->专业评测
 * @apiSuccess {string}     result          评测用户所选答案
 * @apiSuccess {string}     key             评测结果
 * @apiSuccess {int}        total           用户已评测数量。0代表还没有评测
 * @apiSuccess {string}     token           授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "list": [],
 *         "total": 0
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "list": [
 *           {
 *             "type": "character",
 *             "result": "E,I,E,I,E,E,E,S,S,N,N,S,N,N,F,F,T,F,F,F,F,P,J,J,J,J,J,J,J",
 *             "key": "INFJ"
 *           }
 *         ],
 *         "total": 1
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */