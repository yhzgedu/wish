<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {post} /login                                用户 - 登录
 * @apiVersion 1.0.0
 * @apiName userLogin
 * @apiGroup Users
 *
 * @apiParam {string}       phone                   手机号
 * @apiParam {string}       code                    验证码
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "phone": "15111223344",
 *       "code": "785623",
 *     }
 *
 * @apiSuccess {int}        id                      用户 ID
 * @apiSuccess {string}     phone                   电话号码
 * @apiSuccess {string}     real_name               真实姓名
 * @apiSuccess {string}     nickname                昵称
 * @apiSuccess {string}     avatar                  图像
 * @apiSuccess {int}        sex                     性别[1:男, 2:女]
 * @apiSuccess {int}        province_id             省份
 * @apiSuccess {int}        subject                 科目[1:文科, 2:理科]
 * @apiSuccess {int}        score                   分数
 * @apiSuccess {int}        status                  状态[1:正常, 2:锁定, 3:禁用]
 * @apiSuccess {int}        student_id              学生证号
 * @apiSuccess {string}     address                 地址
 * @apiSuccess {string}     memo                    备注[简介]
 * @apiSuccess {string}     created_at              创建时间
 * @apiSuccess {string}     token                   授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "id": 1,
 *         "phone": "15157123839",
 *         "real_name": "黄朝森",
 *         "nickname": "黄易",
 *         "avatar": "http://www.yhzgedu.com/user/10/avatar/1533624280.jpg",
 *         "sex": 1,
 *         "province_id": 1,
 *         "subject": 1,
 *         "score": null,
 *         "status": 1,
 *         "student_id": "201400020125",
 *         "address": "十堰市郧西县",
 *         "memo": null,
 *         "created_at": "2017-01-13 04:10:10"
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /users/{id}                            用户 - 详情
 * @apiVersion 1.0.0
 * @apiName userDetail
 * @apiGroup Users
 *
 * @apiParam {int}          id                      用户 ID
 *
 * @apiSuccess {int}        id                      用户 ID
 * @apiSuccess {string}     phone                   电话号码
 * @apiSuccess {string}     real_name               真实姓名
 * @apiSuccess {string}     nickname                昵称
 * @apiSuccess {string}     avatar                  图像
 * @apiSuccess {int}        sex                     性别[1:男, 2:女]
 * @apiSuccess {int}        province_id             省份
 * @apiSuccess {int}        subject                 科目[1:文科, 2:理科]
 * @apiSuccess {int}        score                   分数
 * @apiSuccess {int}        status                  状态[1:正常, 2:锁定, 3:禁用]
 * @apiSuccess {int}        student_id              学生证号
 * @apiSuccess {string}     address                 地址
 * @apiSuccess {string}     memo                    备注[简介]
 * @apiSuccess {string}     created_at              创建时间
 * @apiSuccess {string}     token                   授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "id": 1,
 *         "phone": "15157123839",
 *         "real_name": "黄朝森",
 *         "nickname": "黄易",
 *         "avatar": "http://www.yhzgedu.com/user/10/avatar/1533624280.jpg",
 *         "sex": 1,
 *         "province_id": 1,
 *         "subject": 1,
 *         "score": null,
 *         "status": 1,
 *         "student_id": "201400020125",
 *         "address": "十堰市郧西县",
 *         "memo": null,
 *         "created_at": "2017-01-13 04:10:10"
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {post} /users/info                               用户 - 修改资料
 * @apiVersion 1.0.0
 * @apiName userInfo
 * @apiGroup Users
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {string}       nickname                    昵称
 * @apiParam {string}       sex                         性别[1:男, 2:女]
 * @apiParam {string}       province_id                 省份
 * @apiParam {string}       subject                     科目[1:文科, 2:理科]
 * @apiParam {string}       score                       分数
 * @apiParam {string}       real_name                   真实姓名
 * @apiParam {string}       student_id                  学生证号
 * @apiParam {string}       address                     地址
 * @apiParam {file}         avatar                      头像文件
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "nickname": "黄易",
 *       "sex": "1",
 *       "province_id": "1",
 *       "subject": "1",
 *       "score": "666",
 *       "real_name": "黄朝森",
 *       "student_id": "201421356985",
 *       "address": "十堰市郧西县",
 *       "avatar": avatar file
 *     }
 *
 * @apiSuccess {int}        id                      用户 ID
 * @apiSuccess {string}     phone                   电话号码
 * @apiSuccess {string}     real_name               真实姓名
 * @apiSuccess {string}     nickname                昵称
 * @apiSuccess {string}     avatar                  图像
 * @apiSuccess {int}        sex                     性别[1:男, 2:女]
 * @apiSuccess {int}        province_id             省份
 * @apiSuccess {int}        subject                 科目[1:文科, 2:理科]
 * @apiSuccess {int}        score                   分数
 * @apiSuccess {int}        status                  状态[1:正常, 2:锁定, 3:禁用]
 * @apiSuccess {int}        student_id              学生证号
 * @apiSuccess {string}     address                 地址
 * @apiSuccess {string}     memo                    备注[简介]
 * @apiSuccess {string}     created_at              创建时间
 * @apiSuccess {string}     token                   授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "id": 1,
 *         "phone": "15157123839",
 *         "real_name": "黄朝森",
 *         "nickname": "黄易",
 *         "avatar": "http://www.yhzgedu.com/user/10/avatar/1533624280.jpg",
 *         "sex": 1,
 *         "province_id": 1,
 *         "subject": 1,
 *         "score": null,
 *         "status": 1,
 *         "student_id": "201400020125",
 *         "address": "十堰市郧西县",
 *         "memo": null,
 *         "created_at": "2017-01-13 04:10:10"
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {post} /users/avatar                             用户 - 修改头像
 * @apiVersion 1.0.0
 * @apiName userAvatar
 * @apiGroup Users
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {file}       avatar                      头像文件
 *
 * @apiSuccess {int}        data                        [ > 0 验证成功]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "avatar":"http://www.yhzgedu.com/user/10/avatar/1533624280.jpg"
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /logout                                    用户 - 退出
 * @apiVersion 1.0.0
 * @apiName userLogout
 * @apiGroup Users
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiSuccess {int}        data                        [ > 0 验证成功]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */
