<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {post} /codes        公共 - 获取验证码
 * @apiVersion 1.0.0
 * @apiName commonCode
 * @apiGroup Common
 *
 * @apiParam {string}       phone       手机号
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "phone": "15111223344",
 *     }
 *
 * @apiSuccess {int}        data [ > 0 获取成功]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 3,
 *       "token": null
 *     }
 */

/**
 * @api {get} /provinces       公共 - 获取省区名称和id
 * @apiVersion 1.0.0
 * @apiName commonProvince
 * @apiGroup Common
 *
 *
 * @apiSuccess {int}    id   省区id
 * @apiSuccess {string} name 省区名称
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": [
 *         {
 *           "id": 1,
 *           "name": "上海"
 *         }
 *       ],
 *       "token": "null"
 *     }
 */