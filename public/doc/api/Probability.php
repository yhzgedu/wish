<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {post} /probability                录取概率
 * @apiVersion 1.0.0
 * @apiName probability
 * @apiGroup Probability
 * 
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *     
 * @apiParam {int}          school_id                                  学校id
 * @apiParam {int}          user_score                                 分数
 * @apiParam {string}       school_major_name                          专业名称
 * @apiSuccess {string}     token         [作为接口授权验证，通过 Header 方式传递]
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *        "data": {
                "admissionsProbability": 录取概率,
                "filingProbability": 投档概率
            },
 *      }
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */


/**
 * @api {post} probability/filters             录取录取概率筛选项
 * @apiVersion 1.0.0
 * @apiName probability/filters 
 * @apiGroup Probability
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 * @apiParam {string}          school_name   学校名称关键字
 * @apiSuccess {string}     token         [作为接口授权验证，通过 Header 方式传递]
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": [
 *           {
 *               "school_id": 学校id,
 *               "school_name": 学校名称,
 *               "major_list": [
 *                   {
 *                      "major_id": 专业id,
 *                       "major_name": 专业名称,
 *                       "ranking": 专业排名
 *                   },
 *                  ....
 *               ]
 *            }
 *            ....
 *        ]
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *  }
 */
