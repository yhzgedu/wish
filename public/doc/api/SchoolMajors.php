<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /school_majors/{id}?limit={limit}&page={page}     学校专业 - 列表
 * @apiVersion 1.0.0
 * @apiName schoolMajorList
 * @apiGroup School_Majors
 *
 * @apiParam {int}          id              学校ID
 * @apiParam {int}          limit           每页个数
 * @apiParam {int}          page            当前页
 *
 * @apiSuccess {int}        id                                   学校专业ID
 * @apiSuccess {string}     major_name                           专业名称
 * @apiSuccess {int}        school_id                            学校ID
 * @apiSuccess {string}     school_name                          学校名称
 * @apiSuccess {int}        major_id                             专业ID
 * @apiSuccess {string}     degree_type                          学历类型
 * @apiSuccess {int}        major_type                           专业所属系别
 * @apiSuccess {int}        url                                  链接
 * @apiSuccess {int}        status                               状态[1:显示2:删除]
 * @apiSuccess {string}     created_at                           创建时间
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 1,
 *             "school_id": 1,
 *             "school_name": "北京大学",
 *             "major_id": 20,
 *             "major_name": "会计专业",
 *             "degree_type": "本科",
 *             "major_type": "经济管理系",
 *             "url": "www.xxx.com",
 *             "status": 1,
 *             "created_at": "2017-08-20 09:20:04"
 *           }
 *         ],
 *         "total": 1
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {post} /school_majors 学校专业 - 创建
 * @apiVersion 1.0.0
 * @apiName schoolMajorsFill
 * @apiGroup School_Majors
 *
 * @apiParam {int}        school_id                            学校ID
 * @apiParam {string}     school_name                          学校名称
 * @apiParam {int}        major_id                             专业ID
 * @apiParam {string}     major_name                           专业名称
 * @apiParam {string}     degree_type                          学历类型
 * @apiParam {int}        major_type                           专业所属系别
 * @apiParam {int}        url                                  链接
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "school_id": 1,
 *       "school_name": "北京大学",
 *       "major_id": 20,
 *       "major_name": "会计专业",
 *       "degree_type": "本科",
 *       "major_type": "经济管理系",
 *       "url": "www.xxx.com"
 *     }
 *
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /school_majors/{id}/detail 学校专业 - 详情
 * @apiVersion 1.0.0
 * @apiName getSchoolMajorsDetail
 * @apiGroup School_Majors
 *
 * @apiParam {int}          id              专业ID
 *
 * @apiSuccess {int}        id                                   专业ID
 * @apiSuccess {string}     name                                 专业名称
 * @apiSuccess {string}     degree_type                          学历类型
 * @apiSuccess {string}     major_type                           专业所属系别
 * @apiSuccess {string}     major_category
 * @apiSuccess {string}     introduce                            专业介绍
 * @apiSuccess {int}        graduation_salary                    毕业平均薪酬
 * @apiSuccess {int}        two_year_salary                      毕业两年平均薪酬
 * @apiSuccess {int}        five_year_salary                     毕业五年平均薪酬
 * @apiSuccess {int}        ten_year_salary                      毕业十年平均薪酬
 * @apiSuccess {int}        male_ratio                           男生占比
 * @apiSuccess {int}        female_ratio                         女生占比
 * @apiSuccess {string}     json
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 1,
 *             "name": "煤层气采输技术",
 *             "degree_type": "专科",
 *             "major_type": "资源环境与安全大类",
 *             "major_category": "煤炭类",
 *             "introduce": "无",
 *             "graduation_salary": null,
 *             "two_year_salary": null,
 *             "five_year_salary": 0,
 *             "ten_year_salary": null,
 *             "male_ratio": 0,
 *             "female_ratio": 0,
 *             "json": "{\"params\":{\"major_id\":\"576ce15ac62e9c35de92c8a3\",\"_major_id\":\"576ce15ac62e9c35de92c8a3\",\"diploma\":5,\"diploma_id\":5},\"intro\":\"\",\"excellent\":[],\"employment\":{\"salary\":[],\"countrySal\":[],\"ind\":[],\"zhineng_dis\":[],\"loc\":[]}}"
 *           }
 *         ]
 *       },
 *       "token": null
 *     }
 */


/**
 * @api {get} /school_majors/{id}/delete 学校专业 - 删除
 * @apiVersion 1.0.0
 * @apiName schoolMajorDelete
 * @apiGroup School_Majors
 *
 * @apiParam {int}          id              学校专业ID
 *
 * @apiSuccess {int}        data            > 0
 * @apiSuccess {string}     token           授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */