<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /plans?year={year}&subject={subject}&recruit_province={recruit_province}&school_province={school_province}&limit={limit}&page={page}                  招生计划 - 列表
 * @apiVersion 1.0.0
 * @apiName planList
 * @apiGroup Plan
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          year                                 年份
 * @apiParam {string}       subject                              科目类型
 * @apiParam {string}       recruit_province                     招生地区
 * @apiParam {string}       school_province                      学校位置
 * @apiParam {int}          limit                                每页个数
 * @apiParam {int}          page                                 当前页
 *
 * @apiSuccess {int}        id                                   招生计划ID
 * @apiSuccess {int}        year                                 年份
 * @apiSuccess {string}     school_name                          学校名称
 * @apiSuccess {int}        school_code                          学校代码
 * @apiSuccess {string}     school_logo                          学校LOGO
 * @apiSuccess {int}        year                                 年份
 * @apiSuccess {int}        score                                分数线
 * @apiSuccess {int}        recruit_count                        招生人数
 * @apiSuccess {bool}       is_favorite                          是否已收藏
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 1,
 *             "school_code": 10001,
 *             "school_name": '北京大学',
 *             "school_logo": null,
 *             "year": 2017,
 *             "score": 453,
 *             "recruit_count": 200,
 *             "is_favorite": true
 *           }
 *         ],
 *         "total": 1
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /plans/{id}                                        招生计划 - 详情
 * @apiVersion 1.0.0
 * @apiName planDetail
 * @apiGroup Plan
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          id                                   招生计划 ID
 *
 * @apiSuccess {int}        school_id                            学校ID
 * @apiSuccess {int}        year                                 年份
 * @apiSuccess {string}     school_name                          学校名称
 * @apiSuccess {int}        school_code                          学校代码
 * @apiSuccess {string}     school_logo                          学校LOGO
 * @apiSuccess {int}        score                                分数线
 * @apiSuccess {int}        recruit_count                        招生人数
 * @apiSuccess {bool}       is_favorite                          是否已收藏
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": [
 *         {
 *           "school_id": 1,
 *           "school_name": "晋中职业技术学院",
 *           "school_code": "",
 *           "school_logo": null,
 *           "year": 2014,
 *           "score": 452,
 *           "recruit_count": 100,
 *           "is_favorite": false,
 *           "recruit": [
 *             {
 *               "major_id": 1,
 *               "major_name": "煤层气采输技术",
 *               "major_code": "4",
 *               "recruit_count": 50,
 *               "school_year": 4,
 *               "tuition": 6000
 *             }
 *           ]
 *         }
 *       ],
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /plans/filters            招生计划 - 获取筛选条件
 * @apiVersion 1.0.0
 * @apiName planFilers
 * @apiGroup Plan
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "year": [
 *           {
 *             "year": 2017
 *           }
 *         ],
 *         "subject": [
 *           {
 *             "subject": "文科"
 *           },
 *           {
 *             "subject": "理科"
 *           }
 *         ],
 *         "recruit_province": [
 *           {
 *             "province_name": "上海"
 *           },
 *           {
 *             "province_name": "云南"
 *           }
 *         ],
 *         "school_province": [
 *           {
 *             "province_name": "上海"
 *           },
 *           {
 *             "province_name": "云南"
 *           }
 *         ],
 *       },
 *       "token": null
 *     }
 */
