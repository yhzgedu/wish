<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /collegeApplications?limit={limit}&page={page}      模拟报考首页（列表）
 * @apiVersion 1.0.0
 * @apiName collegeApplicationList
 * @apiGroup AnalogueFilling
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          limit           每页个数
 * @apiParam {int}          page            当前页
 *
 * @apiSuccess {int}        id                                   志愿ID
 * @apiSuccess {string}     uid                                  填报人ID
 * @apiSuccess {int}        school_id                            学校ID
 * @apiSuccess {string}     school_name                          学校名称
 * @apiSuccess {int}        first_major_id                       第一专业ID
 * @apiSuccess {string}     first_major_name                     第一专业名称
 * @apiSuccess {int}        second_major_id                      第二专业ID
 * @apiSuccess {string}     second_major_name                    第二专业名称
 * @apiSuccess {int}        third_major_id                       第三专业ID
 * @apiSuccess {string}     third_major_name                     第三专业名称
 * @apiSuccess {int}        fourth_major_id                      第四专业ID
 * @apiSuccess {string}     fourth_major_name                    第四专业名称
 * @apiSuccess {int}        fifth_major_id                       第五专业ID
 * @apiSuccess {string}     fifth_major_name                     第五专业名称
 * @apiSuccess {int}        sixth_major_id                       第六专业ID
 * @apiSuccess {string}     sixth_major_name                     第六专业名称
 * @apiSuccess {string}     degree_type                          学历类型
 * @apiSuccess {int}        type                                 填报类型(1:冲;2:保;3:稳;4:垫)
 * @apiSuccess {int}        sort                                 排序（1-6分别代表第一到第六志愿）
 * @apiSuccess {int}        is_obey                              是否服从调剂(1:是;2:否)
 * @apiSuccess {int}        status                               状态[1:显示2:删除]
 * @apiSuccess {string}     created_at                           创建时间
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": {
 *           "本科第一批": [
 *             {
 *               "id": 2,
 *               "uid": 1,
 *               "school_id": 2012,
 *               "school_name": "兰州大学",
 *               "first_major_id": 2019,
 *               "first_major_name": "经济学",
 *               "second_major_id": null,
 *               "second_major_name": null,
 *               "third_major_id": null,
 *               "third_major_name": null,
 *               "fourth_major_id": null,
 *               "fourth_major_name": null,
 *               "fifth_major_id": null,
 *               "fifth_major_name": null,
 *               "sixth_major_id": null,
 *               "sixth_major_name": null,
 *               "degree_type": "本科第一批",
 *               "type": 1,
 *               "sort": 1,
 *               "is_obey": 1,
 *               "status": 1,
 *               "created_at": "2018-01-16 11:37:57"
 *               "updated_at": "2018-01-16 11:37:57"
 *               "logo": "http://school-icon.b0.upaiyun.com/52ac2e9a747aec013fcf5251.jpg"
 *             },
 *             {
 *               "id": 3,
 *               "uid": 1,
 *               "school_id": 2017,
 *               "school_name": "成都学院",
 *               "first_major_id": 2019,
 *               "first_major_name": "经济学",
 *               "second_major_id": null,
 *               "second_major_name": null,
 *               "third_major_id": null,
 *               "third_major_name": null,
 *               "fourth_major_id": null,
 *               "fourth_major_name": null,
 *               "fifth_major_id": null,
 *               "fifth_major_name": null,
 *               "sixth_major_id": null,
 *               "sixth_major_name": null,
 *               "degree_type": "本科第一批",
 *               "type": 2,
 *               "sort": 2,
 *               "is_obey": 1,
 *               "status": 1,
 *               "created_at": "2018-01-16 12:15:21"
 *               "updated_at": "2018-01-16 12:15:21"
 *               "logo": "http://school-icon.b0.upaiyun.com/52ac2e9b747aec013fcf5367.jpg"
 *             }
 *           ],
 *           "本科第二批": [
 *             {
 *               "id": 4,
 *               "uid": 1,
 *               "school_id": 2020,
 *               "school_name": "长春东方职业学院",
 *               "first_major_id": 2020,
 *               "first_major_name": "经济学",
 *               "second_major_id": null,
 *               "second_major_name": null,
 *               "third_major_id": null,
 *               "third_major_name": null,
 *               "fourth_major_id": null,
 *               "fourth_major_name": null,
 *               "fifth_major_id": null,
 *               "fifth_major_name": null,
 *               "sixth_major_id": null,
 *               "sixth_major_name": null,
 *               "degree_type": "本科第二批",
 *               "type": 2,
 *               "sort": 2,
 *               "is_obey": 1,
 *               "status": 1,
 *               "created_at": "2018-01-16 12:19:09"
 *               "updated_at": "2018-01-16 12:19:09"
 *               "logo": "http://school-icon.b0.upaiyun.com/52ac2e99747aec013fcf4f5e.jpg"
 *             }
 *           ]
 *         },
 *         "total": 1
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {post} /collegeApplications               模拟填报保存
 * @apiVersion 1.0.0
 * @apiName collegeApplicationFill
 * @apiGroup AnalogueFilling
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}        school_id                            学校ID
 * @apiParam {string}     school_name                          学校名称
 * @apiParam {int}        first_major_id                       第一专业ID
 * @apiParam {string}     first_major_name                     第一专业名称
 * @apiParam {int}        second_major_id                      第二专业ID
 * @apiParam {string}     second_major_name                    第二专业名称
 * @apiParam {int}        third_major_id                       第三专业ID
 * @apiParam {string}     third_major_name                     第三专业名称
 * @apiParam {int}        fourth_major_id                      第四专业ID
 * @apiParam {string}     fourth_major_name                    第四专业名称
 * @apiParam {int}        fifth_major_id                       第五专业ID
 * @apiParam {string}     fifth_major_name                     第五专业名称
 * @apiParam {int}        sixth_major_id                       第六专业ID
 * @apiParam {string}     sixth_major_name                     第六专业名称
 * @apiParam {string}     degree_type                          学历类型
 * @apiParam {int}        type                                 填报类型(1:冲;2:保;3:稳;4:垫)
 * @apiParam {int}        sort                                 排序（1-6分别代表第一到第六志愿）
 * @apiParam {int}        is_obey                              是否服从调剂(1:是;2:否)
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "school_id": 1,
 *       "school_name": "北京大学",
 *       "first_major_id": 1
 *       "first_major_name": "first_name"
 *       "second_major_id": 2
 *       "second_major_name": "second_name"
 *       "third_major_id": 3
 *       "third_major_name": "third_name"
 *       "fourth_major_id": 4
 *       "fourth_major_name": "fourth_name"
 *       "fifth_major_id": 5
 *       "fifth_major_name": "fifth_name"
 *       "sixth_major_id": 6
 *       "sixth_major_name": "sixth_name"
 *       "degree_type": "本科一批",
 *       "type": 1,
 *       "sort": 1,
 *       "is_obey": 1
 *     }
 *
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

