<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /majors                                           专业 - 列表(所有)
 * @apiVersion 1.0.0
 * @apiName majorList
 * @apiGroup Major
 *
 * @apiSuccess {int}        id                                   专业ID
 * @apiSuccess {string}     name                                 专业名称
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "专科": {
 *           "资源环境与安全大类":{
 *             "煤炭类": [
 *               {
 *                 "id": 1
 *                 "name": "煤层气采输技术",
 *               },
 *               {
 *                 "id": 231
 *                 "name": "矿井建设",
 *               }
 *             ],
 *             "测绘地理信息类": [
 *               {
 *                 "id": 31
 *                 "name": "摄影测量与遥感技术",
 *               },
 *               {
 *                 "id": 45
 *                 "name": "测绘工程技术",
 *               }
 *             ]
 *           }
 *         },
 *         "本科": {
 *           "工学": {
 *             "公安技术类": [
 *               {
 *                 "id": 5
 *                 "name": "核生化消防",
 *               },
 *               {
 *                 "id": 74
 *                 "name": "抢险救援指挥与技术",
 *               }
 *             ],
 *             "建筑类": [
 *               {
 *                 "id": 24
 *                 "name": "风景园林",
 *               },
 *               {
 *                 "id": 338
 *                 "name": "历史建筑保护工程",
 *               }
 *             ]
 *           }
 *         }
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {get} /majors/{id}                                      专业 - 详情
 * @apiVersion 1.0.0
 * @apiName getMajorDetail
 * @apiGroup Major
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {int}          id                                   专业ID
 *
 * @apiSuccess {int}        id                                   专业ID
 * @apiSuccess {string}     name                                 专业名称
 * @apiSuccess {string}     degree_type                          学历类型
 * @apiSuccess {string}     major_type                           专业所属系别
 * @apiSuccess {string}     major_category
 * @apiSuccess {string}     introduce                            专业介绍
 * @apiSuccess {int}        graduation_salary                    毕业平均薪酬
 * @apiSuccess {int}        two_year_salary                      毕业两年平均薪酬
 * @apiSuccess {int}        five_year_salary                     毕业五年平均薪酬
 * @apiSuccess {int}        ten_year_salary                      毕业十年平均薪酬
 * @apiSuccess {int}        male_ratio                           男比例
 * @apiSuccess {int}        female_ratio                         女比例
 * @apiSuccess {string}     industry                             行业去向
 * @apiSuccess {string}     job                                  岗位去向
 * @apiSuccess {string}     json
 * @apiSuccess {bool}       is_favorite                          是否已收藏
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 1,
 *             "name": "煤层气采输技术",
 *             "degree_type": "专科",
 *             "major_type": "资源环境与安全大类",
 *             "major_category": "煤炭类",
 *             "introduce": "无",
 *             "graduation_salary": null,
 *             "two_year_salary": null,
 *             "five_year_salary": 0,
 *             "ten_year_salary": null,
 *             "male_ratio": 0,
 *             "female_ratio": 0,
 *             "industry": [
 *               {
 *                 "name": "航天航空",
 *                 "ratio": "14.04"
 *               }
 *             ],
 *             "job": [
 *               {
 *                 "name": "航天航空",
 *                 "ratio": "14.04"
 *               }
 *             ],
 *             "json": "{\"params\":{\"major_id\":\"576ce15ac62e9c35de92c8a3\",\"_major_id\":\"576ce15ac62e9c35de92c8a3\",\"diploma\":5,\"diploma_id\":5},\"intro\":\"\",\"excellent\":[],\"employment\":{\"salary\":[],\"countrySal\":[],\"ind\":[],\"zhineng_dis\":[],\"loc\":[]}}",
 *             "is_favorite": true
 *           }
 *         ]
 *       },
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */