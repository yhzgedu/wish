<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /filters?province_id={province_id}&school_nature={school_nature}&school_arrangement={school_arrangement}&major_name={major_name}&admissions_probability={admissions_probability}&limit={limit}&page={page}                智能筛选列表
 * @apiVersion 1.0.0
 * @apiName filters
 * @apiGroup Filter
 * 
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *     
 * @apiParam {int}          limit                                每页个数
 * @apiParam {int}          page                                 当前页
 * @apiParam {string}       province_id                          学校地区
 * @apiParam {string}       school_nature                        学校性质 
 * @apiParam {string}       school_arrangement                   学校层次
 * @apiParam {string}       major_name                    		 专业名称
 * @apiParam {string}       admissions_probability               录取概率
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token         [作为接口授权验证，通过 Header 方式传递]
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": {
 *         "list": [
 *            {
 *               "school_id":学校id
 **              "school_name": 学校名称
 *               "major_id": 专业id
 *               "major_name": 专业名称
 *               "school_logo": 学校logo
 *               "is_985": 1, 是否是985（1=是,0=否）
 *               "is_211": 1, 是否是211（1=是,0=否）
 *               "subject_type": 学校性质,
 *               "education": 学校层次,
 *               "property":  本次,
 *               "province_name" : 省名称
 *               "year": 年份
                 "min_score": 最低分数
                 "recruit_count": 计划招生人数
                 "probability": 录取概率
 *           },
 *           ........
 *       }
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */


/**
* @api {get} /filters_option              智能筛选项
* @apiVersion 1.0.0
* @apiName filters_option
* @apiGroup Filter
* @apiHeaderExample {json} Header-Example:
*     {
*       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
*     }
* @apiSuccess {int}        data        > 0
* @apiSuccess {string}     token         [作为接口授权验证，通过 Header 方式传递]
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "code": 200,
*       "message": "Success",
*       "data": {
*           "school_filter": {//学校选项
*               "area": [//地区
*                    {
*                        "id": 1,
*                        "name": "上海"
*                    },
*                ]
*                "school_nature": [//学校性质
*                    {
*                        "id": 1,
*                        "name": "综合"
*                    },
*                ],
*                "school_arrangement": [//学校层次    
*                    {
*                        "id": 1,
*                        "name": "本科"
*                    },
*                ]
*
*            }
*           "school_filter": {//专业
*              "major_list": {
*                "专科": { //一级
*                    "资源环境与安全大类": {/// 二级
*                        "煤炭类": [//三级
*                            {
*                                "id": 1,
*                                "name": "煤层气采输技术" //四级
*                            },
*                        ]
*                    }
*                }
*           }
*           "probability_filter": {//录取概率
*              "admissions_probability": [
*                    {
*                        "id": 1,
*                        "name": "冲"
*                    },
*                    {
*                        "id": 2,
*                       "name": "保"
*                    },
*                    {
*                        "id": 3,
*                        "name": "稳"
*                    },
*                    {
*                        "id": 4,
*                        "name": "垫"
*                    }
*                ]
*           }
*       },
*       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
*     }
*/
