<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /province_scores?province_name={province_name}&year={year}&batch={batch}&subject={subject}&limit={limit}&page={page} 省控线 - 列表
 * @apiVersion 1.0.0
 * @apiName provinceScoreList
 * @apiGroup ProvinceScore
 *
 * @apiParam {string}       province_name                        省份
 * @apiParam {int}          year                                 年份
 * @apiParam {string}       batch                                批次(一本/二本/专科..)
 * @apiParam {string}       subject                              科目类型
 * @apiParam {int}          limit                                每页个数
 * @apiParam {int}          page                                 当前页
 *
 * @apiSuccess {string}     province_name                        省份
 * @apiSuccess {int}        year                                 年份
 * @apiSuccess {string}     subject                              科目类型
 * @apiSuccess {int}        score                                分数线
 * @apiSuccess {string}     batch                                批次(一本/二本/专科..)
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "province_name": '北京',
 *             "year": 2017,
 *             "subject": "理科",
 *             "score": 499,
 *             "batch": "本科一批"
 *           }
 *         ],
 *         "total": 2
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {get} /province_scores/filters 省控线 - 获取筛选条件
 * @apiVersion 1.0.0
 * @apiName provinceScoreFilers
 * @apiGroup ProvinceScore
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "province": [
 *           {
 *             "province_name": "上海"
 *           },
 *           {
 *             "province_name": "云南"
 *           }
 *         ],
 *         "year": [
 *           {
 *             "year": 2017
 *           }
 *         ],
 *         "batch": [
 *           {
 *             "batch": "本科一批"
 *           }
 *         ],
 *         "subject": [
 *           {
 *             "subject": "文科"
 *           },
 *           {
 *             "subject": "理科"
 *           }
 *         ]
 *       },
 *       "token": null
 *     }
 */
