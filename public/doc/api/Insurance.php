<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /insurances?limit={limit}&page={page}             保险 - 列表
 * @apiVersion 1.0.0
 * @apiName insurancesList
 * @apiGroup Insurances
 *
 * @apiParam {int}          limit           每页个数
 * @apiParam {int}          page            当前页
 *
 * @apiSuccess {int}        id              保险ID
 * @apiSuccess {int}        uid             保险人
 * @apiSuccess {string}     service_id      保险服务ID
 * @apiSuccess {int}        wish_id         志愿ID
 * @apiSuccess {string}     status          状态[1:正常2:关闭3:理赔]
 * @apiSuccess {string}     created_at      创建时间
 * @apiSuccess {string}     token           授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 1,
 *             "uid": 1,
 *             "service_id": 1,
 *             "wish_id": 1,
 *             "status": "1",
 *             "created_at": "2016-10-18 11:43:17"
 *           }
 *         ],
 *         "total": 2
 *       },
 *       "token": null
 *     }
 */

/**
 * @api {post} /insurances           保险 - 创建
 * @apiVersion 1.0.0
 * @apiName insurancesLogin
 * @apiGroup Insurances
 *
 * @apiParam {string}       wish_id         志愿ID
 * @apiParam {string}       service_id      服务ID
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "wish_id": "1",
 *       "service_id": "1"
 *     }
 *
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /insurances/{id}          保险 - 详情
 * @apiVersion 1.0.0
 * @apiName getInsurancesDetail
 * @apiGroup Insurances
 *
 * @apiParam {int}          id              保险ID
 *
 * @apiSuccess {int}        id              保险ID
 * @apiSuccess {int}        uid             保险人
 * @apiSuccess {string}     service_id      保险服务ID
 * @apiSuccess {int}        wish_id         志愿ID
 * @apiSuccess {string}     status          状态[1:正常2:关闭3:理赔]
 * @apiSuccess {string}     created_at      创建时间
 * @apiSuccess {string}     token           授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "id": 1,
 *         "uid": 1,
 *         "service_id": 1,
 *         "wish_id": 1,
 *         "status": "1",
 *         "created_at": "2016-10-18 11:43:17"
 *       },
 *       "token": "token"
 *     }
 */

/**
 * @api {get} /insurances/{id}/indemnity             保险 - 理赔
 * @apiVersion 1.0.0
 * @apiName getInsurancesIndemnity
 * @apiGroup Insurances
 *
 * @apiParam {int}          id              保险ID
 *
 * @apiSuccess {int}        data            > 0
 * @apiSuccess {string}     token           授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "id": 1,
 *         "uid": 1,
 *         "service_id": 1,
 *         "wish_id": 1,
 *         "status": "1",
 *         "created_at": "2016-10-18 11:43:17"
 *       },
 *       "token": "token"
 *     }
 */
