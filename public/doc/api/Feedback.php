<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {post} /feedback 用户反馈 - 创建
 * @apiVersion 1.0.0
 * @apiName feedbackCreated
 * @apiGroup Feedback
 *
 * @apiParam {string}       account             用户手机/微信/QQ
 * @apiParam {string}       content             反馈内容
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "account": "18223505994",
 *       "content": "萤火之光，可以燎原！"
 *     }
 *
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */
