<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /banners                   轮播图获取
 * @apiVersion 1.0.0
 * @apiName bannerList
 * @apiGroup Banner
 *
 * @apiSuccess {int}        data        > 0
 * @apiSuccess {string}     token       授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": [
 *         {
 *           "title": "Banner3",
 *           "sort": 3,
 *           "url": "http://www.yhzgedu.com/banner/banner3.png"
 *         },
 *         {
 *           "title": "Banner2",
 *           "sort": 2,
 *           "url": "http://www.yhzgedu.com/banner/banner2.png"
 *         },
 *         {
 *           "title": "Banner1",
 *           "sort": 1,
 *           "url": "http://www.yhzgedu.com/banner/banner1.png"
 *         }
 *       ],
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */
