<?php
/**
 * @Description: 接口文档
 */

/**
 * @api {get} /favorites/{target}?limit={limit}&page={page}     收藏 - 列表
 * @apiVersion 1.0.0
 * @apiName favoritesList
 * @apiGroup Favorites
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {string}       target                  目标[ 1 => 学校, 2 => 专业, 3 => 招生计划 ]
 * @apiParam {int}          limit                   每页个数
 * @apiParam {int}          page                    当前页
 *
 * @apiSuccessExample Success-Response:
 *     //学校收藏
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 2020,
 *             "name": "长春东方职业学院",
 *             "logo": null,
 *             "is_985": 0,
 *             "is_211": 0,
 *             "type": 公办, //学校类型 [ 公立, 民办 ]
 *             "property": "一本", //学校性质 [ 一本, 二本, 三本, 专科 ... ]
 *             "subject_type": "医药院校", //学科类型 [ 综合院校, 工科院校, 艺术院校 ... ]
 *             "education": "专科", //学历类型 [ 本科/专科, 本科, 专科 ... ]
 *             "archive_year": 2017,
 *             "archive_province": "长春",
 *             "archive_score": 555,
 *             "archive_probability": "40%",
 *             "province_name": "吉林", //学校地址
 *             "address": "长春市净月旅游经济开发区新大学城路", //详细地址
 *             "male_ratio": 42,
 *             "female_ratio": 58,
 *             "comprehensive_rank": 0 //综合排名
 *           }
 *         ],
 *         "total": 2
 *       },
 *       "token": "eyJpdiI6InJhTklZWlArUzFIS25LdnQycFJHUFE9PSIsInZhbHVlIjoiTHVXTUo2bkR5d29Nd3BkQ2ZHN0JBampLalpDRDM0emNqaUdUOG9aNlZRT0hOTDVsbDVBTEluXC9hWVZVSStoSlEiLCJtYWMiOiI2Y2MxZDA4MjFkMWIxNWI0MzM4NGQwMDM3ODcxNTU5ZGVlOWI2OTQwMDM1NmYwZWFmZDlmMThkMjc0ZGQ1NmExIn0="
 *     }
 *
 *     //专业收藏
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 2,
 *             "name": "航空地面设备维修", //专业名称
 *             "degree_type": "专科", //学历类型
 *             "major_type": "交通运输大类", //专业类别
 *             "major_category": "航空运输类"
 *           }
 *         ],
 *         "total": 2
 *       },
 *       "token": "eyJpdiI6InJhTklZWlArUzFIS25LdnQycFJHUFE9PSIsInZhbHVlIjoiTHVXTUo2bkR5d29Nd3BkQ2ZHN0JBampLalpDRDM0emNqaUdUOG9aNlZRT0hOTDVsbDVBTEluXC9hWVZVSStoSlEiLCJtYWMiOiI2Y2MxZDA4MjFkMWIxNWI0MzM4NGQwMDM3ODcxNTU5ZGVlOWI2OTQwMDM1NmYwZWFmZDlmMThkMjc0ZGQ1NmExIn0="
 *     }
 *
 *     //招生计划收藏
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "",
 *       "data": {
 *         "list": [
 *           {
 *             "id": 2,
 *             "school_id": 2,
 *             "school_name": "河套学院", //学校名称
 *             "school_code": null, //学校代码
 *             "year": null, //年份
 *             "count": 500 //招生人数
 *           }
 *         ],
 *         "total": 2
 *       },
 *       "token": "eyJpdiI6InJhTklZWlArUzFIS25LdnQycFJHUFE9PSIsInZhbHVlIjoiTHVXTUo2bkR5d29Nd3BkQ2ZHN0JBampLalpDRDM0emNqaUdUOG9aNlZRT0hOTDVsbDVBTEluXC9hWVZVSStoSlEiLCJtYWMiOiI2Y2MxZDA4MjFkMWIxNWI0MzM4NGQwMDM3ODcxNTU5ZGVlOWI2OTQwMDM1NmYwZWFmZDlmMThkMjc0ZGQ1NmExIn0="
 *     }
 */

/**
 * @api {post} /favorites                        收藏 - 创建
 * @apiVersion 1.0.0
 * @apiName favoritesCreated
 * @apiGroup Favorites
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {string}       target              目标[ 1 => 学校, 2 => 专业, 3 => 招生计划 ]
 * @apiParam {int}          target_id           目标ID
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "target": "major",
 *       "target_id": "2"
 *     }
 *
 * @apiSuccess {int}        data                > 0
 * @apiSuccess {string}     token               授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */

/**
 * @api {get} /favorites/cancel/{target}/{target_id} 收藏 - 取消
 * @apiVersion 1.0.0
 * @apiName favoritesCancel
 * @apiGroup Favorites
 *
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 *
 * @apiParam {string}       target              目标[ 1 => 学校, 2 => 专业, 3 => 招生计划 ]
 * @apiParam {int}          target_id           目标ID
 *
 * @apiSuccess {int}        data                > 0
 * @apiSuccess {string}     token               授权验证 token [作为接口授权验证，通过 Header 方式传递]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 200,
 *       "message": "Success",
 *       "data": 1,
 *       "token": "eyJpdiI6IjlUWFlWc0hieXAzZHY0SUF6eWdIUXc9PSIsInZhbHVlIjoiK29EQzh0M0xROVJXNDR4a0dDZmdSR3h1bVlmQ2ZueWIyWTlsaEdzaDhkYms2dXlWb3RnbjFPZCtJRCs5YlhkeCIsIm1hYyI6ImQ0NTZlZTY1OGZkMWE4YmZlMjk5YzRmMDMxZDRkZGE4MWQ4ZjY0OTlmNGFiMzlhNjI5YjY1NDgwZDAwZmYzOWYifQ=="
 *     }
 */
