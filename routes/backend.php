<?php
/**
 * Application Backend Routes
 *
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
$app->get('admins', ['uses' => 'AdminController@index']);
// Detail
$app->get('users/{id}', ['uses' => 'UserController@detail']);
// Update
$app->post('users/{id}', ['uses' => 'UserController@updated']);
// Update password
$app->post('users/updatePwd/{id}', ['uses' => 'UserController@updatePwd']);
// Logout
$app->get('logout', ['uses' => 'AdminController@logout']);

