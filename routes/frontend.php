<?php
/**
 * Application Frontend Routes
 *
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
$app->group([
    'middleware' => ['auth','operation'],
    'namespace' => 'App\Http\Controllers\Frontend'
    ], function () use ($app) {
    // Users 用户
    {
        // Detail
        $app->get('users/{id:\d+}', ['uses' => 'UserController@detail']);
        // Update info
        $app->post('users/info', ['uses' => 'UserController@update']);
        // Update avatar
        $app->post('users/avatar', ['uses' => 'UserController@avatar']);
        // Logout
        $app->get('logout', ['uses' => 'UserController@logout']);
    }
    // My 我的
    {
        // Favorites 收藏
        $app->get('favorites/{target}', ['uses' => 'FavoriteController@index']);
        $app->post('favorites', ['uses' => 'FavoriteController@create']);
        $app->get('favorites/cancel/{target}/{target_id:\d+}', ['uses' => 'FavoriteController@cancel']);
        // Insurance 保险
        $app->get('insurances', ['uses' => 'InsuranceController@index']);
        $app->get('insurances/{id:\d+}', ['uses' => 'InsuranceController@detail']);
        $app->post('insurances', ['uses' => 'InsuranceController@create']);
        $app->get('insurances/{id:\d+}/indemnity', ['uses' => 'InsuranceController@indemnity']);
        // Notice 通知
        $app->get('notices', ['uses' => 'NoticeController@index']);
        $app->get('notices/{id:\d+}', ['uses' => 'NoticeController@detail']);
        $app->post('notices', ['uses' => 'NoticeController@create']);
        $app->get('notices/{id:\d+}/delete', ['uses' => 'NoticeController@delete']);
        $app->get('notices/counts', ['uses' => 'NoticeController@counts']);
    }
    // CollegeApplication 志愿填报
    {
        $app->get('collegeApplications', ['uses' => 'CollegeApplicationController@index']);
        $app->post('collegeApplications', ['uses' => 'CollegeApplicationController@fill']);
        $app->get('collegeApplications/{id:\d+}/cancel', ['uses' => 'CollegeApplicationController@cancel']);
    }
    // Evaluates 评测
    {
        $app->get('evaluates', 'EvaluateController@index');
        $app->post('evaluates', 'EvaluateController@create');
    }

    // Share 分享
    {
        $app->post('shares', ['uses' => 'ShareController@create']);
    }

    // Schools 学校
    {
        $app->get('schools', ['uses' => 'SchoolController@index']);
        $app->get('schools/{id:\d+}', ['uses' => 'SchoolController@detail']);
        $app->get('schools/admissions/{id:\d+}', ['uses' => 'SchoolController@schoolAdmissionsProbability']);
        $app->get('schools/filingLine/{id:\d+}', ['uses' => 'SchoolController@filingLine']);
        $app->get('schools/majorScore/{id:\d+}/{year:\d+}', ['uses' => 'SchoolController@majorScore']);
        $app->get('schools/majors/{id:\d+}', ['uses' => 'SchoolController@major']);
        $app->get('schools/filters', ['uses' => 'SchoolController@filters']);
        $app->get('schools/search/plan', ['uses' => 'SchoolController@searchPlan']);
    }

    // SchoolMajor 学校专业管理
    {
        $app->get('school_majors/{id:\d+}', ['uses' => 'SchoolMajorController@index']);
        $app->post('school_majors', ['uses' => 'SchoolMajorController@create']);
        $app->get('school_majors/{id:\d+}/detail', ['uses' => 'SchoolMajorController@detail']);
        $app->get('school_majors/{id:\d+}/delete', ['uses' => 'SchoolMajorController@delete']);
    }

    // Speciality 专业
    {
        $app->get('majors', ['uses' => 'MajorController@index']);
        $app->get('majors/{id:\d+}', ['uses' => 'MajorController@detail']);
        $app->get('majors/filters', ['uses' => 'MajorController@filters']);
    }

    // Province Score 省控线
    {
        $app->get('province_scores', ['uses' => 'ProvinceScoreController@index']);
        $app->get('province_scores/filters', ['uses' => 'ProvinceScoreController@filters']);
    }

    // Plan 计划
    {
        $app->get('plans', ['uses' => 'SchoolScoreController@index']);
        $app->get('plans/{id:\d+}', ['uses' => 'SchoolScoreController@detail']);
        $app->get('plans/filters', ['uses' => 'SchoolScoreController@filters']);
    }
    //智能筛选
    {
        $app->get('filters', ['uses' => 'FilterController@index']);
        $app->get('filters_option', ['uses' => 'FilterController@option']);


    }
     //录取概率
    {
        $app->post('probability', ['uses' => 'ProbabilityController@index']);
        $app->post('probability/filters', ['uses' => 'ProbabilityController@filters']);
    }
    //模拟报考
    {
        $app->post('analogueFilling/addschool', ['uses' => 'AnalogueFillingController@addschool']);
        $app->post('analogueFilling/addmajor', ['uses' => 'AnalogueFillingController@addmajor']);
        $app->post('analogueFilling', ['uses' => 'AnalogueFillingController@index']);
    } 
});

$app->group(['middleware' => 'operation', 'namespace' => 'App\Http\Controllers\Frontend'], function() use ($app) {
    // Login
    $app->post('login', ['uses' => 'UserController@login']);

    // Feedback 反馈
    {
        $app->post('feedback', ['uses' => 'FeedbackController@create']);
    }
    // Banner 轮播图
    {
        $app->get('banners', ['uses' => 'BannerController@index']);
    }

    // News 新闻
    {
        $app->get('news', ['uses' => 'NewsController@index']);
        $app->get('news/{id:\d+}', ['uses' => 'NewsController@detail']);
    }
    //数据处理
    {
        $app->get('datas', ['uses' => 'DatasController@provinceScore']);
        $app->get('datas/schoolMajor', ['uses' => 'DatasController@school_major']);

    }
});