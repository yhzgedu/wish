<?php
/**
 * Application Common Routes
 *
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */

// Codes
{
    // Created Code
    $app->post('codes', ['uses' => 'CommonController@created']);
    //get provinces
    $app->get('provinces','CommonController@provinces');
}
