<?php

namespace App\Model;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/11/20 20:45
 */
class SchoolMajorScore extends BaseModel
{
    protected $table = 'school_major_score';

    protected $visible = [
        'id', 'school_id', 'school_name', 'school_major_id', 'school_major_name', 'province_id', 'province_name', 'year', 'school_year', 'min_score', 'max_score', 'average_score', 'recruit_count'
    ];

    public function schoolMajor()
    {
        return $this->hasOne('App\Model\SchoolMajor', 'id', 'school_major_id');
    }

    public function school()
    {
        return $this->hasOne('App\Model\School', 'id', 'school_id');
    }
}