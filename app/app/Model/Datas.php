<?php
namespace App\Model;
use Illuminate\Support\Facades\DB;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
class Datas extends BaseModel
{
   	public function get_province_id($province_name)
   	{
   		$province_info=DB::table('province')
   			->where('name', 'like', '%'.$province_name.'%')
   			->first();

   		$province_id=0;
   		if(isset($province_info->id)){
   			$province_id=$province_info->id;
   		}
   		return $province_id;
   	}
   	public function get_batch($batch_name)
   	{
   		if(strpos($batch_name,'一')){
   			$batch=1;
   		}elseif(strpos($batch_name,'二')) {
   			$batch=2;	
   		}else{
   			$batch=3;
   		}
   		return $batch;
   	}
   	public function get_major_id($major_name)
   	{
   		$major_id=0;
   		$major_info=DB::table('major')
   			->where('name', 'like', '%'.$major_name.'%')
   			->first();
   		if(isset($major_info->id)){
   			$major_id=$major_info->id;
   		}
   		return $major_id;
   	}
}