<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:36
 */
class Favorite extends BaseModel
{
    protected $table = 'favorite';

    protected $visible = [
        'id', 'uid', 'target', 'target_id', 'status', 'created_at'
    ];

    public function school()
    {
        return $this->hasOne('App\Model\School', 'id', 'target_id');
    }

    public function major()
    {
        return $this->hasOne('App\Model\Major', 'id', 'target_id');
    }

    public function plan()
    {
        return $this->hasOne('App\Model\SchoolScore', 'id', 'target_id');
    }
}