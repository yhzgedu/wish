<?php
namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
class Operation extends BaseModel
{
    public $table = 'operation';

    public $visible = ['id', 'uid', 'url', 'ip', 'method', 'created_at'];
}