<?php

namespace App\Model;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/11/20 20:45
 */
class SchoolScore extends BaseModel
{
    protected $table = 'school_score';

    protected $visible = [
        'id', 'school_id', 'school_name', 'province_id', 'province_name', 'year', 'batch', 'subject',
        'min_score', 'max_score', 'average_score', 'province_filing_line', 'recruit_count',
        'enrollment_count','school_code'
    ];

    public function school()
    {
        return $this->hasOne('App\Model\School', 'id', 'school_id');
    }

    public function favorite()
    {
        return $this->hasMany('App\Model\Favorite', 'target_id', 'id')
            ->where(['status' => 1,'target' => 3]);
    }

    public function province()
    {
        return $this->hasOne('App\Model\Province', 'id', 'province_id');
    }
}