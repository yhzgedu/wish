<?php
namespace App\Model;
use Illuminate\Support\Facades\DB;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
class User extends BaseModel
{
    protected $table = 'user';

    protected $visible = [
        'id', 'phone', 'real_name', 'nickname', 'avatar', 'sex', 'status', 'province_id', 'subject', 'score',
        'student_id', 'address', 'memo', 'token', 'created_at'
    ];

    protected $hidden = ['token'];

    public function user_rank($user)
    {
    	$rank=1;
  		$user_list=DB::table('user')
   			->where('province_id',$user->province_id)
   			->select('id','score')
   			->orderBy('score','desc')
   			->get();
   		if($user_list)
   		{
   			foreach ($user_list as $key => $value) {
   				if($value->id==$user->id){
   					return $rank=$key+1;
   				}
   			}	
   		}
    	return $rank;
    }
}