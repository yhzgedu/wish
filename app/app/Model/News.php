<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:43
 */
class News extends BaseModel
{
    protected $table = 'news';

    protected $visible = [
        'id', 'category_id', 'title', 'cover', 'content', 'is_top', 'status', 'created_at'
    ];

    public function category()
    {
        return $this->hasOne('App\Model\NewsCategory', 'id', 'category_id');
    }
}