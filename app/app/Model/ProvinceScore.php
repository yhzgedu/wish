<?php

namespace App\Model;

/**
 * @Author:: Linch
 * @DateTime: 2017/12/18
 */
class ProvinceScore extends BaseModel
{
    protected $table = 'province_score';

    protected $visible = [
        'id', 'province_id', 'province_name', 'year', 'subject', 'score', 'batch'
    ];
}