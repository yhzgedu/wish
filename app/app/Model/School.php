<?php

namespace App\Model;

use App\Model\SchoolScore;
use App\Model\SchoolMajorScore;
use App\Model\SchoolMajor;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:45
 */
class School extends BaseModel
{
    protected $table = 'school';

    protected $visible = [
        'id', 'name', 'old_name', 'code', 'comprehensive_rank', 'logo', 'is_985', 'is_211', 'property', 'type',
        'province_name', 'address', 'founding_year', 'website', 'post_code', 'phone', 'introduce', 'subject_type', 'education',
        'male_ratio', 'female_ratio', 'master_point', 'doctoral_point', 'five_year_salary', 'recruit_introduce'
    ];

    public function schoolScore()
    {
        return $this->hasMany('App\Model\SchoolScore', 'school_id', 'id');
    }

    public function schoolMajorScore()
    {
        return $this->hasMany('App\Model\SchoolMajorScore', 'school_id', 'id');
    }

    public function major()
    {
        return $this->hasMany('App\Model\SchoolMajor', 'school_id', 'id');
    }

    public function favorite()
    {
        return $this->hasMany('App\Model\Favorite', 'target_id', 'id')
            ->where(['status' => 1, 'target' => 1]);
    }

    static public function schoolAdmissions($school_id, $subject, $u_province_id, $batch, $year)
    {
        $SchoolScoreModel = new SchoolScore();
        $where = [
            'school_id' => $school_id,
            'subject' => $subject,
            'province_id' => $u_province_id,
            'batch' => $batch,
            'year' => $year,
        ];
        $info = $SchoolScoreModel->where($where)->first(['min_score', 'year'])->toArray();
        if ($info) {
            return $info;
        }
        return false;

    }

    static public function lineSchoolScoreList($school_id, $batch, $subject, $year)
    {

        $SchoolMajorScoreModel = new SchoolMajorScore();
        $where = [
            'school_id' => $school_id,
            'batch' => $batch,
            'year' => $year
        ];
        $list = $SchoolMajorScoreModel
            ->where($where)
            ->orderBy('year', 'decs')
            ->limit(5)
            ->get();
        $data = [];

        if ($list) {
            foreach ($list as $k => $item) {
                $data[$k]['major_code'] = $item->schoolMajor->code;
                $data[$k]['school_major_id'] = $item->school_major_id;
                $data[$k]['subject'] = $subject;
                $data[$k]['school_major_name'] = $item->school_major_name;
                $data[$k]['min_score'] = $item->min_score;
                $data[$k]['max_score'] = $item->max_score;
                $data[$k]['average_score'] = $item->average_score;
                $data[$k]['enrollment_count'] = $item->enrollment_count;
            }

        }
        return $data;

    }
}