<?php

namespace App\Model;

/**
 * @Author:: Linch
 * @DateTime: 2017/11/12
 */
class Evaluate extends BaseModel
{
    protected $table = 'evaluate';

    protected $fillable = [
        'uid', 'type', 'result', 'key'
    ];

//    protected $visible = [
//        'id', 'uid', 'type', 'result', 'created_at'
//    ];
}