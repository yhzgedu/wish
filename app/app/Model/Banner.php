<?php

namespace App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @Author:: Linch
 * @DateTime: 2017/11/8
 */
class Banner extends BaseModel
{
    protected $table = 'banner';

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'type', 'status', 'sort', 'url'
    ];
}