<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:48
 */
class Province extends BaseModel
{
    protected $table = 'province';

    protected $visible = [
        'id', 'name'
    ];

    public function score($year = null, $batch = null, $subject = null)
    {
        $query = $this->hasMany('App\Model\ProvinceScore', 'province_id', 'id');
        if ($year) {
            $query = $query->where('year', '=', $year);
        }
        if ($batch) {
            $query = $query->where('batch', '=', $batch);
        }
        if ($subject) {
            $query = $query->where('subject', '=', $subject);
        }
        return $query->get();
    }
}