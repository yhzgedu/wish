<?php

namespace App\Model;
use Illuminate\Container\Container;
use App\Repository\Contracts\UserInterface;
use App\Model\SchoolMajor;
use App\Model\SchoolScore;
use App\Model\ProvinceScore;
use App\Model\SchoolMajorScore;
use App\Model\Major;

/*
* Author：刘永虎
* Note：智能筛选列表
* Date：22018年9月22日0:4:8
*/
class Filter extends BaseModel
{
    /**
    * 获取计划招生人数和本最低分数
    * @param int $property
    * @param int $province_id
    * @param int $year
    * @param int $subject
    * @param int $user_score
    * @param int $school_id
    * @param int $u_province_id
    * @param string $batch
    * @return $probability
    */
    public function get_plan_sorce($province_id,$year,$subject,$batch,$user_score,$school_id,$u_province_id,$school_major_id,$school_major_name)
    {
        $SchooScoreModel=new SchoolScore();
        $where=[
            'school_id'=>$school_id,
            'province_id'=>$u_province_id,
            'batch'=>$batch,
            'year'=>$year,
            'subject'=>$subject,
        ];
        $info=$SchooScoreModel->where($where)->get(['min_score','year','recruit_count'])->toArray();
        if($info){
            return $info;
        }
        return false;
    }
    /**
    * 获取省控线
    * @param int $province_id
    * @param int $year 
    * @param int $subject 
    * @param int $batch 
    * @return $score
    */
    public function provincialControlLine($province_id,$year,$subject,$batch)
    {
        $score=0;
        $ProvinceScoreModel=new ProvinceScore();
        $where=[
            'province_id'=>$province_id,
            'year'=>$year,
            'subject'=>$subject,
            'batch'=>$batch,
        ];

        $info=$ProvinceScoreModel->where($where)->get(['score'])->toArray();
        if($info){
            $score=$info[0]['score'];
        }

        return $score;
    }
     /**
    * 历年院校投挡线
    * @param int $school_id
    * @param int $subject
    * @param int $u_province_id
    * @param string $batch
    * @return $arr
    */
    public function throwSchoolLine($school_id,$subject,$u_province_id,$batch)
    {
       $SchoolScoreModel=new SchoolScore();
        $where=[
            'school_id'=>$school_id,
            'subject'=>$subject,
            'province_id'=>$u_province_id,
            'batch'=>$batch,
        ];
        $info=$SchoolScoreModel->where($where)->get(['min_score','year','province_filing_line'])->toArray();
       if($info){
            return $info;
        }
        return false;
    }
     /**
    * 历年院校专业分数线
    * @param int $school_id
    * @param int $major_id
    * @param int $subject
    * @param int $u_province_id
    * @param string $batch
    * @return $arr
    */
    public function throwSchoolMajorLine($school_id,$school_major_name,$subject,$u_province_id,$batch)
    {

        $SchoolMajorScoreModel=new SchoolMajorScore();
        $where=[
            'school_id'=>$school_id,
            'school_major_name'=>trim($school_major_name),
            'province_id'=>$u_province_id,
            'batch'=>$batch,
        ];
        $info=$SchoolMajorScoreModel->where($where)->get(['min_score','year'])->toArray();

        if($info){
            return $info;
        }
        return false;
    }
     /**
    * 录取概率
    * @param int $province_id
    * @param int $year
    * @param int $subject
    * @param int $user_score
    * @param int $school_id
    * @param int $u_province_id
    * @param string $batch
    * @return $probability
    */
    public function admissionsProbability($province_id,$year,$subject,$batch,$user_score,$school_id,$u_province_id,$school_major_id,$school_major_name)
    {   
        $probability=0;
        //省控分数线
        $province_score=$this->provincialControlLine($province_id,$year,$subject,$batch);
        //历年院校投挡线
        $throwSchoolLine_arr=$this->throwSchoolMajorLine($school_id,$school_major_name,$subject,$u_province_id,$batch);
        if($throwSchoolLine_arr){
            $school_province_diff_score=0;
            $count= 0;
            foreach ($throwSchoolLine_arr as $key => $value) {
                $province_year_core=0;
                $province_year_core=$this->provincialControlLine($province_id,$value['year'],$subject,$batch);
                $arr[$value['year']]=$value['min_score'];
                $brr[$value['year']]= $province_year_core;
                $year_diff=$value['min_score']-$province_year_core;
                if($year_diff < 0 || !$value['min_score'] || !$province_year_core){
                    $year_diff=0;
                }else{
                    $count++;
                }
                $school_province_diff_score+=$year_diff;
            }
            $diff_user_province=$user_score-$province_score;
            if($diff_user_province < 0){
                $diff_user_province=0;
            }
            if($count!=0){
                $dff_school_province=$school_province_diff_score/$count;
                $probability=$diff_user_province/$dff_school_province*1-0.4;   
            }
           

        }   
        return $this->end_probability($probability);

        
    }
    
    /**
    * 投档概率
    * @param int $province_id
    * @param int $year
    * @param int $subject
    * @param int $user_score
    * @param int $school_id
    * @param int $u_province_id
    * @param string $batch
    * @return $probability
    */
    public function filingProbability($province_id,$year,$subject,$batch,$user_score,$school_id,$u_province_id)
    {
        $probability=0;
        //省控分数线
        $province_score=$this->provincialControlLine($province_id,$year,$subject,$batch);
        //历年院校投挡线
        $throwSchoolLine_arr=$this->throwSchoolLine($school_id,$subject,$u_province_id,$batch);
        if($throwSchoolLine_arr){
            $school_province_diff_score=0;
            $count= 0;
         //  $str1= "历年院校投挡线";
         //  $str2= "历年省控线";
            foreach ($throwSchoolLine_arr as $key => $value) {
                $province_year_core=0;
                $province_year_core=$this->provincialControlLine($province_id,$value['year'],$subject,$batch);
              //  $str1=$str1.$value['year']."=".$value['min_score']."  ";
               // $str2=$str2.$value['year']."=".$province_year_core."  ";
                $year_diff=$value['min_score']-$province_year_core;
                if($year_diff < 0 || !$value['min_score'] || !$province_year_core){
                    $year_diff=0;
                }else{
                    $count++;
                }
                $arr[]=$year_diff;
                $school_province_diff_score+=$year_diff;
            }
            $diff_user_province=$user_score-$province_score;
        //    echo $school_province_diff_score;exit;
            if($diff_user_province < 0 ){
                $diff_user_province=0;
            }
            if($count!=0){
                $probability=$diff_user_province/($school_province_diff_score/$count)*1-0.4;
            }

         // dd( $str1."<br>".$str2);exit;  
        }
        return $this->end_probability($probability);

    }
    /**
    * 概率判断
    * @param int $probability
    * @return $probability
    */
    public function end_probability($probability)
    {
        if($probability > 0.99){
            $probability = 0.99;
        }elseif ($probability < 0.1) {
            $probability = 0;
        }else{
            $probability = round($probability,2);
        }
        $probability=$probability*100;
        return $probability."%";
    }
    /**
    * 判断本科批次
    * @param int $property
    * @return $batch
    */
    public function get_property($property)
    {
        if($property=="一本"){
            $batch='1';
        }elseif ($property=="二本") {
            $batch='2';
        }else{
            $batch='3';
        }
        return $batch;
    }
    /**
    * 判断本科批次
    * @param int $property
    * @return $batch
    */
   public function get_subject($subject)
   {
        $subject=$subject == 1 ? '文科' : '理科';
        return $subject;
   }
    /**
    * 判断保,冲,稳,垫
    * @param int $property
    * @return $batch
    */
     public function get_filingProbability($filingProbability)
   {
       $filingProbability_num=intval($filingProbability);
       if($filingProbability_num >= 80 && $filingProbability_num <= 99){
            return 4;
       }elseif ($filingProbability_num >= 60 && $filingProbability_num < 80) {
            return 3;
          
       }elseif ($filingProbability_num >= 50 && $filingProbability_num < 70) {
           return 2;
       }else{
           return 1;
       }
   }
    /**
     * 判断保,冲,稳,垫
     * @param int $property
     * @return $batch
     */
    public function getSchoolFilingProbabilityText($filingProbability)
    {
        $filingProbability_num=intval($filingProbability);
        if($filingProbability_num >= 80 && $filingProbability_num <= 99){
            return '非常大0哦—';
        }elseif ($filingProbability_num >= 60 && $filingProbability_num < 80) {
            return '较容易哦';

        }elseif ($filingProbability_num >= 50 && $filingProbability_num < 70) {
            return '有点难度哦';
        }else{
            return '几乎不可能';
        }
    }
}