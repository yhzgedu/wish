<?php

namespace App\Model;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/10/17 2:45
 */
class SchoolMajor extends BaseModel
{
    protected $table = 'school_major';

    protected $visible = [
        'id', 'school_id', 'major_id', 'degree_type', 'introduce','major_name'
    ];

    public function school()
    {
        return $this->hasOne('App\Model\School', 'id', 'school_id');
    }

    public function major()
    {
        return $this->hasOne('App\Model\Major', 'id', 'major_id');
    }

    public function score($year = null)
    {
        $query = $this->hasMany('App\Model\SchoolMajorScore', 'school_major_id', 'id');
        if ($year) {
            $query = $query->where('year', '=', $year);
        }
        return $query->get();
    }
}