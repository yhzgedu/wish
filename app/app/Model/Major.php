<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:43
 */
class Major extends BaseModel
{
    protected $table = 'major';

    protected $visible = [
        'id', 'name', 'degree_type', 'major_type', 'major_category', 'introduce', 'graduation_salary', 'two_year_salary',
        'five_year_salary', 'ten_year_salary', 'male_ratio', 'female_ratio', 'json'
    ];

    public function favorite()
    {
        return $this->hasMany('App\Model\Favorite', 'target_id', 'id')
            ->where(['status' => 1,'target' => 2]);
    }

    public function industry()
    {
        return $this->hasMany('App\Model\MajorEmployment', 'major_id', 'id')
            ->where('type', '=', 'industry');
    }

    public function job()
    {
        return $this->hasMany('App\Model\MajorEmployment', 'major_id', 'id')
            ->where('type', '=', 'job');
    }
}