<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/14 18:39
 */
class Code extends BaseModel
{
    /**
     * 正常
     */
    const NORMAL = 1;

    /**
     * 已使用
     */
    const ALREADY = 2;

    public $table = 'code';

    public $visible = ['id', 'phone', 'code', 'status', 'created_at', 'updated_at'];
}