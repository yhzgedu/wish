<?php

namespace App\Support\Helper;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

/**
 * @Author:: HuangYin
 * @DateTime: 2017-03-09 10:17
 */
class CommonHelper
{
    public static $HOST = "http://www.yhzgly.cn/";

    /**
     * Encrypt
     *
     * @param string $encrypt
     * @return bool
     */
    public static function encrypt($encrypt = '')
    {
        if (empty($encrypt)) {
            return false;
        }

        return Crypt::encrypt($encrypt);
    }

    /**
     * Decrypt
     *
     * @param string $encrypt
     * @return bool
     */
    public static function decrypt($encrypt = '')
    {
        if (empty($encrypt)) {
            return false;
        }

        try {
            return Crypt::decrypt($encrypt);
        } catch (DecryptException $e) {

            return false;
        }
    }

    /**
     * Set Token
     *
     * @param $token
     * @return bool
     */
    public static function setToken($token)
    {
        return self::encrypt($token);
    }

    /**
     * Get Token
     *
     * @param $token
     * @return bool|mixed|null
     */
    public static function getToken($token)
    {
        $token = self::decrypt($token);
        $token = json_decode($token, true);
        if (is_array($token) && count($token) === 2) {
            return $token;
        }

        return null;
    }

    /**
     * Salt Password
     *
     * @param $salt
     * @param $password
     * @return string
     */
    public static function hashPassword($salt, $password)
    {
        return md5($salt . $password);
    }

    /**
     * Filter Array
     *
     * @param array $data
     * @param array $column
     * @return array
     */
    public static function filterMap(array $data, $column = [''])
    {
        if (!is_array($data) || !$data || !$column) {
            return $data;
        }
        $result = [];
        foreach ($data as $keys => $value) {
            foreach ($value as $key => $item) {
                if (in_array($key, $column)) {
                    $result[$keys][$key] = $item;
                }
            }
        }

        return $result;
    }

    /**
     * Hump to Line
     *
     * @param $str
     * @return null|string|string[]
     */
    public function humpToLine($str)
    {
        $str = preg_replace_callback('/([A-Z]{1})/', function ($matches) {

            return '_' . strtolower($matches[0]);
        }, $str);

        return $str;
    }

    /**
     * 创建多级目录
     *
     * @param string $dir
     * @return bool
     */
    public static function mkdirs($dir)
    {
        if (!is_dir($dir)) {
            if (!self::mkdirs(dirname($dir))) {
                return false;
            }
            if (!mkdir($dir, 0777)) {
                return false;
            }
        }
        return true;
    }


    public static function calculate()
    {
        $provinceScore = \App\Model\ProvinceScore::where('year', '>=', date('Y', strtotime('-4 year')))
            ->where('year', '<=', date('Y', strtotime('-1 year')))
            ->where('province_name', '=', '安徽')
            ->where('subject', '=', '理科')
            ->where('batch', '=', '本科第一批')
            ->get(['id', 'year', 'score']);
        $schoolScore = \App\Model\SchoolScore::where('year', '>=', date('Y', strtotime('-4 year')))
            ->where('year', '<=', date('Y', strtotime('-1 year')))
            ->where('school_id', '=', 2)
            ->where('subject', '=', '理科')
            ->get(['id', 'year', 'min_score']);
        $yearScore = [];
        $diffScore = [];
        foreach ($provinceScore as $value) {
            $yearScore[$value['year']]['province_score'] = $value['score'];
        }
        foreach ($schoolScore as $value) {
            $yearScore[$value['year']]['school_score'] = $value['min_score'];
            if (isset($yearScore[$value['year']]['province_score'])) {
                $diffScore[] = $value['min_score'] - $yearScore[$value['year']]['province_score'];
            }
        }
        $diffScore = [157, 144, 178, 181];
        $averageDiff = array_sum($diffScore) / count($diffScore);
        sort($diffScore);
        $diff = end($diffScore) - reset($diffScore);
        $minDiff = $averageDiff - $diff;
        $maxDiff = $averageDiff + $diff;

        $userScore = 657;
        $yearProvinceScore = 492;
        $userDiffScore = $userScore - $yearProvinceScore;
        $minProbability = 0.01;
        $maxProbability = 0.99;
        if ($userDiffScore <= $minDiff) {
            $probability = $minProbability;
        } else if ($userDiffScore >= $maxDiff) {
            $probability = $maxProbability;
        } else {
            $a = ($maxProbability - $minProbability) / ($maxDiff - $minDiff);
            $b = $minProbability - $a * $minDiff;
            $probability = ($userScore - $yearProvinceScore) * $a + $b;
            $probability = round($probability,2);
        }
        print_r($probability);
        exit;
    }
}