<?php
namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
interface RepositoryInterface
{
    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = ['*']);

    /**
     * @param int $perPage
     * @param int $currentPage
     * @return mixed
     */
    public function paginate($perPage = 15, $currentPage = 1);

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data);

    /**
     * @param array $data
     * @param       $criteria
     * @param       $field
     * @return mixed
     */
    public function update(array $data, $criteria, $field);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param       $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*']);

    /**
     * @param       $field
     * @param       $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($field, $value, $columns = ['*']);

    /**
     * @param       $field
     * @param       $value
     * @param array $columns
     * @return mixed
     */
    public function findAllBy($field, $value, $columns = ['*']);

    /**
     * @param       $where
     * @param array $columns
     * @return mixed
     */
    public function findAll($where, $columns = ['*']);

    /**
     * @param     $id
     * @param     $field
     * @param int $amount
     * @return mixed
     */
    public function increment($id, $field, $amount = 1);
}