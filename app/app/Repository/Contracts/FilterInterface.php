<?php

namespace App\Repository\Contracts;

/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
interface FilterInterface
{
    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param $map
     * @return mixed
     */
    public function lists($limit, $page, $map,$uid);
}