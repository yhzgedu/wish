<?php

namespace App\Repository\Contracts;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/10/20 20:48
 */
interface MajorInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 更新
     *
     * @param       $id
     * @param array $data
     * @return mixed
     */
    public function updated($id, array $data);

    /**
     * 详情
     *
     * @param $id
     * @param $uid
     * @return mixed
     */
    public function detail($id, $uid);

    /**
     * 列表
     *
     * @param $map
     * @return mixed
     */
    public function lists($map);
}