<?php

namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/14 23:27
 */
interface MessageTemplateInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);
}