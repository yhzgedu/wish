<?php

namespace App\Repository\Contracts;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/8/20 20:48
 */
interface SchoolInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 更新
     *
     * @param       $id
     * @param array $data
     * @return mixed
     */
    public function updated($id, array $data);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param $map
     * @param $uid
     * @return mixed
     */
    public function lists($limit, $page, $map, $uid);

    /**
     * 详情
     *
     * @param $id
     * @param $uid
     * @return mixed
     */
    public function detail($id, $uid);

    /**
     * 院校投档线
     *
     * @param $id
     * @param $uid
     * @return mixed
     */
    public function filingLine($id, $uid);

    /**
     * 专业分数线
     *
     * @param $id
     * @param $year
     * @param $uid
     * @return mixed
     */
    public function majorScore($id, $year, $uid);

    /**
     * 在招专业
     *
     * @param $id
     * @return mixed
     */
    public function major($id);

    /**
     * 获取筛选条件
     *
     * @return mixed
     */
    public function filters();
}