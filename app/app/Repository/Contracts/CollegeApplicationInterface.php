<?php

namespace App\Repository\Contracts;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/7/17 2:57
 */
interface CollegeApplicationInterface
{
    /**
     * 申报
     *
     * @param array $data
     * @return mixed
     */
    public function fill(array $data);

    /**
     * 取消
     *
     * @param $uid
     * @param $id
     * @return mixed
     */
    public function cancel($uid, $id);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param $map
     * @return mixed
     */
    public function lists($limit, $page, $map);
}