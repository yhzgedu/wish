<?php

namespace App\Repository\Contracts;

/**
 * @Author:: Linch
 * @DateTime: 2017/10/30
 */
interface FeedbackInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @return mixed
     */
    public function lists($limit, $page);
}