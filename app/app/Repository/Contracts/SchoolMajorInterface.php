<?php

namespace App\Repository\Contracts;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/10/20 20:48
 */
interface SchoolMajorInterface
{
    /**
     * 添加
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 更新状态
     *
     * @param $id
     * @return mixed
     */
    public function deleted($id);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param $map
     * @return mixed
     */
    public function lists($limit, $page, $map);
}