<?php

namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
interface OperationInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 列表 [$uid == 0 ? '所有日志' : '单个用户日志']
     *
     * @param     $limit
     * @param     $page
     * @param int $uid
     * @return mixed
     */
    public function lists($limit, $page, $uid = 0);
}