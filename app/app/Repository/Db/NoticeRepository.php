<?php

namespace App\Repository\Db;

use App\Repository\Contracts\NoticeInterface;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/8/20 20:48
 */
class NoticeRepository extends Repository implements NoticeInterface
{
    protected $rules = [
        'created' => [
            'uid' => 'required|integer',
            'content'=>'required|max:64',
        ]
    ];

    protected $messages = [
        'uid.required'         => "用户 ID 不能为空",
        'uid.integer'          => "用户 ID 必须是整数",
        'content.required'      => "内容不能为空",
        'content.max'           => "内容不能大于 64 个字",
    ];

    protected function modelName()
    {
        return 'App\Model\Notice';
    }

    public function created(array $data)
    {
        $this->validate($data, 'created');

        return $this->save($data);
    }

    public function detail($id)
    {
        return $this->find($id);
    }

    public function lists($limit, $page)
    {
        $total = $this->model->count();

        $list = $this->model->orderBy('id', 'desc')->simplePaginate($limit, ['*'], 'page', $page);

        return $this->simplePaginate('list', $list->items(), $total);
    }

    public function updateStatus($id, $status = 1)
    {
        return $this->update(['status' => $status], $id, 'id');
    }
}