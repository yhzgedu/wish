<?php

namespace App\Repository\Db;

use App\Repository\Contracts\ShareInterface;

/**
 * @Author:: Linch
 * @DateTime: 2017/12/01
 */
class ShareRepository extends Repository implements ShareInterface
{
    protected $rules = [
        'created' => [
            'uid' => 'required|integer',
            'type'=>'required',
            'title'=>'required',
            'content'=>'required',
            'image'=>'required',
            'link'=>'required',
        ]
    ];

    protected $messages = [
        'uid.required'          => "用户 ID 不能为空",
        'uid.integer'           => "用户 ID 必须是整数",
        'type.required'         => "类型不能为空",
        'title.required'        => "标题不能为空",
        'content.required'      => "内容不能为空",
        'image.required'        => "图片不能为空",
        'link.required'         => "链接不能为空",
    ];

    protected function modelName()
    {
        return 'App\Model\Share';
    }

    public function created(array $data)
    {
        $this->validate($data, 'created');

        return $this->save($data);
    }

    public function detail($id)
    {
        return $this->find($id);
    }

    public function lists($limit, $page)
    {
        $total = $this->model->count();

        $list = $this->model->orderBy('id', 'desc')->simplePaginate($limit, ['*'], 'page', $page);

        return $this->simplePaginate('list', $list->items(), $total);
    }
}