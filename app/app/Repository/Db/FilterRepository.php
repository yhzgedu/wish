<?php

namespace App\Repository\Db;

use App\Repository\Contracts\FilterInterface;
use App\Repository\Contracts\UserInterface;
use App\Model\Province;
use App\Model\SchoolMajor;
use App\Model\School;
use App\Model\Filter;
/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
class FilterRepository extends Repository implements FilterInterface
{
    protected function modelName()
    {
        return 'App\Model\Major';
    }
     /**
    * 智能筛选列表
    * @param int $limit
    * @param int $page
    * @param array $map
    * @param int $uid
    * @return $data
    */
    public function lists($limit, $page, $map = [],$uid)
    {
        $user = $this->container->make(UserInterface::class)->detail($uid);
        $Filter_mode=new Filter();
        $where=$this->searchModelMap($map);
        $query = SchoolMajor::leftJoin('school', 'school_major.school_id', '=', 'school.id')
            ->where($where);
        $total =$query->count();
        $data=$query->simplePaginate($limit, ['*'], 'page', $page);
        $list=[];
        $year=date('Y',time());
        if($data->toArray()){
            foreach ($data as $key => $value) {
                $list[$key]['school_id'] = $value->id;
                $list[$key]['school_name'] = $value->school_name;
                $list[$key]['major_id'] = $value->major_id;
                $list[$key]['major_name'] = $value->major_name;
                $list[$key]['school_logo'] = $value->school->logo;
                $list[$key]['is_985'] = $value->is_985;
                $list[$key]['is_211'] = $value->is_211;
                $list[$key]['subject_type'] = $value->subject_type;
                $list[$key]['education'] = $value->education;
                $list[$key]['property'] = $value->property;
                $list[$key]['province_name'] = $value->province_name;
                $list[$key]['year'] = $year;
                //文理科
                $subject=$Filter_mode->get_subject($user['subject'] );
                //本科批次
                $batch=$Filter_mode->get_property($value->property);
                //获取计划招生人数和本最低分数
                $get_plan_sorce=$Filter_mode->get_plan_sorce($value->province_id,$year, $subject,$batch, $user->score,$value->id,$user->province_id,$value->major_id,$value->major_name); 
                $list[$key]['min_score']=$get_plan_sorce ? $get_plan_sorce['min_score'] : 0;
                $list[$key]['recruit_count']=$get_plan_sorce ? $get_plan_sorce['recruit_count'] : 0;
                $list[$key]['probability']=$Filter_mode->admissionsProbability($value->province_id,$year, $subject,$batch, $user->score,$value->id,$user->province_id,$value->major_id,$value->major_name);           
            }
        }
        return $this->simplePaginate('list', $list, $total);
    }
     /**
    * 智能筛选选项
    * @return $list
    */
    public  function filters()
    {
        $list = [];
        $map = [];
        //筛选学校地区
        $list['school_filter']['area']= Province::all();  
        //筛选学校性质
        $list['school_filter']['school_nature']=config('database.school_nature');
        //筛选学校层次    
        $list['school_filter']['school_arrangement']=config('database.school_arrangement');
        //专业列表
        $url = 'www.wmzy.com';
        $major_list=[];
        $query = $this->searchMap($map);
        $data = $query->get(['id','name','degree_type','major_type','major_category'])->toArray();
         foreach ($data as $key => $val) {
             $major_list[$val['degree_type']][$val['major_type']][$val['major_category']][] = [
                 'id' => $val['id'],
                 'name' => $val['name'],
             ];
         }
        $list['major_filter']['major_list']=$major_list;
        //录取概率
        $list['probability_filter']['admissions_probability']=config('database.admissions_probability');
        return $list;

    }
   
}