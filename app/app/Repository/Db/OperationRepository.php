<?php

namespace App\Repository\Db;

use App\Repository\Contracts\OperationInterface;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
class OperationRepository extends Repository implements OperationInterface
{
    protected function modelName()
    {
        return 'App\Model\Operation';
    }

    public function created(array $data)
    {
        return $this->save($data);
    }

    public function lists($limit, $page, $uid = 0)
    {
        $query = $this->model;

        if ($uid) {
            $query = $query->where('uid', '=', $uid);
        }

        $total = $query->count();
        $list = $query->orderBy('id', 'desc')->simplePaginate($limit, ['*'], 'page', $page);

        // TODO 日志转义

        return $this->simplePaginate('list', $list->items(), $total, ceil($total/$limit), $page);
    }
}