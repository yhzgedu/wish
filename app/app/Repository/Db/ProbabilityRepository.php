<?php

namespace App\Repository\Db;

use App\Repository\Contracts\ProbabilityInterface;
use App\Repository\Contracts\UserInterface;
use App\Model\Province;
use App\Model\School;
use App\Model\Filter;
use App\Exceptions\ValidateException;
use App\Support\Helper\CommonHelper;
/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
class ProbabilityRepository extends Repository implements ProbabilityInterface
{
    protected $rules = [
        'filters'   => [
            'school_name' => 'required',
        ]
    ];
    protected $messages = [
        'school_name.required' => "学校名称不能为空",
    ];
    protected function modelName()
    {
        return 'App\Model\SchoolMajor';
    }
    /**
    * 计算院校录取概率
    * @param int $school_id
    * @param int $user_score
    * @param int $uid
    * @param string $school_major_name
    * @return $data
    */
    public function lists($school_id, $user_score, $map,$uid)
    {

        $user = $this->container->make(UserInterface::class)->detail($uid);
        $where=$this->searchModelMap($map);
        $info=$this->model
            ->where($where)
            ->with('school')
            ->get();
        $data=[
            'admissionsProbability'=>'0%',
            'filingProbability'=>'0%',
        ];        
        if($info->toArray()){   
            $Filter_mode=new Filter();
            //文理科
            $subject=$Filter_mode->get_subject($user['subject'] );
            //本科批次
            $batch=$Filter_mode->get_property($info[0]->school->property);
            //录取概率
            $admissionsProbability=$Filter_mode->admissionsProbability($info[0]->school->province_id,date('Y',time()), $subject,$batch, $user_score,$info[0]->school->id,$user->province_id,$info[0]->major_id,$info[0]->major_name);
            //投档概率
            $filingProbability=$Filter_mode->filingProbability($info[0]->school->province_id,date('Y',time()), $subject,$batch, $user_score,$info[0]->school->id,$user->province_id);
            $data=[
                'admissionsProbability'=>$admissionsProbability,
                'filingProbability'=>$filingProbability,
            ];
        }
        return $data;
    }
    /**
    * 预搜索院校专业
    * @param string $school_name
    * @return $newdata
    */
    public  function filters($school_name)
    {   
        $bool=false;
        if($school_name){
            $list=$this->model
            ->where('school_name', 'like', '%'.$school_name.'%')
            ->get();
            $bool=$list->toArray();
        }
        $newdata=[];
        if($bool){
            foreach ($list as $key => $value) {
                $data[$value->school_id]['school_id']=$value->school_id;
                $data[$value->school_id]['school_name']=$value->school_name;
                $data[$value->school_id]['major_list'][]=[
                    'major_id'=>$value['major_id'],
                    'major_name'=>$value['major_name'],
                    'ranking'=>intval($value['ranking'])
                ];
            }
            foreach ($data as $key => $value) {
               $newdata[]=$value;
            }  
        }
        return $newdata;

    }
   
}