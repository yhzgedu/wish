<?php

namespace App\Repository\Db;

use App\Repository\Contracts\SchoolMajorInterface;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/10/20 20:48
 */
class SchoolMajorRepository extends Repository implements SchoolMajorInterface
{
    protected $rules = [
        'created' => [
            'school_id' => 'required|integer',
            'major_id'=>'required|integer',
            'major_name'=>'required|min:2|max:32',
            'school_name'=>'required|min:2|max:32',
            'degree_type'=>'required|min:2|max:32',
            'major_type'=>'required|min:2|max:32'
        ],
        'updated' => [
            'school_id' => 'required|integer',
            'major_id'=>'required|integer',
            'major_name'=>'required|min:2|max:32',
            'school_name'=>'required|min:2|max:32',
            'degree_type'=>'required|min:2|max:32',
            'major_type'=>'required|min:2|max:32'
        ]
    ];

    protected $messages = [
        'school_id.required'         => "学校 ID 不能为空",
        'school_id.integer'         => "学校 ID 必须是整数",
        'major_id.required'         => "专业 ID 不能为空",
        'major_id.integer'         => "专业 ID 必须是整数",
        'major_name.required'         => "专业名称不能为空",
        'major_name.min'              => "专业名称最少两个字",
        'major_name.max'              => "专业名称最多32个字",
        'school_name.required'         => "学校名称不能为空",
        'school_name.min'              => "学校名称最少两个字",
        'school_name.max'              => "学校名称最多32个字",
        'degree_type.required'         => "专业等级不能为空",
        'degree_type.min'              => "专业等级最少两个字",
        'degree_type.max'              => "专业等级最多32个字",
        'major_type.required'         => "专业类别不能为空",
        'major_type.min'              => "专业类别最少两个字",
        'major_type.max'              => "专业类别最多32个字",
    ];

    protected function modelName()
    {
        return 'App\Model\SchoolMajor';
    }

    public function created(array $data)
    {
        $this->validate($data,'created');

        return $this->save($data);
    }

    public function updated($id, array $data)
    {
        $this->validate($data, 'updated');

        return $this->update($data, $id, 'id');
    }

    public function detail($id)
    {
        $detail = $this->model->where('major_id', '=', $id)->with('major')->get();
        $data = [];
        foreach ($detail as $key => $value) {
            $data[$key] = $value->toArray();
            $data[$key]['detail'] = $value->major;
        }

        return $data;
    }

    public function lists($limit, $page, $map = [])
    {
        $query = $this->searchMap($map);
        $total = $query->count();

        $list = $query->simplePaginate($limit, ['*'], 'page', $page);

        return $this->simplePaginate('list', $list->items(), $total);
    }

    public function deleted($id)
    {
        return $this->update(['status' => 2], $id, 'id');
    }
}