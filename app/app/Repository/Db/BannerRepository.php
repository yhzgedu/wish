<?php

namespace App\Repository\Db;

use App\Repository\Contracts\BannerInterface;

/**
 * @Author:: Linch
 * @DateTime: 2017/10/30
 */
class BannerRepository extends Repository implements BannerInterface
{
    protected $rules = [
        'created' => [
            'title' => 'required',
            'status'=>'required',
            'sort'=>'required|integer',
            'url'=>'required|max:255',
        ]
    ];

    protected $messages = [
        'title.required'        => "标题不能为空",
        'status.required'       => "状态不能为空",
        'sort.required'         => "排序不能为空",
        'sort.integer'          => "排序必须是整数",
        'url.required'          => "地址不能为空",
        'url.max'               => "内容不能大于 255 个字",
    ];

    protected function modelName()
    {
        return 'App\Model\Banner';
    }

    public function created(array $data)
    {
        $this->validate($data, 'created');

        return $this->save($data);
    }

    public function detail($id)
    {
        return $this->find($id);
    }

    public function lists()
    {
        $list = $this->model->where('status', 1)
            ->orderBy('sort', 'desc')
            ->get(['title', 'sort', 'url']);

        return $list;
    }
}