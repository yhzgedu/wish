<?php

namespace App\Repository\Db;

use App\Repository\Contracts\SchoolScoreInterface;
use Illuminate\Support\Facades\DB;

/**
 * @Author:: Linch
 * @DateTime: 2017/12/12
 */
class SchoolScoreRepository extends Repository implements SchoolScoreInterface
{
    protected function modelName()
    {
        return 'App\Model\SchoolScore';
    }

    public function detail($id, $uid)
    {
        $detail = $this->model->where('id', '=', $id)->with(['favorite' => function ($query) use ($uid) {
            $query->where('uid', '=', $uid);
        }])->get();
        $data = [];
        foreach ($detail as $key => $val) {
            $data[$key]['school_id'] = $val->school_id;
            $data[$key]['school_name'] = $val->school->name;
            $data[$key]['school_code'] = $val->school->code;
            $data[$key]['school_logo'] = $val->school->logo;
            $data[$key]['year'] = $val->year;
            $data[$key]['score'] = $val->score;
            $data[$key]['recruit_count'] = $val->recruit_count;
            $data[$key]['is_favorite'] = $val->favorite->count() ? true : false;
            if ($val->school->major) {
                foreach ($val->school->major as $k => $major) {
                    foreach ($major->score($val->year) as $score) {
                        $data[$key]['recruit'][] = [
                            'major_id' => $major->major->id,
                            'major_name' => $major->major->name,
                            'major_code' => $major->code,
                            'recruit_count' => $score->recruit_count,
                            'school_year' => $score->school_year,
                            'tuition' => $score->tuition
                        ];
                    }
                }
            }
        }

        return $data;
    }

    public function lists($limit, $page, $map = [], $uid, $schoolProvince)
    {
        $query = $this->searchMap($map);
        $total = $query->count();
        $query = $query->whereHas('school', function ($query) use ($schoolProvince) {
            if ($schoolProvince) {
                $query->where('province_name', '=', $schoolProvince);
            }
        })->with(['favorite' => function ($query) use ($uid) {
            $query->where('uid', '=', $uid);
        }])->groupBy('school_id')->orderBy('id', 'desc');
        $data = $query->simplePaginate($limit, ['*', DB::raw('sum(recruit_count) as recruit_count')], 'page', $page);
        $list = [];
        foreach ($data as $key => $val) {
            if ($val->school) {
                $list[$key]['id'] = $val->id;
                $list[$key]['school_name'] = $val->school->name;
                $list[$key]['school_code'] = $val->school->code;
                $list[$key]['school_logo'] = $val->school->logo;
                $list[$key]['year'] = $val->year;
                $list[$key]['score'] = $val->score;
                $list[$key]['recruit_count'] = $val->recruit_count;
                $list[$key]['is_favorite'] = $val->favorite->count() ? true : false;
            }
        }

        return $this->simplePaginate('list', $list, $total);
    }

    public function filters()
    {
        $list = [];
        $list['year'] = $this->model->groupBy('year')->get(['year'])->toArray();
        $list['subject'] = $this->model->groupBy('subject')->get(['subject'])->toArray();
        $list['recruit_province'] = $this->model->groupBy('province_name')->get(['province_name'])->toArray();
        $list['school_province'] = $this->model->leftJoin('school', 'school.id', '=', 'school_score.school_id')
            ->groupBy('school.province_name')->get(['school.province_name'])->toArray();

        return $list;
    }
}