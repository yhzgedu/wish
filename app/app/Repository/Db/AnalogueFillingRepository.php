<?php

namespace App\Repository\Db;

use App\Repository\Contracts\AnalogueFillingInterface;
use App\Repository\Contracts\UserInterface;
use App\Model\Province;
use App\Model\SchoolMajorScore;
use App\Model\Filter;
use App\Model\User;
use App\Exceptions\ValidateException;
use App\Support\Helper\CommonHelper;
/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
class AnalogueFillingRepository extends Repository implements AnalogueFillingInterface
{
    
    protected function modelName()
    {
        return 'App\Model\School';
    }
    /**
    * 模拟报考首页
    * @param array $school_ids
    * @param int $uid
    * @return json
    */
    public function index($school_ids,$uid)
    {
    	$data=[];
    	$year=date('Y',time());
        $Filter_mode=new Filter();
        $User_mode=new User();
    	$user = $this->container->make(UserInterface::class)->detail($uid);
    	$data['user']=[
    		'province_name'=>$user->province_name,
    		'subject'=>$Filter_mode->get_subject($user->subject),
    		'score'=>$user->score,
    		'rank'=>$User_mode->user_rank($user)
    	];
    	$data['school_list']=[];
   		if($school_ids){
   			$school_arr=explode(",", $school_ids);
   			foreach ($school_arr as $key => $value) {
   				$school_info=[];
   				$school_info=$this->model
   				->select('id','name','logo','province_id','property')
   				->where(['id'=>$value])
   				->first();
   				//文理科
        		$subject=$Filter_mode->get_subject($user['subject'] );
        		//本科批次
        		$batch=$Filter_mode->get_property($school_info->property);
        		$filingProbability=$Filter_mode->filingProbability($school_info->province_id,$year,$subject,$batch,$user->score,$school_info->id,$user->province_id);
   				$data['school_list'][$batch][]=[
   					'id'=>$school_info->id,
   					'name'=>$school_info->name,
   					'filingProbability'=>$filingProbability,
   					'filingProbability_msg'=>$Filter_mode->get_filingProbability($filingProbability),
   				];
   			}
   			
   		}
   		return $data;

   	}
    /**
    * 模拟报考添加院校
    * @param array $map
    * @param int $uid
    * @return json
    */
    public function addschool($map,$uid)
    {
    	$where=$this->searchModelMap($map);	
    	$list=$this->model
            ->where($where)
            ->select('id','name')
            ->get();
    	return $list;
    }
     /**
    * 模拟报考添加专业
    * @param array $map
    * @param int $uid
    * @return json
    */
    public function addmajor($map,$uid)
    {

        $data = [];
    	$year = '2016';
        $Filter_mode=new Filter();
    	$where=$this->searchModelMap($map);
    	$user = $this->container->make(UserInterface::class)->detail($uid);
        $query = $this->model->where($where)
        	->select('id','name','logo','province_id','property')
            ->with(['schoolMajorScore' => function ($query) use ($user, $year) {
                $query->where([
                    'school_major_score.year' => $year,
                    'school_major_score.province_id' => $user['province_id']
                ]);
            }]);
        $detail = $query->first();
        $data=[
        	'id'=>$detail['id'],
        	'name'=>$detail['name'],
        	'logo'=>$detail['logo'],
        ];
        //文理科
        $subject=$Filter_mode->get_subject($user['subject'] );
        //本科批次
        $batch=$Filter_mode->get_property($detail->property);
        $data['filingProbability']=$Filter_mode->filingProbability($detail->province_id,$year,$subject,$batch,$user->score,$detail->id,$user->province_id);
        if ($detail->schoolMajorScore) {
            foreach ($detail->schoolMajorScore as $k => $schoolMajorScore) {
                $data['major_list'][$k]['school_major_id'] = $schoolMajorScore->schoolMajor->id;
                $data['major_list'][$k]['major_name'] = $schoolMajorScore->schoolMajor->major_name;
            }
        }
        return $data;
    }
}