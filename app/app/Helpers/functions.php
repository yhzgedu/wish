<?php

/**
 * 公用的方法  返回json数据，进行信息的提示
 * @param $status 状态
 * @param string $message 提示信息
 * @param array $data 返回数据
 */
if (!function_exists('p'))
{
	 /**
     * 输入数据类型
     * @data string    $data 打印的数据类型
     */
    function p($data)
    {
        echo "<pre>";
        var_dump($data);
        echo "<pre>";
    }
}
