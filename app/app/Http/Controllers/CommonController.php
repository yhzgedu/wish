<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidateException;
use App\Model\Province;
use App\Repository\Contracts\CodeInterface;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var CodeInterface
     */
    protected $code;

    /**
     * CommonController constructor.
     * 
     * @param Request $request
     * @param CodeInterface $code
     */
    public function __construct(Request $request, CodeInterface $code)
    {
        $this->request = $request;

        $this->code = $code;
    }

    /**
     * description
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function created()
    {
        try {
            $data['phone'] = $this->request->input('phone', null);
            $result = $this->code->created($data);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    public function provinces()
    {
        return $this->success(Province::all());
    }
}
