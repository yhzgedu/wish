<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\FilterInterface;
use Illuminate\Http\Request;
use App\Support\Helper\CommonHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;



/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
class FilterController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var FilterInterface
     */
    protected $filter;
     /**
     * @var int
     */
    protected $uid;
   
    public function __construct(Request $request, FilterInterface $filter)
    {
        $this->request = $request;

        $this->filter = $filter;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
    * 智能筛选列表
    * @return json
    */
    public function index()
    {
        $limit=$this->request->input('limit', 15);
        $page = $this->request->input('page', 1); 
        $province_id = $this->request->input('province_id');
        $school_nature = $this->request->input('school_nature');
        $school_arrangement = $this->request->input('school_arrangement');
        $major_name = $this->request->input('major_name');
        $admissions_probability=$this->request->input('admissions_probability');
        $searchMap = $this->searchMap([
            ['school.province_id', $province_id, '='],
            ['school.subject_type', $school_nature, 'like'],
            ['school.education', $school_arrangement, 'like'],
            ['school_major.major_name', $major_name, 'like'],
        ]);
       $list = $this->filter->lists($limit, $page, $searchMap,$this->uid);
       return $this->success($list);
    }
    
    /**
    * 智能筛选选项
    * @return json
    */
    public function option()
    {
        $list = $this->filter->filters();
        return $this->success($list);
    }
    
}