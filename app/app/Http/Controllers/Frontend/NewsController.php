<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\NewsInterface;
use Illuminate\Http\Request;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/8/2 2:33
 */
class NewsController extends Controller
{
    /**
     * @var Request
     */
    public $request;

    /**
     * @var NewsInterface
     */
    public $news;

    /**
     * NewsController constructor.
     *
     * @param NewsInterface $news
     * @param Request $request
     */
    public function __construct(NewsInterface $news, Request $request)
    {
        $this->request = $request;

        $this->news = $news;
    }

    /**
     * 文章详情
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($id)
    {
        $news = $this->news->detail($id);

        return $this->success($news);
    }

    /**
     * 文章列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $list = $this->news->lists($limit, $page);

        return $this->success($list);
    }
}