<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\ShareInterface;
use App\Exceptions\ValidateException;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @Author:: Linch
 * @DateTime: 2017/12/01
 */
class ShareController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ShareInterface
     */
    protected $share;

    /**
     * @var int
     */
    protected $uid;

    /**
     * ShareController constructor.
     *
     * @param Request $request
     * @param ShareInterface $share
     */
    public function __construct(Request $request, ShareInterface $share)
    {
        $this->request = $request;

        $this->share = $share;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $list = $this->share->lists($limit, $page);

        return $this->success($list);
    }

    /**
     * 创建
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        try {
            $data = $this->request->only(['type', 'title', 'content', 'image', 'link']);
            $data['uid'] = $this->uid;
            $result = $this->share->created($data);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * 详情
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail()
    {
        $id = $this->request->only(['id']);
        $list = $this->share->detail($id);

        return $this->success($list);
    }
}