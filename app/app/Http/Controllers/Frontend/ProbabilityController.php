<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\ProbabilityInterface;
use Illuminate\Http\Request;
use App\Support\Helper\CommonHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
class ProbabilityController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ProbabilityInterface
     */ 
     protected $Probability;
     /**
     * @var int
     */
    protected $uid;
    

    public function __construct(Request $request,ProbabilityInterface $Probability)
    {
        $this->request = $request;
        $this->Probability = $Probability;
        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }
    /**
    * 录取概率
    * @return json
    */
    public function index()
    {

        $school_id=$this->request->input('school_id');
        $user_score = $this->request->input('user_score'); 
        $school_major_name = $this->request->input('school_major_name'); 
        $searchMap = $this->searchMap([
            ['school_id', $school_id, '='],
            ['major_name', $school_major_name, '='],
        ]);
        $list = $this->Probability->lists($school_id, $user_score, $searchMap,$this->uid);
        return $this->success($list);
    }
     /**
    * 录取概率选项
    * @return json
    */
     public  function filters()
    {

        $school_name=$this->request->input('school_name');
        $list = $this->Probability->filters($school_name);
        return $this->success($list);

    }
}