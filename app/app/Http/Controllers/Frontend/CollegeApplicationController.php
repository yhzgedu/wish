<?php

namespace App\Http\Controllers\Frontend;

use App\Exceptions\ValidateException;
use App\Http\Controllers\Controller;
use App\Repository\Contracts\CollegeApplicationInterface;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Auth;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/10/20 20:34
 */
class CollegeApplicationController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var CollegeApplicationInterface
     */
    protected $collegeApplication;

    /**
     * @var int
     */
    protected $uid;

    /**
     * CollegeApplicationController constructor.
     *
     * @param Request $request
     * @param CollegeApplicationInterface $collegeApplication
     */
    public function __construct(Request $request, CollegeApplicationInterface $collegeApplication)
    {
        $this->request = $request;

        $this->collegeApplication = $collegeApplication;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $searchMap = $this->searchMap([
            ['uid', $this->uid, '=']
        ]);
        $list = $this->collegeApplication->lists($limit, $page, $searchMap);

        return $this->success($list);
    }

    /**
     * 申报
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fill()
    {
        try {
            $data = $this->request->only(['school_id', 'school_name', 'first_major_id', 'first_major_name', 'second_major_id', 'second_major_name',
                'third_major_id', 'third_major_name','fourth_major_id', 'fourth_major_name','fifth_major_id', 'fifth_major_name',
                'sixth_major_id', 'sixth_major_name', 'degree_type', 'type', 'sort', 'is_obey']);
            $data['uid'] = $this->uid;
            $list = $this->collegeApplication->fill($data);

            return $this->success($list);
        } catch (ValidateException $e) {
            return $this->validateError($e);
        }
    }

    /**
     * 取消
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($id)
    {
        $list = $this->collegeApplication->cancel($this->uid, $id);

        return $this->success($list);
    }
}