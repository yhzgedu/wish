<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Model\SchoolScore;
use App\Repository\Contracts\SchoolInterface;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/8/20 20:36
 */
class SchoolController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var SchoolInterface
     */
    protected $school;

    /**
     * @var int
     */
    protected $uid;

    /**
     * SchoolController constructor.
     *
     * @param Request $request
     * @param SchoolInterface $school
     */
    public function __construct(Request $request, SchoolInterface $school)
    {
        $this->request = $request;

        $this->school = $school;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $type = $this->request->input('type');
        $subjectType = $this->request->input('subject_type');
        $property = $this->request->input('property');
        $provinceName = $this->request->input('province_name');

        $searchMap = $this->searchMap([
            ['type', $type, '='],
            ['subject_type', $subjectType, '='],
            ['property', $property, 'like'],
            ['school.province_name', $provinceName, '=']
        ]);
        $list = $this->school->lists($limit, $page, $searchMap, $this->uid);

        return $this->success($list);
    }

    /**
     * 学校简介
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($id)
    {
        $detail = $this->school->detail($id, $this->uid);

        return $this->success($detail);
    }

    /**
     * 院校前年投档线和录取概率
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function schoolAdmissionsProbability($id)
    {
        $admissions = $this->school->admissions($id, $this->uid);
        return $this->success($admissions);
    }

    /**
     * 院校投档线
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function filingLine($id)
    {
        $detail = $this->school->filingLine($id, $this->uid);

        return $this->success($detail);
    }

    /**
     * 专业分数线
     *
     * @param $id
     * @param $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function majorScore($id, $year)
    {
        $detail = $this->school->majorScore($id, $year, $this->uid);

        return $this->success($detail);
    }

    /**
     * 在招专业
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function major($id)
    {
        $detail = $this->school->major($id);

        return $this->success($detail);
    }

    /**
     * 获取筛选条件
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function filters()
    {
        $list = $this->school->filters();

        return $this->success($list);
    }

    /**
     * 搜索招生计划结果
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchPlan()
    {
        $validator = Validator::make($this->request->input(), [
            'province_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            throw new BadRequestHttpException('省份ID缺失');
        }

        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $name = $this->request->input('name');
        $province = $this->request->input('province_id');

        $needFields = [
            'school_score.id',
            'school.code as school_code',
            'school_score.school_name',
            'school_score.subject',
            'school_score.batch',
            'school_score.recruit_count',
            'school_score.min_score as score',
        ];

        $result = SchoolScore::leftJoin('school', 'school.id', '=', 'school_score.school_id')
            ->where(['school_score.province_id' => $province, 'school_score.year' => (int)date('Y') - 1])
            ->where('school_score.school_name', 'like', "%$name%")
            ->select($needFields)
            ->simplePaginate($limit, $needFields, 'page', $page)
            ->items();

        return $this->success(['list' => $result]);
    }

    /**
     * 搜索招生计划结果
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function admissions($id)
    {
        $admissions = $this->school->admissions($id, $this->uid);
        return $admissions;

    }
}