<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\EvaluateInterface;
use App\Exceptions\ValidateException;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @Author:: Linch
 * @DateTime: 2017/10/30
 */
class EvaluateController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EvaluateInterface
     */
    protected $evaluate;

    /**
     * @var int
     */
    protected $uid;

    /**
     * EvaluateController constructor.
     *
     * @param Request $request
     * @param EvaluateInterface $evaluate
     */
    public function __construct(Request $request, EvaluateInterface $evaluate)
    {
        $this->request = $request;

        $this->evaluate = $evaluate;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $list = $this->evaluate->lists($this->uid);

        return $this->success($list);
    }

    /**
     * 创建
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        try {
            $data = $this->request->only(['type', 'result', 'key']);
            $data['uid'] = $this->uid;
            $result = $this->evaluate->created($data);

            return $this->success([
                'type' => $result->type,
                'result' => $result->result,
                'key' => $result->key,
            ]);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * 详情
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail()
    {
        $id = $this->request->only(['id']);
        $list = $this->evaluate->detail($id);

        return $this->success($list);
    }
}