<?php

namespace App\Repository\Contracts;

/**
 * @Author:: Linch
 * @DateTime: 2017/12/12
 */
interface SchoolScoreInterface
{
    /**
     * 详情
     *
     * @param $id
     * @param $uid
     * @return mixed
     */
    public function detail($id, $uid);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param $map
     * @param $uid
     * @param $schoolProvince
     * @return mixed
     */
    public function lists($limit, $page, $map, $uid, $schoolProvince);

    /**
     * 获取筛选条件
     *
     * @return mixed
     */
    public function filters();
}