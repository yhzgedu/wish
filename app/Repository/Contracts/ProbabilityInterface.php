<?php

namespace App\Repository\Contracts;

/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
interface ProbabilityInterface
{
   
	/**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param $map
     * @param $uid
     * @return mixed
     */
    public function lists($limit, $page, $map, $uid);

      /**
     * 获取筛选条件
     *
     * @return mixed
     */
    public function filters($school_name);
}