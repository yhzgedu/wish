<?php
namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
interface AdminInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);

    /**
     * Token 是否存在
     *
     * @param $token
     * @return mixed
     */
    public function checkToken($token);

    /**
     * 列表
     *
     * @param       $limit
     * @param       $page
     * @return mixed
     */
    public function lists($limit, $page);

    /**
     * 根据手机号查找用户信息
     *
     * @param $phone
     * @return mixed
     */
    public function findByPhone($phone);

    /**
     * 更新
     *
     * @param       $id
     * @param array $data
     * @return mixed
     */
    public function updated($id, array $data);

    /**
     * 修改密码
     *
     * @param $id
     * @param $password
     * @param $newPassword
     * @return mixed
     */
    public function updatePwd($id, $password, $newPassword);

    /**
     * 重置密码
     *
     * @param $id
     * @return mixed
     */
    public function resetPwd($id);

    /**
     * 登录
     *
     * @param array $data
     * @return mixed
     */
    public function signIn(array $data);

    /**
     * 验证
     *
     * @param $uid
     * @param $password
     * @return mixed
     */
    public function check($uid, $password);

    /**
     * 登出
     *
     * @param $uid
     * @return mixed
     */
    public function logout($uid);

    /**
     * 获取菜单
     *
     * @param $roleId
     * @return mixed
     */
    public function getMenu($roleId);

    /**
     * 日志列表 [$uid == 0 ? '所有日志' : '单个用户日志']
     *
     * @param     $limit
     * @param     $page
     * @param int $uid
     * @return mixed
     */
    public function OperationList($limit, $page, $uid = 0);
}