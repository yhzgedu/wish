<?php

namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/14 22:42
 */
interface MessageInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @param $tid
     * @return mixed
     */
    public function created(array $data, $tid = 1);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param array $map
     * @return mixed
     */
    public function lists($limit, $page, $map = []);
}