<?php
namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
interface UserInterface
{
    /**
     * Token 是否存在
     *
     * @param $token
     * @return mixed
     */
    public function checkToken($token);

    /**
     * 登录
     *
     * @param array $data
     * @return mixed
     */
    public function login(array $data);

    /**
     * 退出
     *
     * @param $uid
     * @return mixed
     */
    public function logout($uid);

    /**
     * 更新
     *
     * @param       $id
     * @param array $data
     * @return mixed
     */
    public function updated($id, array $data);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);
}