<?php

namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:56
 */
interface FavoriteInterface
{
    /**
     * 收藏
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 更新状态 [1:收藏2:取消]
     *
     * @param $map
     * @param int $status
     * @return mixed
     */
    public function updateStatus($map, $status = 1);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param $map
     * @param $target
     * @return mixed
     */
    public function lists($limit, $page, $map, $target);
}