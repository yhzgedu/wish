<?php

namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:58
 */
interface NewsInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 更新
     *
     * @param       $id
     * @param array $data
     * @return mixed
     */
    public function updated($id, array $data);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param array $map
     * @return mixed
     */
    public function lists($limit, $page, $map = []);
}