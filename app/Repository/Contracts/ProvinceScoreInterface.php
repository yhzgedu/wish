<?php

namespace App\Repository\Contracts;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/9/3 20:48
 */
interface ProvinceScoreInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 更新
     *
     * @param       $id
     * @param array $data
     * @return mixed
     */
    public function updated($id, array $data);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @param $map
     * @return mixed
     */
    public function lists($limit, $page, $map);

    /**
     * 获取筛选条件
     *
     * @return mixed
     */
    public function filters();
}