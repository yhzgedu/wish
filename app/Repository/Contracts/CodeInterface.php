<?php

namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/14 18:17
 */
interface CodeInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 验证
     *
     * @param $phone
     * @param $code
     * @return mixed
     */
    public function checked($phone, $code);

    /**
     * 更新状态
     *
     * @param $id
     * @return mixed
     */
    public function updateStatus($id);
}