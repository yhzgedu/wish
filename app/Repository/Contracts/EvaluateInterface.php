<?php

namespace App\Repository\Contracts;

/**
 * @Author:: Linch
 * @DateTime: 2017/11/12
 */
interface EvaluateInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);

    /**
     * 列表
     *
     * @param $uid
     * @return mixed
     */
    public function lists($uid);
}