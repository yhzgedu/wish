<?php

namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:57
 */
interface InsuranceInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);

    /**
     * 理赔
     *
     * @param $uid
     * @param $id
     * @return mixed
     */
    public function indemnity($uid, $id);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @return mixed
     */
    public function lists($limit, $page);
}