<?php

namespace App\Repository\Contracts;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:59
 */
interface NoticeInterface
{
    /**
     * 创建
     *
     * @param array $data
     * @return mixed
     */
    public function created(array $data);

    /**
     * 详情
     *
     * @param $id
     * @return mixed
     */
    public function detail($id);

    /**
     * 更新状态 [1:显示2:删除]
     *
     * @param $id
     * @param int $status
     * @return mixed
     */
    public function updateStatus($id, $status = 1);

    /**
     * 列表
     *
     * @param $limit
     * @param $page
     * @return mixed
     */
    public function lists($limit, $page);
}