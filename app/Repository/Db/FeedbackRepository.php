<?php

namespace App\Repository\Db;

use App\Repository\Contracts\FeedbackInterface;

/**
 * @Author:: Linch
 * @DateTime: 2017/10/30
 */
class FeedbackRepository extends Repository implements FeedbackInterface
{
    protected $rules = [
        'created' => [
            'uid' => 'required|integer',
            'account'=>'required|max:20',
            'content'=>'required|max:64',
        ]
    ];

    protected $messages = [
        'uid.required'         => "用户 ID 不能为空",
        'uid.integer'          => "用户 ID 必须是整数",
        'account.required'      => "账户不能为空",
        'account.max'           => "账户不能大于 20 个字",
        'content.required'      => "内容不能为空",
        'content.max'           => "内容不能大于 64 个字",
    ];

    protected function modelName()
    {
        return 'App\Model\Feedback';
    }

    public function created(array $data)
    {
        $this->validate($data, 'created');

        return $this->save($data);
    }

    public function detail($id)
    {
        return $this->find($id);
    }

    public function lists($limit, $page)
    {
        $total = $this->model->count();

        $list = $this->model->orderBy('id', 'desc')->simplePaginate($limit, ['*'], 'page', $page);

        return $this->simplePaginate('list', $list->items(), $total);
    }
}