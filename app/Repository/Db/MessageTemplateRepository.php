<?php

namespace App\Repository\Db;

use App\Repository\Contracts\MessageTemplateInterface;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/14 23:29
 */
class MessageTemplateRepository extends Repository implements MessageTemplateInterface
{
    protected function modelName()
    {
        return 'App\Model\MessageTemplate';
    }

    public function created(array $data)
    {
        return $this->save($data);
    }

    public function detail($id)
    {
        return $this->model->where('id', $id)->where('status', 1)->first();
    }
}