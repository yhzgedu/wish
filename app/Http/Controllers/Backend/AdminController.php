<?php

namespace App\Http\Controllers\Backend;

use App\Exceptions\ValidateException;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Repository\Contracts\AdminInterface;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Auth;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
class AdminController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var AdminInterface
     */
    protected $admin;

    protected $uid;

    protected $storeId;

    /**
     * AdminController constructor.
     *
     * @param Request $request
     * @param AdminInterface $admin
     */
    public function __construct(Request $request, AdminInterface $admin)
    {
        $this->request = $request;

        $this->admin = $admin;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'];

        $this->storeId = CommonHelper::getToken(Auth::user())['storeId'];
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $list = $this->admin->lists($limit, $page);

        return $this->success($list);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($id)
    {
        $detail = $this->admin->detail($id);

        return $this->success($detail);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function created()
    {
        try {
            $data = $this->request->only(['phone', 'password', 'rePassword', 'real_name']);
            $data['role_id'] = $this->request->input('role_id')?:Admin::SUPER_MANAGER;
            // 返回的店铺信息
            $data['store_id'] = $this->request->input('storeId')?:$this->storeId;
            $data['storeName'] = $this->request->input('storeName')?:null;
            $data['shopkeeper'] = $this->request->input('shopkeeper')?:null;
            $data['tel'] = $this->request->input('tel')?:null;
            $data['address'] = $this->request->input('address')?:null;
            $id = $this->admin->created($data);

            return $this->success($id);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * Update
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updated($id)
    {
        try {
            $data = $this->request->only(['phone', 'password', 'rePassword', 'real_name']);
            $result = $this->admin->updated($id, $data);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * Update Password
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePwd($id)
    {
        if ($id != $this->uid) {
            return $this->fail('没有操作权限~');
        }

        try {
            $password = $this->request->input('password');
            $newPassword = $this->request->input('newPassword');
            $result = $this->admin->updatePwd($this->uid, $password, $newPassword);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * Reset Password
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPwd($id)
    {
        try {
            $result = $this->admin->resetPwd($id);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * Sign In
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        try {
            $data = $this->request->only(['phone', 'password']);
            $userInfo = $this->admin->signIn($data);

            return $this->success($userInfo);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            $result = $this->admin->logout($this->uid);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * 日志列表 [$uid == 0 ? '所有日志' : '单个用户日志']
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function operationList()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $uid = $this->request->input('uid', 0);
        $list = $this->admin->operationList($limit, $page, $uid);

        return $this->success($list);
    }
}