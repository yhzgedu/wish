<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\BannerInterface;
use App\Exceptions\ValidateException;
use Illuminate\Http\Request;

/**
 * @Author:: Linch
 * @DateTime: 2017/11/8
 */
class BannerController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var BannerInterface
     */
    protected $banner;

    /**
     * BannerController constructor.
     *
     * @param Request $request
     * @param BannerInterface $banner
     */
    public function __construct(Request $request, BannerInterface $banner)
    {
        $this->request = $request;

        $this->banner = $banner;
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $list = $this->banner->lists();

        return $this->success($list);
    }

    /**
     * 创建
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function created()
    {
        try {
            $data = $this->request->only(['title', 'type', 'sort', 'url']);
            $result = $this->banner->created($data);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * 详情
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail()
    {
        $id = $this->request->only(['id']);
        $list = $this->banner->detail($id);

        return $this->success($list);
    }
}