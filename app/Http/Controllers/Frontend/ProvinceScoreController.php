<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\ProvinceScoreInterface;
use Illuminate\Http\Request;
use Auth;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/9/3 21:36
 */
class ProvinceScoreController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ProvinceScoreInterface
     */
    protected $provinceScore;

    /**
     * ProvinceController constructor.
     * 
     * @param Request $request
     * @param ProvinceScoreInterface $provinceScore
     */
    public function __construct(Request $request, ProvinceScoreInterface $provinceScore)
    {
        $this->request = $request;

        $this->provinceScore = $provinceScore;

    }

    /**
     * 省控线列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $provinceName = $this->request->input('province_name');
        $year = $this->request->input('year');
        $batch = $this->request->input('batch');
        $subject = $this->request->input('subject');

        $searchMap = $this->searchMap([
            ['province_name', $provinceName, '='],
            ['year', $year, '='],
            ['batch', $batch, '='],
            ['subject', $subject, '='],
        ]);
        $list = $this->provinceScore->lists($limit, $page, $searchMap);

        return $this->success($list);
    }

    /**
     * 获取筛选条件
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function filters()
    {
        $list = $this->provinceScore->filters();

        return $this->success($list);
    }
}