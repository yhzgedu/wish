<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Support\Helper\CommonHelper;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use App\Model\Datas;

/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
class DatasController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ProbabilityInterface
     */ 
     protected $Probability;
     /**
     * @var int
     */
    protected $uid;
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
    * 处理省控线分数
    * @param Request $request
    * @param ProbabilityInterface $Probability
    */
    public function provinceScore()
    {
        set_time_limit(0);
        $list=DB::table('data')->get();
        $data_modle=new Datas();
        foreach ($list as $key => $value) {
           $str=ltrim($value->name,"(");
           $str=rtrim($str,")");
           $arr=explode(",", $str);
           $province_id=$data_modle->get_province_id(trim($arr[0],"'"));
           $batch=$data_modle->get_batch(trim($arr[4],"'"));
           $inset_arr=[
                'province_id'=>$province_id,
                'province_name'=>trim($arr[0],"'"),
                'year'=>$arr[1],
                'subject'=>trim($arr[2],"'"),
                'score'=>$arr[3],
                'batch'=>$batch,
                'created_at'=>date('Y-m-d H:i:s',time()),
           ];
          DB::table('province_score')->insert($inset_arr);
          echo $inset_arr['province_name'].$inset_arr['year'].$inset_arr['subject'].trim($arr[4],"'").$inset_arr['score']."插入成功<br>";
        }
    }
    /**
    * 处理学校专业
    * @param Request $request
    * @param ProbabilityInterface $Probability
    */
    public function school_major()
    {
        set_time_limit(0);
        $data_modle=new Datas();
        $list=DB::table('school_major')->select('id','major_id', 'major_name')->get();
        foreach ($list as $key => $value) {
            $major_id= $data_modle->get_major_id($value->major_name);
            DB::table('school_major')
            ->where('id', $value->id)
            ->update(['major_id' => $major_id]);
        }
    }
    
}