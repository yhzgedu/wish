<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\FilterInterface;
use Illuminate\Http\Request;
use App\Support\Helper\CommonHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;



/**
 * @Author:: Linch
 * @DateTime: 2018/1/2
 */
class FilterController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var FilterInterface
     */
    protected $filter;
     /**
     * @var int
     */
    protected $uid;
   
    public function __construct(Request $request, FilterInterface $filter)
    {
        $this->request = $request;

        $this->filter = $filter;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
    * 智能筛选列表
    * @return json
    */
    public function index()
    {
        $limit=$this->request->input('limit', 15);
        $page = $this->request->input('page', 1); 
        $province_id = $this->request->input('province_id');
        $school_nature = $this->request->input('school_nature');
        $school_arrangement = $this->request->input('school_arrangement');
        $major_name = $this->request->input('major_name');
        $admissions_probability=$this->request->input('admissions_probability');


        $array=$searchMap=array();
        if($province_id){
            $array[]=['school.province_id', $province_id, '='];
        }
        if($school_nature){
            $array[]=['school.subject_type', $school_nature, 'like'];
        }
        if($school_arrangement){
            $array[]=['school.education', $school_arrangement, 'like'];
        }
        if($major_name){
            $array[]=['school_major.major_name', $major_name, 'like'];
        }
      
        if(count($array)){
            $searchMap = $this->searchMap($array);
        }
  
       $list = $this->filter->lists($limit, $page, $searchMap,$this->uid);
       return $this->success($list);
    }
    
    /**
    * 智能筛选选项
    * @return json
    */
    public function option()
    {
        $list = $this->filter->filters();
        return $this->success($list);
    }
    
}