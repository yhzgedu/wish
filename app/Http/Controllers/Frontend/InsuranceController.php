<?php

namespace App\Http\Controllers\Frontend;

use App\Exceptions\ValidateException;
use App\Http\Controllers\Controller;
use App\Repository\Contracts\InsuranceInterface;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Auth;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/8/20 17:34
 */
class InsuranceController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var InsuranceInterface
     */
    protected $insurance;

    /**
     * @var int
     */
    protected $uid;

    /**
     * InsuranceController constructor.
     *
     * @param Request $request
     * @param InsuranceInterface $insurance
     */
    public function __construct(Request $request, InsuranceInterface $insurance)
    {
        $this->request = $request;

        $this->insurance = $insurance;

        $this->uid = 1;//CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $list = $this->insurance->lists($limit, $page);

        return $this->success($list);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($id)
    {
        $detail = $this->insurance->detail($id);

        return $this->success($detail);
    }

    /**
     * 购买
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        try {
            $data = $this->request->only(['wish_id', 'service_id']);
            $data['uid'] = $this->uid;
            $list = $this->insurance->created($data);

            return $this->success($list);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * 理赔
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function indemnity($id)
    {
        $list = $this->insurance->indemnity($this->uid, $id);

        return $this->success($list);
    }
}