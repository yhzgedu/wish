<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\AnalogueFillingInterface;
use Illuminate\Http\Request;
use App\Support\Helper\CommonHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
/**
 * @Author 刘永虎
 * @DateTime: 2018年10月11日18:56:43
 * @Api 模拟报考接口
 */
class AnalogueFillingController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ProbabilityInterface
     */ 
     protected $AnalogueFilling;
     /**
     * @var int
     */
    protected $uid;
    

    public function __construct(Request $request,AnalogueFillingInterface $AnalogueFilling)
    {
        $this->request = $request;
        $this->AnalogueFilling = $AnalogueFilling;
        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }
    /**
    * 模拟报考首页
    * @return json
    */
    public function index()
    {
        $school_ids=$this->request->input('school_ids');
        $list = $this->AnalogueFilling->index($school_ids,$this->uid);
        return $this->success($list);
        
    }
    /**
    * 添加院校
    * @return json
    */
    public function addschool()
    {
        $school_name=$this->request->input('school_name');
        $searchMap = $this->searchMap([
            ['name', $school_name, 'like'],
        ]);
        $list = $this->AnalogueFilling->addschool($searchMap,$this->uid);
        return $this->success($list);
    }
    /**
    * 添加专业
    * @return json
    */
    public function addmajor()
    {
        $school_id=$this->request->input('school_id');
        $searchMap = $this->searchMap([
            ['id', $school_id, '='],
        ]);
        $list = $this->AnalogueFilling->addmajor($searchMap,$this->uid);
        return $this->success($list);
    }
}