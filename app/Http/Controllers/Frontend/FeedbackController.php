<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\FeedbackInterface;
use App\Exceptions\ValidateException;
use Illuminate\Http\Request;

/**
 * @Author:: Linch
 * @DateTime: 2017/10/30
 */
class FeedbackController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var FeedbackInterface
     */
    protected $feedback;

    /**
     * @var int
     */
    protected $uid;

    /**
     * FeedbackController constructor.
     *
     * @param Request $request
     * @param FeedbackInterface $feedback
     */
    public function __construct(Request $request, FeedbackInterface $feedback)
    {
        $this->request = $request;

        $this->feedback = $feedback;

        $this->uid = 1;//CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $list = $this->feedback->lists($limit, $page);

        return $this->success($list);
    }

    /**
     * 创建
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        try {
            $data = $this->request->only(['account', 'content']);
            $data['uid'] = $this->uid;
            $result = $this->feedback->created($data);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * 详情
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail()
    {
        $id = $this->request->only(['id']);
        $list = $this->feedback->detail($id);

        return $this->success($list);
    }
}