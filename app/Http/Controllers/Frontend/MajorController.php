<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\MajorInterface;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/10/20 20:36
 */
class MajorController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var MajorInterface
     */
    protected $major;

    /**
     * @var int
     */
    protected $uid;

    /**
     * MajorController constructor.
     *
     * @param Request $request
     * @param MajorInterface $major
     */
    public function __construct(Request $request, MajorInterface $major)
    {
        $this->request = $request;

        $this->major = $major;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 详情
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($id)
    {
        $detail = $this->major->detail($id, $this->uid);

        return $this->success($detail);
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $url = $this->request->input('url', 'www.wmzy.com');

        $searchMap = $this->searchMap([
            ['url', $url, 'like']
        ]);
        $list = $this->major->lists($searchMap);

        return $this->success($list);
    }
}