<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\SchoolScoreInterface;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @Author:: Linch
 * @DateTime: 2017/12/12
 */
class SchoolScoreController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var SchoolScoreInterface
     */
    protected $schoolScore;

    /**
     * @var int
     */
    protected $uid;

    /**
     * SchoolScoreController constructor.
     *
     * @param Request $request
     * @param SchoolScoreInterface $schoolScore
     */
    public function __construct(Request $request, SchoolScoreInterface $schoolScore)
    {
        $this->request = $request;

        $this->schoolScore = $schoolScore;

        $this->uid = CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 详情
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($id)
    {
        $detail = $this->schoolScore->detail($id, $this->uid);

        return $this->success($detail);
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $year = $this->request->input('year', 2017);
        $batch = $this->request->input('batch');
        $subject = $this->request->input('subject');
        $recruitProvince = $this->request->input('recruit_province');
        $schoolProvince = $this->request->input('school_province');

        $searchMap = $this->searchMap([
            ['year', $year, '='],
            ['batch', $batch, '='],
            ['subject', $subject, '='],
            ['province_name', $recruitProvince, '='],
        ]);
        $list = $this->schoolScore->lists($limit, $page, $searchMap, $this->uid, $schoolProvince);

        return $this->success($list);
    }

    /**
     * 获取筛选条件
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function filters()
    {
        $list = $this->schoolScore->filters();

        return $this->success($list);
    }
}