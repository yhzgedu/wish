<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\NoticeInterface;
use App\Exceptions\ValidateException;
use App\Support\Helper\CommonHelper;
use Illuminate\Http\Request;
use Auth;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/8/20 20:36
 */
class NoticeController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var NoticeInterface
     */
    protected $notice;

    /**
     * @var int
     */
    protected $uid;

    /**
     * NoticeController constructor.
     * 
     * @param Request $request
     * @param NoticeInterface $notice
     */
    public function __construct(Request $request, NoticeInterface $notice)
    {
        $this->request = $request;

        $this->notice = $notice;

        $this->uid = 1;//CommonHelper::getToken(Auth::user())['uid'] ?: 0;
    }

    /**
     * 列表
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $list = $this->notice->lists($limit, $page);

        return $this->success($list);
    }

    /**
     * 创建
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        try {
            $data = $this->request->only(['uid', 'content']);
            $result = $this->notice->created($data);

            return $this->success($result);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * 详情
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail()
    {
        $id = $this->request->only(['id', 'notice_id']);
        $list = $this->notice->detail($id);

        return $this->success($list);
    }

    /**
     * 删除
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $list = $this->notice->updateStatus($id, 2);

        return $this->success($list);
    }
}