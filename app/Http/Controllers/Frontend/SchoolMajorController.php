<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\Contracts\SchoolMajorInterface;
use Illuminate\Http\Request;
use Auth;

/**
 * @Author:: LuXiang
 * @DateTime: 2017/10/20 20:36
 */
class SchoolMajorController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var SchoolMajorInterface
     */
    protected $school_major;

    /**
     * SchoolMajorController constructor.
     *
     * @param Request $request
     * @param SchoolMajorInterface $school_major
     */
    public function __construct(Request $request, SchoolMajorInterface $school_major)
    {
        $this->request = $request;

        $this->school_major = $school_major;
    }

    /**
     * 列表
     *
     * @param $school_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($school_id)
    {
        $limit = $this->request->input('limit', 15);
        $page = $this->request->input('page', 1);
        $searchMap = $this->searchMap([
            ['school_id', $school_id, '=']
        ]);
        $list = $this->school_major->lists($limit, $page, $searchMap);

        return $this->success($list);
    }

    /**
     * 添加
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        try {
            $data = $this->request->only(['major_name', 'school_name', 'school_id', 'major_id', 'url', 'degree_type', 'major_id']);
            $list = $this->school_major->created($data);

            return $this->success($list);
        } catch (ValidateException $e) {

            return $this->validateError($e);
        }
    }

    /**
     * 详情
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($id)
    {
        $detail = $this->school_major->detail($id);

        return $this->success($detail);
    }

    /**
     * 更新状态
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $list = $this->school_major->deleted($id);

        return $this->success($list);
    }
}