<?php

namespace App\Providers;

use App\Repository\Contracts\AdminInterface;
use Illuminate\Support\ServiceProvider;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->header('Token')) {
                return app(AdminInterface::class)->checkToken($request->header('Token'));
            }
        });
    }
}