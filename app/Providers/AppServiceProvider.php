<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepositories();
    }

    protected function registerRepositories()
    {
        $repositories = require base_path().DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'repositories.php';
        foreach ($repositories as $interface => $concrete) {
            $this->app->bind($interface, $concrete);
        }
    }
}