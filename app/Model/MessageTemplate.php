<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/14 23:09
 */
class MessageTemplate extends BaseModel
{
    public $table = 'message_template';

    public $visible = ['id', 'content', 'status', 'created_at'];
}