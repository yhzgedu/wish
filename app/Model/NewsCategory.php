<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:44
 */
class NewsCategory extends BaseModel
{
    protected $table = 'news_category';

    protected $visible = [
        'id', 'name', 'description', 'status', 'created_at'
    ];
}