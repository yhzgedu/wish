<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:42
 */
class Notice extends BaseModel
{
    protected $table = 'notice';

    protected $visible = [
        'id','uid', 'content', 'created_at', 'status' ,'updated_at'
    ];
}