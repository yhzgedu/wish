<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/14 22:38
 */
class Message extends BaseModel
{
    public $table = 'message';

    public $visible = ['id', 'tid', 'phone', 'status', 'created_at'];
}