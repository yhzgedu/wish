<?php

namespace App\Model;

/**
 * @Author:: Linch
 * @DateTime: 2017/10/30
 */
class Feedback extends BaseModel
{
    protected $table = 'feedback';

    protected $visible = [
        'id','uid', 'account', 'content', 'created_at' ,'updated_at'
    ];
}