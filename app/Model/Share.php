<?php

namespace App\Model;

/**
 * @Author:: Linch
 * @DateTime: 2017/12/01
 */
class Share extends BaseModel
{
    protected $table = 'share';

    protected $visible = [
        'id', 'uid', 'type', 'title', 'content', 'image', 'link', 'created_at'
    ];

    const UPDATED_AT = null;
}