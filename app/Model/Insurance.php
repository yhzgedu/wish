<?php

namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/7/17 2:38
 */
class Insurance extends BaseModel
{
    protected $table = 'insurance';

    protected $visible = [
        'id', 'uid', 'service_id', 'wish_id', 'status', 'created_at'
    ];
}