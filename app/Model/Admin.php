<?php
namespace App\Model;

/**
 * @Author:: HuangYin
 * @DateTime: 2017/6/7 22:35
 */
class Admin extends BaseModel
{
    /**
     * 超级管理员
     */
    const SUPER_MANAGER = 1;
    
    /**
     * 管理员
     */
    const MANAGER = 2;

    public $table = 'admin';

    public $visible = [
        'id', 'phone', 'email', 'password', 'real_name', 'avatar', 'sex', 'salt', 'role_id', 'status', 'memo',
        'token', 'created_at'
    ];

    protected $hidden = ['password', 'salt', 'token'];
}