<?php

namespace App\Model;

/**
 * @Author:: Linch
 * @DateTime: 2017/12/19
 */
class MajorEmployment extends BaseModel
{
    protected $table = 'major_employment';

    protected $visible = [
        'id', 'major_id', 'major_name', 'type', 'name', 'ratio'
    ];
}